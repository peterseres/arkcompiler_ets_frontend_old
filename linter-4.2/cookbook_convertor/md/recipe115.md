#  Scripts and modules

Rule ``arkts-no-scripts``

**Severity: error**

In general, scripts and modules in ArkTS are very close to TypeScript.
Differences are described in separate recipes.


## See also

- Recipe 118:  Special import type declarations are not supported (``arkts-no-special-imports``)
- Recipe 119:  Importing a module for side-effects only is not supported (``arkts-no-side-effects-imports``)
- Recipe 120:  ``import default as ...`` is not supported (``arkts-no-import-default-as``)
- Recipe 121:  ``require`` is not supported (``arkts-no-require``)
- Recipe 123:  Renaming in export declarations is not supported (``arkts-no-export-renaming``)
- Recipe 124:  Export list declaration is not supported (``arkts-no-export-list-decl``)
- Recipe 125:  Re-exporting is not supported (``arkts-no-reexport``)
- Recipe 126:  ``export = ...`` assignment is not supported (``arkts-no-export-assignment``)


