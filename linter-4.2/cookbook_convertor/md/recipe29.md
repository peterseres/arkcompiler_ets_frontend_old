#  Indexed access is not supported for fields

Rule ``arkts-no-props-by-index``

**Severity: error**

ArkTS does not support indexed access for class fields. Use dot notation
instead.


## TypeScript


```

    class Point {x: number = 0; y: number = 0}
    let p: Point = {x: 1, y: 2}
    let x = p["x"]

```

## ArkTS


```

    class Point {x: number = 0; y: number = 0}
    let p: Point = {x: 1, y: 2}
    let x = p.x

```


