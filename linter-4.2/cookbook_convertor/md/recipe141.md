#  ``readonly T[]`` syntax is not supported

Rule ``arkts-no-readonly-params``

**Severity: error**

Currently ArkTS supports ``readonly`` for properties, but not for parameters.


## TypeScript


```

    function foo(arr: readonly string[]) {
        arr.slice()        // OK
        arr.push("hello!") // Compile-time error
    }

```

## ArkTS


```

    function foo(arr: string[]) {
        arr.slice()        // OK
        arr.push("hello!") // OK
    }

```


