#  Optional properties are not supported for primitive types

Rule ``arkts-no-opt-props``

**Severity: error**

ArkTS does not support optional properties of primitive types. You can use
properties with default values or reference types. For reference types,
non-specified optional property is set to ``null``. This rule applies both to
classes and interfaces.


## TypeScript


```

    class CompilerOptions {
        strict?: boolean
        sourcePath?: string
        targetPath?: string
    }

    let options: CompilerOptions = {
        strict: true,
        sourcePath: "./src"
    }

    if (options.targetPath == undefined) {
        // Some logic
    }

```

## ArkTS


```

    class CompilerOptions {
        strict: boolean = false
        sourcePath: string = ""
        targetPath?: string
    }

    let options: CompilerOptions = {
        strict: true,
        sourcePath: "./src"
        // targetPath is implicitly set to null
    }

    if (options.targetPath == null) {
        // Some logic
    }

```


