#  Dynamic property declaration is not supported

Rule ``arkts-no-dyn-prop-decl``

**Severity: error**

ArkTS does not support dynamic property declaration. All object properties must
be declared immediately in the class. While it can be replaced with an array
of objects, it is still better to adhere to the static language paradigm and
declare fields, their names and types explicitly.


## TypeScript


```

    class Person {
        name: string = ""
        age: number = 0; // semicolon is required here
        [key: string]: string | number
    }

    const person: Person = {
        name: "John",
        age: 30,
        email: "john@example.com",
        phone: 1234567890,
    }

```

## ArkTS


```

    class Person {
        name: string
        age: number
        email: string
        phone: number

        constructor(name: string, age: number, email: string, phone: number) {
            this.name = name
            this.age = age
            this.email = email
            this.phone = phone
        }
    }

    function main(): void {
        const person: Person = new Person("John", 30, "john@example.com", 1234567890)
    }

```

## See also

- Recipe 001:  Objects with property names that are not identifiers are not supported (``arkts-identifiers-as-prop-names``)
- Recipe 002:  ``Symbol()`` API is not supported (``arkts-no-symbol``)
- Recipe 052:  Attempt to access an undefined property is a compile-time error (``arkts-no-undefined-prop-access``)
- Recipe 059:  ``delete`` operator is not supported (``arkts-no-delete``)
- Recipe 060:  ``typeof`` operator is allowed only in expression contexts (``arkts-no-type-query``)
- Recipe 066:  ``in`` operator is not supported (``arkts-no-in``)
- Recipe 105:  Property-based runtime type checks are not supported (``arkts-no-prop-existence-check``)
- Recipe 144:  Usage of standard library is restricted (``arkts-limited-stdlib``)


