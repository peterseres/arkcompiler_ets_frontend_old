#  ``typeof`` operator is allowed only in expression contexts

Rule ``arkts-no-type-query``

**Severity: error**

ArkTS supports ``typeof`` operator only in the expression context. Specifying
type notations using ``typeof`` is not supported.


## TypeScript


```

    let n1 = 42
    let s1 = "foo"
    console.log(typeof n1) // "number"
    console.log(typeof s1) // "string"
    let n2: typeof n1
    let s2: typeof s1

```

## ArkTS


```

    let n1 = 42
    let s1 = "foo"
    console.log(typeof n1) // "number"
    console.log(typeof s1) // "string"
    let n2: number
    let s2: string

```

## See also

- Recipe 001:  Objects with property names that are not identifiers are not supported (``arkts-identifiers-as-prop-names``)
- Recipe 002:  ``Symbol()`` API is not supported (``arkts-no-symbol``)
- Recipe 052:  Attempt to access an undefined property is a compile-time error (``arkts-no-undefined-prop-access``)
- Recipe 059:  ``delete`` operator is not supported (``arkts-no-delete``)
- Recipe 066:  ``in`` operator is not supported (``arkts-no-in``)
- Recipe 105:  Property-based runtime type checks are not supported (``arkts-no-prop-existence-check``)
- Recipe 109:  Dynamic property declaration is not supported (``arkts-no-dyn-prop-decl``)
- Recipe 144:  Usage of standard library is restricted (``arkts-limited-stdlib``)


