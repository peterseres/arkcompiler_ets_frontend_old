#  Re-exporting is not supported

Rule ``arkts-no-reexport``

**Severity: error**

ArkTS does not support re-exporting. All desired entities must be
imported explicitly from the modules that export them.


## TypeScript


```

    // module1
    export class MyClass {
        // ...
    }

    // module2
    export { MyClass } from "module1"

    // consumer module
    import { MyClass } from "module2"

    const myInstance = new MyClass()

```

## ArkTS


```

    // module1
    export class MyClass {
      // ...
    }

    // module2
    // some stuff

    // consumer module
    import { MyClass } from "module1"
    import * from "module2"

    const myInstance = new MyClass()

```

## See also

- Recipe 123:  Renaming in export declarations is not supported (``arkts-no-export-renaming``)
- Recipe 124:  Export list declaration is not supported (``arkts-no-export-list-decl``)
- Recipe 126:  ``export = ...`` assignment is not supported (``arkts-no-export-assignment``)


