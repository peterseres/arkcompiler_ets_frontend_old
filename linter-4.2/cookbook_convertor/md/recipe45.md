#  Lambdas require explicit type annotation for parameters

Rule ``arkts-explicit-param-types-in-lambdas``

**Severity: error**

Currently, ArkTS requires the types of lambda parameters
to be explicitly specified.


## TypeScript


```

    let f = (s) => { // type any is assumed
        console.log(s)
    }

```

## ArkTS


```

    // Explicit types for lambda parameters are mandatory:
    let f = (s: string) => {
        console.log(s)
    }

```


