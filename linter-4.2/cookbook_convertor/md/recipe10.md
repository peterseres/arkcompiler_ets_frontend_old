#  ``bigint`` is not a builtin type, suffix ``n`` for numeric literals is not supported

Rule ``arkts-no-n-suffix``

**Severity: error**

ArkTS supports ``bigint`` as a part of the standard library, not as a builtin
type. ``n`` suffix for numeric literals is not supported, ``BigInt`` factory
function can be used to produce values of ``bigint`` type.


## TypeScript


```

    let a: bigint = 1n

```

## ArkTS


```

    let a = BigInt(1)
    let b: bigint = BigInt(2)

```


