#  Values computed at runtime are not supported in ``case`` statements

Rule ``arkts-no-computed-case``

**Severity: error**

ArkTS supports ``case`` statements that contain only compile-time values.
Use ``if`` statements as an alternative.


## TypeScript


```

    let x = 2
    let y = 3
    switch (x) {
        case 1:
            console.log(1)
            break
        case 2:
            console.log(2)
            break
        case y:
            console.log(y)
            break
        default:
            console.log("other")
    }

```

## ArkTS


```

    let x = 2
    switch (x) {
        case 1:
            console.log(1)
            break
        case 2:
            console.log(2)
            break
        case 3:
            console.log(3)
            break
        default:
            console.log("other")
    }

```


