#  Inference of implied types is not supported

Rule ``arkts-no-implied-inference``

**Severity: error**

Currently, ArkTS does not support inference of implied types. Use explicit
type notation instead. Use ``Object[]`` if you need containers that hold
data of mixed types.


## TypeScript


```

    let [a, b, c] = [1, "hello", true]

```

## ArkTS


```

    let a = 1
    let b = "hello"
    let c = true

    let arr: Object[] = [1, "hello", true]
    let a1 = arr[0]
    let b1 = arr[1]
    let c1 = arr[2]

```


