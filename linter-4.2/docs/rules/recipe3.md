#  Private '#' identifiers are not supported

Rule ``arkts-no-private-identifiers``

**Severity: error**

ArkTS does not private identifiers started with ``#`` symbol, use ``private``
keyword instead.


## TypeScript


```

    class C {
        foo = 1
    }

```

## ArkTS


```

    class C {
        private foo = 1
    }


```


