#  Iterable interfaces are not supported

Rule ``arkts-noiterable``

**Severity: error**

ArkTS does not support the ``Symbol`` API, ``Symbol.iterator`` and
eventually iterable interfaces. Use arrays and library-level containers to
iterate over data.


## See also

- Recipe 002:  ``Symbol()`` API is not supported (``arkts-no-symbol``)
- Recipe 080:  ``for .. in`` is not supported (``arkts-no-for-in``)
- Recipe 082:  ``for-of`` is supported only for arrays and strings (``arkts-for-of-str-arr``)


