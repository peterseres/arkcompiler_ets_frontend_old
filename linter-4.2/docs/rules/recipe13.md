#  Use ``Object[]`` instead of tuples

Rule ``arkts-no-tuples``

**Severity: error**

Currently, ArkTS does not support tuples. You can use arrays of ``Object``
(``Object[]``) to emulate tuples.


## TypeScript


```

    var t: [number, string] = [3, "three"]
    var n = t[0]
    var s = t[1]

```

## ArkTS


```

    let t: Object[] = [3, "three"]
    let n = t[0]
    let s = t[1]

```

## See also

- Recipe 013:  Use ``Object[]`` instead of tuples (``arkts-no-tuples``)


