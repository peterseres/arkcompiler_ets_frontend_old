#  Explicit values for enumeration constants are not supported

Rule ``arkts-no-explicit-enum-init``

**Severity: error**

Currently, ArkTS does not support assigning explicit values for ``enums``.


## TypeScript


```

    enum E {
        A,
        B,
        C = 10,
        D
    }

```

## ArkTS


```

    enum E {
        A,
        B,
        C = 10,  // Compile-time error: assigning out of order values for enums is not supported
        D
    }

    enum E_fixed {
        A,
        B,
        C,   // OK
        D
    }

```


