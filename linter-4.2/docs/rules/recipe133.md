#  Runtime import expressions are not supported

Rule ``arkts-no-runtime-import``

**Severity: error**

ArkTS does not support such "runtime" import expressions as ``await import...``
because in ArkTS import is a compile-time, not a runtime feature. Use regular
import syntax instead.


## TypeScript


```

    const zipUtil = await import("./utils/create-zip-file")

```

## ArkTS


```

    import { zipUtil } from "./utils/create-zip-file"

```

## See also

- Recipe 129:  Wildcards in module names are not supported (``arkts-no-module-wildcards``)
- Recipe 130:  Universal module definitions (UMD) are not supported (``arkts-no-umd``)
- Recipe 143:  Import assertions are not supported (``arkts-no-import-assertions``)


