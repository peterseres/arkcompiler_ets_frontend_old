#  Object literal must correspond to explicitly declared class or interface

Rule ``arkts-no-untyped-obj-literals``

**Severity: error**

ArkTS supports the usage of object literals if the compiler can infer
to what classes or interfaces such literals correspond to.
Otherwise, a compile-time error occurs.

The class or interface can be specified as a type annotation for a variable.


## TypeScript


```

    let x = {f: 1}

```

## ArkTS


```

    class O {
       f: number
    }

    let x: O = {f: 1} // OK
    let y = {f: 1} // Compile-time error, cannot infer object literal type
    let z: Object = {f: 2} // Compile-time error, class 'Object' does not have field 'f'

```

## See also

- Recipe 040:  Object literals cannot be used as type declarations (``arkts-no-obj-literals-as-types``)
- Recipe 043:  Untyped array literals are not supported (``arkts-no-noninferrable-arr-literals``)


