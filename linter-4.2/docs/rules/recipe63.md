#  Binary ``+`` operator supports implicit casts only for numbers and strings

Rule ``arkts-no-polymorphic-plus``

**Severity: error**

ArkTS supports implicit casts for ``+`` only for strings and numbers.
Elsewhere, any form of an explicit cast to string is required.


## TypeScript


```

    enum E { E1, E2 }

    let a = 10 + 32   // 42
    let b = E.E1 + 10 // 10
    let c = 10 + "5"  // "105"

    let d = "5" + E.E2 // "51"
    let e = "Hello, " + "world!" // "Hello, world!"
    let f = "string" + true // "stringtrue"

    let g = (new Object()) + "string" // "[object Object]string"

```

## ArkTS


```

    enum E { E1, E2 }

    let a = 10 + 32   // 42
    let b = E.E1 + 10 // 10
    let c = 10 + "5"  // "105"

    let d = "5" + E.E2 // "51"
    let e = "Hello, " + "world!" // "Hello, world!"
    let f = "string" + true // "stringtrue"

    let g = (new Object()).toString() + "string"

```

## See also

- Recipe 055:  Unary operators ``+``, ``-`` and ``~`` work only on numbers (``arkts-no-polymorphic-unops``)
- Recipe 056:  Unary ``+`` cannot be used for casting to ``number`` (``arkts-no-unary-plus-cast``)
- Recipe 061:  Binary operators ``*``, ``/``, ``%``, ``-``, ``<<``, ``>>``, ``>>>``, ``&``, ``^`` and ``|`` work only on numeric types (``arkts-no-polymorphic-binops``)


