#  ``switch`` statements cannot accept values of arbitrary types

Rule ``arkts-limited-switch``

**Severity: error**

ArkTS supports the values of the types ``char``, ``byte``, ``short``, ``int``,
``long``, ``Char``, ``Byte``, ``Short``, ``Int``, ``Long``, ``String`` or
``enum`` in ``switch`` statements. Use ``if`` statements in other cases.


## TypeScript


```

    class Point {
        x: number = 0
        y: number = 0
    }

    let a = new Point()

    switch (a) {
        case null: break
        default: console.log("not null")
    }

```

## ArkTS


```

    class Point {
        x: number = 0
        y: number = 0
    }

    let a = new Point()

    if (a != null) {
        console.log("not null")
    }

```


