/*
 * Copyright (c) 2023-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import * as ts from 'typescript';
import { AutofixInfo } from './Common';
import { FaultID } from './Problems';
import * as Utils from './Utils';

export const AUTOFIX_ALL: AutofixInfo = {
  problemID: '', start: -1, end: -1
}

export const autofixInfo: AutofixInfo[] = [];

export function shouldAutofix(node: ts.Node, faultID: FaultID): boolean {
  if (autofixInfo.length === 0) return false;
  if (autofixInfo.length === 1 && autofixInfo[0] == AUTOFIX_ALL) return true;
  return autofixInfo.findIndex(
    value => value.start === node.getStart() && value.end === node.getEnd() && value.problemID === FaultID[faultID]
  ) !== -1;
}

export interface Autofix {
  replacementText: string;
  start: number;
  end: number;
}

const printer: ts.Printer = ts.createPrinter({ omitTrailingSemicolon: false, removeComments: false });

function numericLiteral2IdentifierName(numeric: ts.NumericLiteral) {
  return '__' + numeric.getText();
}

function stringLiteral2IdentifierName(str: ts.StringLiteral) {
  let text = (str as ts.StringLiteral).getText();
  return text.substring(1, text.length-1); // cut out starting and ending quoters.
}

function propertyName2IdentifierName(name: ts.PropertyName): string {
  if (name.kind === ts.SyntaxKind.NumericLiteral) 
    return numericLiteral2IdentifierName(name as ts.NumericLiteral);

  if (name.kind === ts.SyntaxKind.StringLiteral) 
    return stringLiteral2IdentifierName(name as ts.StringLiteral);
        
  return '';
}

function indexExpr2IdentifierName(index: ts.Expression) {
  if (index.kind === ts.SyntaxKind.NumericLiteral) 
    return numericLiteral2IdentifierName(index as ts.NumericLiteral);

  if (index.kind === ts.SyntaxKind.StringLiteral) 
    return stringLiteral2IdentifierName(index as ts.StringLiteral);
    
  return '';
}

export function fixLiteralAsPropertyName(node: ts.Node): Autofix[] | undefined {
  if (ts.isPropertyDeclaration(node) || ts.isPropertyAssignment(node)) {
    let propName = (node as (ts.PropertyDeclaration | ts.PropertyAssignment)).name;
    let identName = propertyName2IdentifierName(propName);
    if (identName) 
      return [{ replacementText: identName, start: propName.getStart(), end: propName.getEnd() }];
  }
    
  return undefined;
}

export function fixPropertyAccessByIndex(node: ts.Node): Autofix[] | undefined {
  if (ts.isElementAccessExpression(node)) {
    let elemAccess = node as ts.ElementAccessExpression;
    let identifierName = indexExpr2IdentifierName(elemAccess.argumentExpression);
    if (identifierName) 
      return [{ 
        replacementText: elemAccess.expression.getText() + '.' + identifierName, 
        start: elemAccess.getStart(), end: elemAccess.getEnd() 
      }];
  }
    
  return undefined;
}

export function fixBigIntLiteral(tsBigIntLiteral: ts.BigIntLiteral, isStringArg: boolean): Autofix[] {
  let value = tsBigIntLiteral.getText();
  if (value.endsWith('n')) value = value.substring(0, value.length - 1);

  // Note: BigInt constructor can't parse literal value
  // with underscore chars if ctor argument is a string.
  let newNode = ts.factory.createCallExpression(
    ts.factory.createIdentifier('BigInt'),
    undefined,
    [isStringArg
      ? ts.factory.createStringLiteral(value.replace(/_/g, ''), true)
      : ts.factory.createNumericLiteral(value)]
  );

  return [{
    replacementText: printer.printNode(ts.EmitHint.Unspecified, newNode, tsBigIntLiteral.getSourceFile()),
    start: tsBigIntLiteral.getStart(),
    end: tsBigIntLiteral.getEnd(),
  }];
}

export function fixParamWithoutType(param: ts.ParameterDeclaration, paramType: ts.TypeNode, 
  isFuncExprParam: boolean = false): Autofix | ts.ParameterDeclaration {
  let paramWithType = ts.factory.createParameterDeclaration(
    undefined, undefined, param.dotDotDotToken, param.name, param.questionToken, 
    paramType, param.initializer
  );
  if (isFuncExprParam) return paramWithType;

  let text = printer.printNode(ts.EmitHint.Unspecified, paramWithType, param.getSourceFile());
  return { start: param.getStart(), end: param.getEnd(), replacementText: text };
}

export function fixFunctionExpression(funcExpr: ts.FunctionExpression, 
  params: ts.NodeArray<ts.ParameterDeclaration> = funcExpr.parameters, 
  retType: ts.TypeNode | undefined = funcExpr.type): Autofix {
  let arrowFunc = ts.factory.createArrowFunction(
    undefined, undefined, params, retType, ts.factory.createToken(ts.SyntaxKind.EqualsGreaterThanToken), 
    funcExpr.body
  );
  let text = printer.printNode(ts.EmitHint.Unspecified, arrowFunc, funcExpr.getSourceFile());
  return { start: funcExpr.getStart(), end: funcExpr.getEnd(), replacementText: text };
}

export function fixReturnType(funcLikeDecl: ts.FunctionLikeDeclaration, typeNode: ts.TypeNode): Autofix {
  let text = ': ' + printer.printNode(ts.EmitHint.Unspecified, typeNode, funcLikeDecl.getSourceFile());
  let pos = getReturnTypePosition(funcLikeDecl);
  return { start: pos, end: pos, replacementText: text };
}

function getReturnTypePosition(funcLikeDecl: ts.FunctionLikeDeclaration): number {
  if (funcLikeDecl.body) {
    // Find position of the first node or token that follows parameters.
    // After that, iterate over child nodes in reverse order, until found
    // first closing parenthesis.
    let postParametersPosition = ts.isArrowFunction(funcLikeDecl)
      ? funcLikeDecl.equalsGreaterThanToken.getStart()
      : funcLikeDecl.body.getStart();
    
    const children = funcLikeDecl.getChildren();
    for (let i = children.length - 1; i >= 0; i--) {
      const child = children[i];
      if (child.kind === ts.SyntaxKind.CloseParenToken && child.getEnd() < postParametersPosition)
        return child.getEnd();
    }
  }

  // Shouldn't get here.
  return -1;
}

export function fixCtorParameterProperties(ctorDecl: ts.ConstructorDeclaration, paramTypes: ts.TypeNode[]): Autofix[] | undefined {
  let fieldInitStmts: ts.Statement[] = [];
  let newFieldPos = ctorDecl.getStart();
  let autofixes: Autofix[] = [{ start: newFieldPos, end: newFieldPos, replacementText: '' }];

  for (let i = 0; i < ctorDecl.parameters.length; i++) {
    const param = ctorDecl.parameters[i];

    // Parameter property can not be a destructuring parameter. 
    if (!ts.isIdentifier(param.name)) continue;

    if (Utils.hasAccessModifier(param)) {
      let propIdent = ts.factory.createIdentifier(param.name.text);

      let newFieldNode = ts.factory.createPropertyDeclaration(
        undefined, param.modifiers, propIdent, undefined, paramTypes[i], undefined
      );
      let newFieldText = printer.printNode(ts.EmitHint.Unspecified, newFieldNode, ctorDecl.getSourceFile()) + '\n';
      autofixes[0].replacementText += newFieldText;

      let newParamDecl = ts.factory.createParameterDeclaration(
        undefined, undefined, undefined, param.name, param.questionToken, param.type, param.initializer
      );
      let newParamText = printer.printNode(ts.EmitHint.Unspecified, newParamDecl, ctorDecl.getSourceFile());
      autofixes.push({ start: param.getStart(), end: param.getEnd(), replacementText: newParamText });

      fieldInitStmts.push(ts.factory.createExpressionStatement(ts.factory.createAssignment(
        ts.factory.createPropertyAccessExpression(
          ts.factory.createThis(),
          propIdent,
        ),
        propIdent
      )));
    }
  }

  // Note: Bodyless ctors can't have parameter properties.
  if (ctorDecl.body) {
    let newBody = ts.factory.createBlock(fieldInitStmts.concat(ctorDecl.body.statements), true);
    let newBodyText = printer.printNode(ts.EmitHint.Unspecified, newBody, ctorDecl.getSourceFile());
    autofixes.push({ start: ctorDecl.body.getStart(), end: ctorDecl.body.getEnd(), replacementText: newBodyText });
  }

  return autofixes;
}