/**
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "forOfStatement.h"

#include "plugins/ecmascript/es2panda/binder/scope.h"
#include "plugins/ecmascript/es2panda/compiler/base/iterators.h"
#include "plugins/ecmascript/es2panda/compiler/base/lreference.h"
#include "plugins/ecmascript/es2panda/compiler/core/labelTarget.h"
#include "plugins/ecmascript/es2panda/compiler/core/pandagen.h"
#include "plugins/ecmascript/es2panda/compiler/core/ETSGen.h"
#include "plugins/ecmascript/es2panda/checker/TSchecker.h"
#include "plugins/ecmascript/es2panda/ir/astDump.h"
#include "plugins/ecmascript/es2panda/ir/expression.h"

namespace panda::es2panda::ir {
void ForOfStatement::Iterate(const NodeTraverser &cb) const
{
    cb(left_);
    cb(right_);
    cb(body_);
}

void ForOfStatement::Dump(ir::AstDumper *dumper) const
{
    dumper->Add(
        {{"type", "ForOfStatement"}, {"await", is_await_}, {"left", left_}, {"right", right_}, {"body", body_}});
}

void ForOfStatement::Compile([[maybe_unused]] compiler::PandaGen *pg) const
{
    compiler::LocalRegScope decl_reg_scope(pg, Scope()->DeclScope()->InitScope());

    right_->Compile(pg);

    compiler::LabelTarget label_target(pg);
    auto iterator_type = is_await_ ? compiler::IteratorType::ASYNC : compiler::IteratorType::SYNC;
    compiler::Iterator iterator(pg, this, iterator_type);

    pg->SetLabel(this, label_target.ContinueTarget());

    iterator.Next();
    iterator.Complete();
    pg->BranchIfTrue(this, label_target.BreakTarget());

    iterator.Value();
    pg->StoreAccumulator(this, iterator.NextResult());

    auto lref = compiler::JSLReference::Create(pg, left_, false);

    {
        compiler::IteratorContext for_of_ctx(pg, iterator, label_target);
        pg->LoadAccumulator(this, iterator.NextResult());
        lref.SetValue();

        compiler::LoopEnvScope decl_env_scope(pg, Scope()->DeclScope());
        compiler::LoopEnvScope env_scope(pg, Scope(), {});
        body_->Compile(pg);
    }

    pg->Branch(this, label_target.ContinueTarget());
    pg->SetLabel(this, label_target.BreakTarget());
}

void ForOfStatement::Compile(compiler::ETSGen *etsg) const
{
    etsg->Unimplemented();
}

checker::Type *ForOfStatement::Check([[maybe_unused]] checker::TSChecker *checker)
{
    return nullptr;
}

checker::Type *ForOfStatement::Check([[maybe_unused]] checker::ETSChecker *checker)
{
    return nullptr;
}
}  // namespace panda::es2panda::ir
