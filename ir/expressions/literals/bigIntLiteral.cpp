/**
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "bigIntLiteral.h"

#include "plugins/ecmascript/es2panda/compiler/core/pandagen.h"
#include "plugins/ecmascript/es2panda/checker/TSchecker.h"
#include "plugins/ecmascript/es2panda/ir/astDump.h"

namespace panda::es2panda::ir {
void BigIntLiteral::Iterate([[maybe_unused]] const NodeTraverser &cb) const {}

void BigIntLiteral::Dump(ir::AstDumper *dumper) const
{
    dumper->Add({{"type", "BigIntLiteral"}, {"value", src_}});
}

void BigIntLiteral::Compile(compiler::PandaGen *pg) const
{
    pg->LoadAccumulatorBigInt(this, src_);
}

checker::Type *BigIntLiteral::Check(checker::TSChecker *checker)
{
    auto search = checker->BigintLiteralMap().find(src_);
    if (search != checker->BigintLiteralMap().end()) {
        return search->second;
    }

    auto *new_bigint_literal_type = checker->Allocator()->New<checker::BigintLiteralType>(src_, false);
    checker->BigintLiteralMap().insert({src_, new_bigint_literal_type});
    return new_bigint_literal_type;
}

checker::Type *BigIntLiteral::Check([[maybe_unused]] checker::ETSChecker *checker)
{
    return nullptr;
}
}  // namespace panda::es2panda::ir
