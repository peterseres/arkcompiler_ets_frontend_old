/*
 * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ohos.migrator.test.java;

import * from "std/math/math";
export open class switch_statement  {
    public static ReturnFromSwitch(id : int): String {
        switch (id) {
            case 1:
                return "First";
            case 2:
                return "Second";
            default:
                return "Unknown";
        }

    }
    public static CaseClausesVariations(): void {
        let a : int = 0;
// empty switch
        switch (1) {
        }

// no default clause
        switch (2) {
            case 1:
                a = 21;
                break;
            case 2:
                a = 22;
                break;
        }

// only default case
        switch (3) {
            default:
                a = 31;
                break;
        }

// case clause followed by default
        switch (4) {
            case 1:
                a = 41;
                break;
            case 2:
                break;
            default:
                a = 43;
                break;
        }

// case clause following default clause
        switch (5) {
            default:
                a = 51;
                break;
            case 1:
                a = 52;
                break;
            case 2:
                a = 53;
                break;
        }

// case clauses before and after default clause
        switch (6) {
            case 1:
                a = 61;
                break;
            default:
                a = 62;
                break;
            case 2:
                a = 63;
                break;
        }

// Fall-through
        switch (7) {
            case 1:
            case 2:
                System.out.println("Falling through case 1 and case 2");
                break;
            default:
                System.out.println("Default case");
                break;
        }

// Fall-through
        switch (8) {
            case 1:
            default:
                System.out.println("Falling through both case and default clauses");
                break;
        }

// Fall-through
        switch (9) {
            case 1:
                System.out.println("In case 1: Falling through to default case");
            default:
                System.out.println("In default case");
                break;
            case 2:
                System.out.println("In case 2");
                break;
        }

    }
    public static SwitchWithLocalDeclarations(): void {
        let i : int = 10;
// Local variable is referenced across several case clauses.
        {
            let w : int ; // This declaration is moved in front of switch. Initialization is turned into assignment.
            let e : int ; // This declaration is moved in front of switch. No initialization.
            switch (i) {
                case 0:
                    {
                        let q : int = 5;
                        w = q;
                        break;
                    }
                default:
                    w = 10;
                    e = 20;
                    System.out.println(w + e);
                    break;
            }

        }
// Multiple variables in single variable declaration list.
        {
            let q : int ;
            let r : int ;
            switch (i) {
                case 0:
                    {
                        q = 5;
                        let w : int ; // 'q' and 'r' are moved in front of switch.
                        let e : int = 10;
                        let z : int = 20; // Both 'z' and 'x' declarations are left in this block.
                        let x : int ;
                        System.out.println(q + e + z);
                        break;
                    }
                default:
                    q = 2;
                    r = 4;
                    System.out.println(q + r);
                    break;
            }

        }
// Block variable and hiding.
        {
            let localVar : int ;
            switch (i) {
                case 1:
                    {
                        let localVar : String = "some value"; // 'String localVar' will hide the 'int localVar' in current block scope.
                    }
                    break;
                case 2:
                    localVar = 5;
                    break;
                default:
                    localVar = 6;
                    break;
            }

        }
// Local variable is initialized with expression that can cause side-effects.
// The order of evaluation of variable initializers must be preserved.
        {
            let q : int ;
            let e : int ;
            switch (i) {
                case 0:
                    {
                        q = i++;
                        let w : int = i++;
                        e = i++;
                        break;
                    }
                default:
                    q = 1;
                    e = 2;
                    break;
            }

        }
// Variable 'k' is referenced from nested switch, though, it's still being used
// only within the case clause it was declared in.
        switch (i) {
            case 1:
                {
                    let k : int = 10;
                    switch (i) {
                        case 3:
                            k = 20;
                            break;
                        default:
                            break;
                    }

                    break;
                }
            default:
                break;
        }

// Switch with local class declaration
        switch (i) {
            case 1:
                {
                    open class LocalClass  {
                        open M(): void {
                            System.out.println("LocalClass.M()");
                        }
                    }

                    new LocalClass().M();
                    break;
                }
            default:
                break;
        }

    }
    class Color extends Enum<Color>  {
        public static readonly Red : Color = new Color("Red", 0);
        public static readonly Green : Color = new Color("Green", 1);
        public static readonly Blue : Color = new Color("Blue", 2);
        public static values(): Color[] {
            return [Red, Green, Blue];
        }
        public static valueOf(name : String): Color {
            for (let value : Color of values()){
                if (name == value.toString()) return value;
            }
            return null;
        }
        private constructor(name : String, ordinal : int) {
            super(name, ordinal);
        }

    }

    private static SwitchWithEnumValues(): void {
        let color : Color = Color.Green;
        switch (color) {
            case Red:
                System.out.println("Color is red");
                break;
            case Blue:
                System.out.println("Color is blue");
                break;
            default:
                System.out.println("Color is default");
                break;
        }

    }
}

