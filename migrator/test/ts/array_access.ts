/*
 * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

function test(): void {
   let a = [1,2,3,4,5];

   let a1 = a[1];
   a[2] = 10;

   let b = [1,2,3][2];
   a[b] = a[0];

   let s = foo()[index()];


   let t = tuple();
   a[1] = t[0];

   let c = [1, "string"][0];
}

function foo(): string[] {
   return ["string1", "string2", "string3"];
}

function index(): number {
   return 1;
}

function tuple(): [number, string] {
   return [1, "string"];
}
