/**
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "typeRelationContext.h"
#include "plugins/ecmascript/es2panda/binder/variable.h"
#include "plugins/ecmascript/es2panda/binder/scope.h"
#include "plugins/ecmascript/es2panda/binder/declaration.h"
#include "plugins/ecmascript/es2panda/ir/expressions/arrayExpression.h"
#include "plugins/ecmascript/es2panda/ir/expressions/identifier.h"
#include "plugins/ecmascript/es2panda/ir/ts/tsArrayType.h"
#include "plugins/ecmascript/es2panda/ir/ts/tsTypeParameter.h"

namespace panda::es2panda::checker {
void AssignmentContext::ValidateArrayTypeInitializerByElement(TypeRelation *relation, ir::ArrayExpression *node,
                                                              ETSArrayType *target)
{
    for (uint32_t index = 0; index < node->Elements().size(); index++) {
        ir::Expression *current_array_elem = node->Elements()[index];
        AssignmentContext(relation, current_array_elem,
                          current_array_elem->Check(relation->GetChecker()->AsETSChecker()), target->ElementType(),
                          current_array_elem->Start(),
                          {"Array element at index ", index, " is not compatible with the target array element type."});
    }
}

bool InstantiationContext::ValidateTypeArguments(ETSObjectType *type, ir::TSTypeParameterDeclaration *type_param_decl,
                                                 ir::TSTypeParameterInstantiation *type_args,
                                                 const lexer::SourcePosition &pos)
{
    if (type_param_decl != nullptr && type_args == nullptr) {
        checker_->ThrowTypeError({"Type '", type, "' is generic but type argument were not provided."}, pos);
    }

    if (type_param_decl == nullptr && type_args != nullptr) {
        checker_->ThrowTypeError({"Type '", type, "' is not generic."}, pos);
    }

    if (type_args == nullptr) {
        result_ = type;
        return true;
    }

    ASSERT(type_param_decl != nullptr && type_args != nullptr);
    if (type_param_decl->Params().size() != type_args->Params().size()) {
        checker_->ThrowTypeError({"Type '", type, "' has ", type_param_decl->Params().size(),
                                  " number of type parameters, but ", type_args->Params().size(),
                                  " type arguments were provided."},
                                 pos);
    }

    for (size_t type_param_iter = 0; type_param_iter < type_param_decl->Params().size(); ++type_param_iter) {
        auto *const param_type = type_args->Params().at(type_param_iter)->GetType(checker_);
        checker_->CheckValidGenericTypeParameter(param_type, pos);
        auto *const type_param_constraint =
            type_param_decl->Params().at(type_param_iter)->AsTSTypeParameter()->Constraint();
        if (type_param_constraint == nullptr) {
            continue;
        }

        auto *constraint_type = type_param_constraint->GetType(checker_)->AsETSObjectType();

        if (param_type->IsETSTypeReference()) {
            auto arg_ref = param_type->AsETSTypeReference();

            // TODO(mmartin): make correct check later for multiple bounds
            if ((!arg_ref->GetConstraints().empty()) &&
                checker_->Relation()->IsIdenticalTo(arg_ref->GetConstraints()[0], constraint_type)) {
                continue;
            }
        }

        auto *arg_ref_type =
            checker_->AsETSChecker()->GetReferredTypeFromETSTypeReference(param_type)->AsETSObjectType();

        if (const auto *const found = checker_->AsETSChecker()->Scope()->FindLocal(
                constraint_type->Name(), binder::ResolveBindingOptions::TYPE_ALIASES);
            found != nullptr) {
            arg_ref_type = found->TsType()->AsETSObjectType();
        }

        auto assignable = checker_->Relation()->IsAssignableTo(arg_ref_type, constraint_type);
        if (constraint_type->HasObjectFlag(ETSObjectFlags::INTERFACE)) {
            for (const auto *const interface : arg_ref_type->Interfaces()) {
                // TODO(mmartin): make correct check later for multiple bounds
                assignable = (interface == constraint_type) || assignable;
            }
        }

        if (!assignable) {
            checker_->ThrowTypeError(
                {"Type '", arg_ref_type, "' is not assignable to constraint type '", constraint_type, "'."},
                type_args->Params().at(type_param_iter)->Start());
        }
    }

    return false;
}

void InstantiationContext::InstantiateType(ETSObjectType *type, ir::TSTypeParameterDeclaration *type_param_decl,
                                           ir::TSTypeParameterInstantiation *type_args)
{
    ArenaVector<Type *> type_arg_types(checker_->Allocator()->Adapter());
    type_arg_types.reserve(type_args->Params().size());

    auto flags = ETSObjectFlags::NO_OPTS;

    for (auto *const it : type_args->Params()) {
        auto *param_type = it->GetType(checker_)->Instantiate(checker_->Allocator(), checker_->Relation(),
                                                              checker_->GetGlobalTypesHolder());

        if (param_type->HasTypeFlag(TypeFlag::ETS_PRIMITIVE)) {
            checker_->Relation()->SetNode(it);
            auto *const boxed_type_arg = checker_->PrimitiveTypeAsETSBuiltinType(param_type);
            ASSERT(boxed_type_arg);
            param_type = boxed_type_arg->Instantiate(checker_->Allocator(), checker_->Relation(),
                                                     checker_->GetGlobalTypesHolder());
        }

        param_type->AddTypeFlag(TypeFlag::GENERIC);
        type_arg_types.push_back(param_type);

        if (param_type->IsETSTypeReference()) {
            flags |= ETSObjectFlags::INCOMPLETE_INSTANTIATION;
        }
    }

    InstantiateType(type, type_param_decl, type_arg_types);
    result_->AddObjectFlag(flags);
}

void InstantiationContext::InstantiateType(ETSObjectType *type, ir::TSTypeParameterDeclaration *type_param_decl,
                                           ArenaVector<Type *> &type_arg_types)
{
    util::StringView hash = checker_->GetHashFromTypeArguments(type_arg_types);

    checker::ScopeContext scope_ctx(checker_, type_param_decl->Scope());

    ArenaVector<Type *> saved_decl_param_types(checker_->Allocator()->Adapter());

    // TODO(mmartin): are type_arg_vars_ needed?
    for (size_t idx = 0; idx < type_param_decl->Params().size(); idx++) {
        auto *const param_var = type_param_decl->Params()[idx]->Name()->Variable()->AsLocalVariable();
        auto *const type_param = param_var->TsType()->AsETSTypeParameter();

        saved_decl_param_types.emplace_back(type_param->GetType());
        type_param->SetType(type_arg_types[idx]);
        type_arg_vars_.push_back(param_var);
    }

    auto saved_type_args = type->TypeArguments();

    if (type->Variable() != nullptr) {
        checker_->SubstituteGenericTypeDeclTypeParams(type);
    }

    result_ = type->Instantiate(checker_->Allocator(), checker_->Relation(), checker_->GetGlobalTypesHolder())
                  ->AsETSObjectType();

    type->SetTypeArguments(std::move(saved_type_args));
    for (size_t idx = 0; idx < type_param_decl->Params().size(); idx++) {
        const auto *const param_var = type_param_decl->Params()[idx]->Name()->Variable()->AsLocalVariable();
        auto *const type_param = param_var->TsType()->AsETSTypeParameter();

        type_param->SetType(saved_decl_param_types[idx]);
    }

    type->GetInstantiationMap().try_emplace(hash, result_);
    result_->AddTypeFlag(TypeFlag::GENERIC);
    result_->SetTypeArguments(std::move(type_arg_types));
    type_arg_vars_.clear();
}
}  // namespace panda::es2panda::checker
