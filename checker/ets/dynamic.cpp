/**
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "plugins/ecmascript/es2panda/checker/ETSchecker.h"

#include "plugins/ecmascript/es2panda/binder/scope.h"
#include "plugins/ecmascript/es2panda/binder/declaration.h"
#include "plugins/ecmascript/es2panda/binder/binder.h"
#include "plugins/ecmascript/es2panda/binder/ETSBinder.h"
#include "plugins/ecmascript/es2panda/ir/base/classProperty.h"
#include "plugins/ecmascript/es2panda/ir/base/classStaticBlock.h"
#include "plugins/ecmascript/es2panda/ir/base/methodDefinition.h"
#include "plugins/ecmascript/es2panda/ir/base/scriptFunction.h"
#include "plugins/ecmascript/es2panda/ir/expressions/assignmentExpression.h"
#include "plugins/ecmascript/es2panda/ir/expressions/callExpression.h"
#include "plugins/ecmascript/es2panda/ir/expressions/functionExpression.h"
#include "plugins/ecmascript/es2panda/ir/expressions/identifier.h"
#include "plugins/ecmascript/es2panda/ir/expressions/memberExpression.h"
#include "plugins/ecmascript/es2panda/ir/statements/blockStatement.h"
#include "plugins/ecmascript/es2panda/ir/statements/classDeclaration.h"
#include "plugins/ecmascript/es2panda/ir/statements/expressionStatement.h"
#include "plugins/ecmascript/es2panda/parser/program/program.h"
#include "plugins/ecmascript/es2panda/util/helpers.h"
#include "generated/signatures.h"

namespace panda::es2panda::checker {

ir::Identifier *ETSChecker::AddParam(binder::FunctionParamScope *param_scope, util::StringView name,
                                     checker::Type *type)
{
    auto param_ctx = binder::LexicalScope<binder::FunctionParamScope>::Enter(Binder(), param_scope, false);
    auto *param_id = AllocNode<ir::Identifier>(name, Allocator());
    auto *param_var = std::get<1>(Binder()->AddParamDecl(param_id));
    param_var->SetTsType(type);
    param_id->SetVariable(param_var);
    return param_id;
}

static bool IsByValueCall(ir::Expression *callee)
{
    if (callee->IsMemberExpression()) {
        return false;
    }

    if (callee->IsETSTypeReference()) {
        return false;
    }

    auto *var = callee->AsIdentifier()->Variable();
    auto *import = util::Helpers::ImportDeclarationForDynamicVar(var);
    if (import != nullptr) {
        auto *decl = var->Declaration()->Node();
        if (decl->IsImportSpecifier()) {
            return false;
        }
    }

    return true;
}

ir::ScriptFunction *ETSChecker::CreateDynamicCallIntrinsic(ir::Expression *callee,
                                                           const ArenaVector<ir::Expression *> &arguments)
{
    auto *name = AllocNode<ir::Identifier>("invoke", Allocator());
    auto *param_scope = Allocator()->New<binder::FunctionParamScope>(Allocator(), nullptr);
    auto *scope = Allocator()->New<binder::FunctionScope>(Allocator(), param_scope);

    ArenaVector<ir::Expression *> params(Allocator()->Adapter());

    auto *info = CreateSignatureInfo();
    info->min_arg_count = arguments.size() + 2;

    auto *obj_param = AddParam(param_scope, "obj", callee->TsType());
    params.push_back(obj_param);
    info->params.push_back(obj_param->Variable()->AsLocalVariable());

    ir::Identifier *param2;
    if (!IsByValueCall(callee)) {
        param2 = AddParam(param_scope, "qname", GlobalETSStringLiteralType());
    } else {
        auto lang = callee->TsType()->AsETSDynamicType()->Language();
        param2 = AddParam(param_scope, "this", GlobalBuiltinDynamicType(lang));
    }

    params.push_back(param2);
    info->params.push_back(param2->Variable()->AsLocalVariable());

    for (size_t i = 0; i < arguments.size(); i++) {
        util::UString param_name("p" + std::to_string(i), Allocator());
        auto *param = AddParam(param_scope, param_name.View(), arguments[i]->TsType());
        params.push_back(param);
        info->params.push_back(param->Variable()->AsLocalVariable());
    }

    auto *func = AllocNode<ir::ScriptFunction>(scope, std::move(params), nullptr, nullptr, nullptr,
                                               ir::ScriptFunctionFlags::METHOD, ir::ModifierFlags::NONE, false);

    scope->BindNode(func);
    param_scope->BindNode(func);
    scope->BindParamScope(param_scope);
    param_scope->BindFunctionScope(scope);

    func->SetIdent(name);

    auto *signature = CreateSignature(info, callee->TsType(), func);
    signature->AddSignatureFlag(SignatureFlags::STATIC);

    func->SetSignature(signature);

    return func;
}

Signature *ETSChecker::ResolveDynamicCallExpression(ir::Expression *callee,
                                                    const ArenaVector<ir::Expression *> &arguments, bool is_construct)
{
    ASSERT(callee->TsType()->IsETSDynamicType());

    auto &dynamic_intrinsics = *DynamicCallIntrinsics(is_construct);

    auto lang = callee->TsType()->AsETSDynamicType()->Language();
    auto map_it = dynamic_intrinsics.find(lang);

    if (map_it == dynamic_intrinsics.cend()) {
        std::tie(map_it, std::ignore) = dynamic_intrinsics.emplace(lang, Allocator()->Adapter());
    }

    auto &map = map_it->second;

    std::stringstream ss;
    ss << "dyncall";
    if (IsByValueCall(callee)) {
        ss << "-byvalue";
    }
    for (auto *arg : arguments) {
        auto *type = arg->Check(this);
        ss << "-";
        type->ToString(ss);
    }

    auto key = ss.str();
    auto it = map.find(util::StringView(key));
    if (it == map.end()) {
        auto *func = CreateDynamicCallIntrinsic(callee, arguments);
        map.emplace(util::UString(key, Allocator()).View(), func);
        return func->Signature();
    }

    return it->second->Signature();
}

ir::ClassStaticBlock *ETSChecker::CreateClassInitializer(binder::ClassScope *class_scope,
                                                         const ClassInitializerBuilder &builder)
{
    auto class_ctx = binder::LexicalScope<binder::LocalScope>::Enter(Binder(), class_scope->StaticMethodScope());

    ArenaVector<ir::Expression *> params(Allocator()->Adapter());

    auto *param_scope = Allocator()->New<binder::FunctionParamScope>(Allocator(), class_scope);
    auto *scope = Allocator()->New<binder::FunctionScope>(Allocator(), param_scope);

    auto *id = AllocNode<ir::Identifier>(compiler::Signatures::CCTOR, Allocator());

    ArenaVector<ir::Statement *> statements(Allocator()->Adapter());

    builder(scope, &statements);

    auto *body = AllocNode<ir::BlockStatement>(scope, std::move(statements));

    auto *func = AllocNode<ir::ScriptFunction>(
        scope, std::move(params), nullptr, body, nullptr,
        ir::ScriptFunctionFlags::STATIC_BLOCK | ir::ScriptFunctionFlags::EXPRESSION, ir::ModifierFlags::STATIC, false);
    scope->BindNode(func);
    func->SetIdent(id);
    param_scope->BindNode(func);
    scope->BindParamScope(param_scope);
    param_scope->BindFunctionScope(scope);

    auto *signature_info = CreateSignatureInfo();
    signature_info->rest_var = nullptr;
    auto *signature = CreateSignature(signature_info, GlobalVoidType(), func);
    func->SetSignature(signature);

    auto *func_expr = AllocNode<ir::FunctionExpression>(func);
    auto *static_block = AllocNode<ir::ClassStaticBlock>(func_expr, Allocator());
    static_block->AddModifier(ir::ModifierFlags::STATIC);

    Binder()->AsETSBinder()->BuildInternalName(func);
    Binder()->AsETSBinder()->BuildFunctionName(func);
    Binder()->Functions().push_back(func->Scope());

    return static_block;
}

ir::ClassStaticBlock *ETSChecker::CreateDynamicCallClassInitializer(binder::ClassScope *class_scope, Language lang,
                                                                    bool is_construct)
{
    return CreateClassInitializer(class_scope, [this, lang, is_construct](binder::FunctionScope *scope,
                                                                          ArenaVector<ir::Statement *> *statements) {
        auto [builtin_class_name, builtin_method_name] =
            util::Helpers::SplitSignature(is_construct ? compiler::Signatures::Dynamic::InitNewClassBuiltin(lang)
                                                       : compiler::Signatures::Dynamic::InitCallClassBuiltin(lang));

        auto *class_id = AllocNode<ir::Identifier>(builtin_class_name, Allocator());
        auto *method_id = AllocNode<ir::Identifier>(builtin_method_name, Allocator());
        auto *callee = AllocNode<ir::MemberExpression>(class_id, method_id, ir::MemberExpressionKind::PROPERTY_ACCESS,
                                                       false, false);

        ArenaVector<ir::Expression *> call_params(Allocator()->Adapter());

        std::stringstream ss;
        auto name = is_construct ? compiler::Signatures::Dynamic::NewClass(lang)
                                 : compiler::Signatures::Dynamic::CallClass(lang);
        auto package = Binder()->Program()->GetPackageName();

        ss << compiler::Signatures::CLASS_REF_BEGIN;
        if (!package.Empty()) {
            std::string package_str(package);
            std::replace(package_str.begin(), package_str.end(), *compiler::Signatures::METHOD_SEPARATOR.begin(),
                         *compiler::Signatures::NAMESPACE_SEPARATOR.begin());
            ss << package_str << compiler::Signatures::NAMESPACE_SEPARATOR;
        }
        ss << name << compiler::Signatures::MANGLE_SEPARATOR;

        auto *class_name = AllocNode<ir::StringLiteral>(util::UString(ss.str(), Allocator()).View());
        call_params.push_back(class_name);

        auto *init_call = AllocNode<ir::CallExpression>(callee, std::move(call_params), nullptr, false);

        {
            ScopeContext ctx(this, scope);
            init_call->Check(this);
        }

        statements->push_back(AllocNode<ir::ExpressionStatement>(init_call));
    });
}

void ETSChecker::BuildClass(util::StringView name, const ClassBuilder &builder)
{
    auto *class_id = AllocNode<ir::Identifier>(name, Allocator());
    auto [decl, var] = Binder()->NewVarDecl<binder::ClassDecl>(class_id->Start(), class_id->Name());
    class_id->SetVariable(var);

    auto class_ctx = binder::LexicalScope<binder::ClassScope>(Binder());

    auto *class_def =
        AllocNode<ir::ClassDefinition>(Allocator(), class_ctx.GetScope(), class_id,
                                       ir::ClassDefinitionModifiers::DECLARATION, ir::ModifierFlags::NONE);

    auto *class_def_type = Allocator()->New<checker::ETSObjectType>(
        Allocator(), class_def->Ident()->Name(), class_def->Ident()->Name(), class_def, checker::ETSObjectFlags::CLASS);
    class_def->SetTsType(class_def_type);

    auto *class_decl = AllocNode<ir::ClassDeclaration>(class_def, Allocator());
    class_decl->SetParent(Binder()->TopScope()->Node());
    class_def->Scope()->BindNode(class_decl);
    decl->BindNode(class_def);

    Binder()->Program()->Ast()->Statements().push_back(class_decl);

    binder::BoundContext bound_ctx(Binder()->AsETSBinder()->GetGlobalRecordTable(), class_def);

    ArenaVector<ir::AstNode *> class_body(Allocator()->Adapter());

    builder(class_ctx.GetScope(), &class_body);

    class_def->AddProperties(std::move(class_body));
}

void ETSChecker::BuildDynamicCallClass(bool is_construct)
{
    auto &dynamic_intrinsics = *DynamicCallIntrinsics(is_construct);

    if (dynamic_intrinsics.empty()) {
        return;
    }

    for (auto &entry : dynamic_intrinsics) {
        auto lang = entry.first;
        auto &intrinsics = entry.second;
        auto class_name = is_construct ? compiler::Signatures::Dynamic::NewClass(lang)
                                       : compiler::Signatures::Dynamic::CallClass(lang);
        BuildClass(class_name, [this, lang, &intrinsics, is_construct](binder::ClassScope *scope,
                                                                       ArenaVector<ir::AstNode *> *class_body) {
            for (auto &[_, func] : intrinsics) {
                (void)_;

                func->Scope()->ParamScope()->SetParent(scope);

                auto *func_expr = AllocNode<ir::FunctionExpression>(func);

                auto *method = AllocNode<ir::MethodDefinition>(ir::MethodDefinitionKind::METHOD, func->Id(), func_expr,
                                                               ir::ModifierFlags::PUBLIC | ir::ModifierFlags::NATIVE |
                                                                   ir::ModifierFlags::STATIC,
                                                               Allocator(), false);

                Binder()->AsETSBinder()->BuildInternalName(func);
                Binder()->AsETSBinder()->BuildFunctionName(func);

                class_body->push_back(method);
            }

            class_body->push_back(CreateDynamicCallClassInitializer(scope, lang, is_construct));
        });
    }
}

ir::ClassStaticBlock *ETSChecker::CreateDynamicModuleClassInitializer(
    binder::ClassScope *class_scope, const std::vector<ir::ETSImportDeclaration *> &imports)
{
    return CreateClassInitializer(class_scope, [this, imports](binder::FunctionScope *scope,
                                                               ArenaVector<ir::Statement *> *statements) {
        for (auto *import : imports) {
            auto builtin = compiler::Signatures::Dynamic::LoadModuleBuiltin(import->Language());
            auto [builtin_class_name, builtin_method_name] = util::Helpers::SplitSignature(builtin);

            auto *class_id = AllocNode<ir::Identifier>(builtin_class_name, Allocator());
            auto *method_id = AllocNode<ir::Identifier>(builtin_method_name, Allocator());
            auto *callee = AllocNode<ir::MemberExpression>(class_id, method_id,
                                                           ir::MemberExpressionKind::PROPERTY_ACCESS, false, false);

            ArenaVector<ir::Expression *> call_params(Allocator()->Adapter());
            call_params.push_back(import->ResolvedSource());

            auto *load_call = AllocNode<ir::CallExpression>(callee, std::move(call_params), nullptr, false);

            auto *module_class_id = AllocNode<ir::Identifier>(compiler::Signatures::DYNAMIC_MODULE_CLASS, Allocator());
            auto *field_id = AllocNode<ir::Identifier>(import->AssemblerName(), Allocator());
            auto *property = AllocNode<ir::MemberExpression>(module_class_id, field_id,
                                                             ir::MemberExpressionKind::PROPERTY_ACCESS, false, false);

            auto *initializer =
                AllocNode<ir::AssignmentExpression>(property, load_call, lexer::TokenType::PUNCTUATOR_SUBSTITUTION);

            {
                ScopeContext ctx(this, scope);
                initializer->Check(this);
            }

            statements->push_back(AllocNode<ir::ExpressionStatement>(initializer));
        }
    });
}

ir::MethodDefinition *ETSChecker::CreateDynamicModuleClassInitMethod(binder::ClassScope *class_scope)
{
    auto class_ctx = binder::LexicalScope<binder::LocalScope>::Enter(Binder(), class_scope->StaticMethodScope());
    ArenaVector<ir::Expression *> params(Allocator()->Adapter());
    auto *param_scope = Allocator()->New<binder::FunctionParamScope>(Allocator(), class_scope);
    auto *scope = Allocator()->New<binder::FunctionScope>(Allocator(), param_scope);
    auto *id = AllocNode<ir::Identifier>(compiler::Signatures::DYNAMIC_MODULE_CLASS_INIT, Allocator());

    ArenaVector<ir::Statement *> statements(Allocator()->Adapter());
    auto *body = AllocNode<ir::BlockStatement>(scope, std::move(statements));

    auto *func = AllocNode<ir::ScriptFunction>(scope, std::move(params), nullptr, body, nullptr,
                                               ir::ScriptFunctionFlags::METHOD, ir::ModifierFlags::STATIC, false);
    scope->BindNode(func);
    func->SetIdent(id);
    param_scope->BindNode(func);
    scope->BindParamScope(param_scope);
    param_scope->BindFunctionScope(scope);

    auto *signature_info = CreateSignatureInfo();
    signature_info->rest_var = nullptr;
    auto *signature = CreateSignature(signature_info, GlobalVoidType(), func);
    signature->AddSignatureFlag(SignatureFlags::STATIC);
    func->SetSignature(signature);

    auto *func_expr = AllocNode<ir::FunctionExpression>(func);
    auto *method =
        AllocNode<ir::MethodDefinition>(ir::MethodDefinitionKind::METHOD, func->Id(), func_expr,
                                        ir::ModifierFlags::PUBLIC | ir::ModifierFlags::STATIC, Allocator(), false);

    Binder()->AsETSBinder()->BuildInternalName(func);
    Binder()->AsETSBinder()->BuildFunctionName(func);
    Binder()->Functions().push_back(func->Scope());

    auto *decl = Allocator()->New<binder::LetDecl>(id->Name());
    decl->BindNode(method);

    auto *func_type = CreateETSFunctionType(signature, id->Name());
    auto *var = scope->AddDecl(Allocator(), decl, Binder()->Extension());
    var->SetTsType(func_type);
    var->AddFlag(binder::VariableFlags::PROPERTY);
    func->Id()->SetVariable(var);

    auto *class_type = class_scope->Node()->AsClassDeclaration()->Definition()->TsType()->AsETSObjectType();
    class_type->AddProperty<PropertyType::STATIC_METHOD>(var->AsLocalVariable());

    return method;
}

void ETSChecker::EmitDynamicModuleClassInitCall()
{
    auto *global_class = Binder()->Program()->GlobalClass();
    auto &body = global_class->Body();
    auto it = std::find_if(body.begin(), body.end(), [](ir::AstNode *node) { return node->IsClassStaticBlock(); });

    ASSERT(it != body.end());

    auto *static_block = (*it)->AsClassStaticBlock();
    auto *cctor_body = static_block->Function()->Body()->AsBlockStatement();

    auto *class_id = AllocNode<ir::Identifier>(compiler::Signatures::DYNAMIC_MODULE_CLASS, Allocator());
    auto *method_id = AllocNode<ir::Identifier>(compiler::Signatures::DYNAMIC_MODULE_CLASS_INIT, Allocator());
    auto *callee =
        AllocNode<ir::MemberExpression>(class_id, method_id, ir::MemberExpressionKind::PROPERTY_ACCESS, false, false);

    ArenaVector<ir::Expression *> call_params(Allocator()->Adapter());
    auto *init_call = AllocNode<ir::CallExpression>(callee, std::move(call_params), nullptr, false);

    {
        ScopeContext ctx(this, cctor_body->Scope());
        init_call->Check(this);
    }

    cctor_body->Statements().push_back(AllocNode<ir::ExpressionStatement>(init_call));
}

void ETSChecker::BuildDynamicImportClass()
{
    auto dynamic_imports = Binder()->AsETSBinder()->DynamicImports();
    if (dynamic_imports.empty()) {
        return;
    }

    BuildClass(compiler::Signatures::DYNAMIC_MODULE_CLASS,
               [this, dynamic_imports](binder::ClassScope *scope, ArenaVector<ir::AstNode *> *class_body) {
                   std::unordered_set<util::StringView> fields;
                   std::vector<ir::ETSImportDeclaration *> imports;

                   auto *class_type = scope->Node()->AsClassDeclaration()->Definition()->TsType()->AsETSObjectType();

                   for (auto *import : dynamic_imports) {
                       auto source = import->Source()->Str();
                       if (fields.find(source) != fields.cend()) {
                           continue;
                       }

                       auto assembly_name = std::string(source);
                       std::replace_if(
                           assembly_name.begin(), assembly_name.end(), [](char c) { return std::isalnum(c) == 0; },
                           '_');
                       assembly_name.append(std::to_string(fields.size()));

                       import->AssemblerName() = util::UString(assembly_name, Allocator()).View();
                       fields.insert(import->AssemblerName());
                       imports.push_back(import);

                       auto *field_ident = AllocNode<ir::Identifier>(import->AssemblerName(), Allocator());
                       auto *field = AllocNode<ir::ClassProperty>(field_ident, nullptr, nullptr,
                                                                  ir::ModifierFlags::STATIC | ir::ModifierFlags::PUBLIC,
                                                                  Allocator(), false);
                       field->SetTsType(GlobalBuiltinDynamicType(import->Language()));

                       auto *decl = Allocator()->New<binder::LetDecl>(field_ident->Name());
                       decl->BindNode(field);

                       auto *var = scope->AddDecl(Allocator(), decl, Binder()->Extension());
                       var->AddFlag(binder::VariableFlags::PROPERTY);
                       field_ident->SetVariable(var);

                       class_type->AddProperty<PropertyType::STATIC_FIELD>(var->AsLocalVariable());

                       class_body->push_back(field);
                   }

                   class_body->push_back(CreateDynamicModuleClassInitializer(scope, imports));
                   class_body->push_back(CreateDynamicModuleClassInitMethod(scope));
               });

    EmitDynamicModuleClassInitCall();
}

}  // namespace panda::es2panda::checker
