/**
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "signature.h"

#include "plugins/ecmascript/es2panda/binder/scope.h"
#include "plugins/ecmascript/es2panda/ir/base/scriptFunction.h"
#include "plugins/ecmascript/es2panda/ir/ts/tsTypeParameter.h"
#include "plugins/ecmascript/es2panda/checker/ETSchecker.h"

namespace panda::es2panda::checker {

util::StringView Signature::InternalName() const
{
    return internal_name_.Empty() ? func_->Scope()->InternalName() : internal_name_;
}

Signature *Signature::Copy(ArenaAllocator *allocator, TypeRelation *relation, GlobalTypesHolder *global_types)
{
    SignatureInfo *copied_info = allocator->New<SignatureInfo>(signature_info_, allocator);

    for (size_t idx = 0; idx < signature_info_->params.size(); idx++) {
        auto *const param_type = signature_info_->params[idx]->TsType();
        if (param_type->HasTypeFlag(TypeFlag::GENERIC) && param_type->IsETSObjectType()) {
            copied_info->params[idx]->SetTsType(param_type->Instantiate(allocator, relation, global_types));
            auto original_type_args = relation->GetChecker()
                                          ->AsETSChecker()
                                          ->GetOriginalBaseType(param_type->AsETSObjectType())
                                          ->TypeArguments();
            copied_info->params[idx]->TsType()->AsETSObjectType()->SetTypeArguments(std::move(original_type_args));
        } else if (param_type->IsETSTypeReference()) {
            auto *const ets_checker = relation->GetChecker()->AsETSChecker();
            auto *const ref_param_type = param_type->AsETSTypeReference();
            auto *const ref_type_param = ets_checker->CreateTypeParameter(ref_param_type->GetAssemblerRef());
            ref_type_param->SetType(ref_param_type->Ref()->Instantiate(allocator, relation, global_types));
            auto *const copied_type_ref = ets_checker->CreateTypeReference(
                ref_type_param->GetTypeRef(), ref_type_param->GetAssemblerTypeRef(), ref_param_type->VarRef());
            copied_info->params[idx]->SetTsType(copied_type_ref);
        } else {
            copied_info->params[idx]->SetTsType(
                ETSChecker::TryToInstantiate(param_type, allocator, relation, global_types));
        }
    }

    Type *copied_return_type = return_type_;
    if (return_type_->IsETSTypeReference()) {
        const auto func_type_param = relation->GetChecker()->AsETSChecker()->CheckIfTypeReferenceIsInTypeParamList(
            func_->TypeParams(), return_type_->AsETSTypeReference());

        if (!func_type_param) {
            copied_return_type = return_type_->Instantiate(allocator, relation, global_types);
            copied_return_type = copied_return_type->Instantiate(allocator, relation, global_types);
            if (copied_return_type->HasTypeFlag(TypeFlag::GENERIC) && copied_return_type->IsETSObjectType()) {
                auto original_type_args = relation->GetChecker()
                                              ->AsETSChecker()
                                              ->GetOriginalBaseType(copied_return_type->AsETSObjectType())
                                              ->TypeArguments();
                copied_return_type->AsETSObjectType()->SetTypeArguments(std::move(original_type_args));
            }
        }
    } else {
        copied_return_type = ETSChecker::TryToInstantiate(return_type_, allocator, relation, global_types);
    }

    auto *const copied_signature = allocator->New<Signature>(copied_info, copied_return_type, func_);
    copied_signature->flags_ = flags_;
    copied_signature->internal_name_ = internal_name_;
    copied_signature->owner_obj_ = owner_obj_;
    copied_signature->owner_var_ = owner_var_;

    return copied_signature;
}

void Signature::ToString(std::stringstream &ss, const binder::Variable *variable, bool print_as_method) const
{
    ss << "(";

    for (auto it = signature_info_->params.begin(); it != signature_info_->params.end(); it++) {
        ss << (*it)->Name();

        if ((*it)->HasFlag(binder::VariableFlags::OPTIONAL)) {
            ss << "?";
        }

        ss << ": ";

        (*it)->TsType()->ToString(ss);

        if (std::next(it) != signature_info_->params.end()) {
            ss << ", ";
        }
    }

    if (signature_info_->rest_var != nullptr) {
        if (!signature_info_->params.empty()) {
            ss << ", ";
        }

        ss << "...";
        ss << signature_info_->rest_var->Name();
        ss << ": ";
        signature_info_->rest_var->TsType()->ToString(ss);
        ss << "[]";
    }

    ss << ")";

    if (print_as_method || (variable != nullptr && variable->HasFlag(binder::VariableFlags::METHOD))) {
        ss << ": ";
    } else {
        ss << " => ";
    }

    return_type_->ToString(ss);
}

void Signature::Identical(TypeRelation *relation, Signature *other)
{
    if (signature_info_->min_arg_count != other->MinArgCount() ||
        signature_info_->params.size() != other->Params().size()) {
        relation->Result(false);
        return;
    }

    if (relation->NoReturnTypeCheck()) {
        relation->Result(true);
    } else {
        relation->IsIdenticalTo(return_type_, other->ReturnType());
    }

    if (relation->IsTrue()) {
        for (uint64_t i = 0; i < signature_info_->params.size(); i++) {
            const auto *const ets_checker = relation->GetChecker()->AsETSChecker();
            auto *const this_sig_param_type =
                ets_checker->GetReferredTypeFromETSTypeReference(signature_info_->params[i]->TsType());
            auto *const other_sig_param_type =
                ets_checker->GetReferredTypeFromETSTypeReference(other->Params()[i]->TsType());
            if (!CheckFunctionalInterfaces(relation, this_sig_param_type, other_sig_param_type)) {
                relation->IsIdenticalTo(this_sig_param_type, other_sig_param_type);
            }

            if (!relation->IsTrue()) {
                return;
            }
        }

        if (signature_info_->rest_var != nullptr && other->RestVar() != nullptr) {
            relation->IsIdenticalTo(signature_info_->rest_var->TsType(), other->RestVar()->TsType());
        } else if ((signature_info_->rest_var != nullptr && other->RestVar() == nullptr) ||
                   (signature_info_->rest_var == nullptr && other->RestVar() != nullptr)) {
            relation->Result(false);
        }
    }
}

bool Signature::CheckFunctionalInterfaces(TypeRelation *relation, Type *source, Type *target)
{
    if (!source->IsETSObjectType() || !source->AsETSObjectType()->HasObjectFlag(ETSObjectFlags::FUNCTIONAL_INTERFACE)) {
        return false;
    }

    if (!target->IsETSObjectType() || !target->AsETSObjectType()->HasObjectFlag(ETSObjectFlags::FUNCTIONAL_INTERFACE)) {
        return false;
    }

    auto source_invoke_func = source->AsETSObjectType()
                                  ->GetProperty(util::StringView("invoke"), PropertySearchFlags::SEARCH_INSTANCE_METHOD)
                                  ->TsType()
                                  ->AsETSFunctionType()
                                  ->CallSignatures()[0];

    auto target_invoke_func = target->AsETSObjectType()
                                  ->GetProperty(util::StringView("invoke"), PropertySearchFlags::SEARCH_INSTANCE_METHOD)
                                  ->TsType()
                                  ->AsETSFunctionType()
                                  ->CallSignatures()[0];

    relation->IsIdenticalTo(source_invoke_func, target_invoke_func);
    return true;
}

void Signature::AssignmentTarget(TypeRelation *relation, Signature *source)
{
    if (signature_info_->rest_var == nullptr &&
        (source->Params().size() - source->OptionalArgCount()) > signature_info_->params.size()) {
        relation->Result(false);
        return;
    }

    for (size_t i = 0; i < source->Params().size(); i++) {
        if (signature_info_->rest_var == nullptr && i >= Params().size()) {
            break;
        }

        if (signature_info_->rest_var != nullptr) {
            relation->IsAssignableTo(source->Params()[i]->TsType(), signature_info_->rest_var->TsType());

            if (!relation->IsTrue()) {
                return;
            }

            continue;
        }

        relation->IsAssignableTo(source->Params()[i]->TsType(), Params()[i]->TsType());

        if (!relation->IsTrue()) {
            return;
        }
    }

    relation->IsAssignableTo(source->ReturnType(), return_type_);

    if (relation->IsTrue() && signature_info_->rest_var != nullptr && source->RestVar() != nullptr) {
        relation->IsAssignableTo(source->RestVar()->TsType(), signature_info_->rest_var->TsType());
    }
}
}  // namespace panda::es2panda::checker
