/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "etsObjectType.h"

#include "plugins/ecmascript/es2panda/binder/declaration.h"
#include "plugins/ecmascript/es2panda/checker/ETSchecker.h"
#include "plugins/ecmascript/es2panda/checker/ets/conversion.h"
#include "plugins/ecmascript/es2panda/checker/types/typeFlag.h"
#include "plugins/ecmascript/es2panda/checker/types/typeRelation.h"
#include "plugins/ecmascript/es2panda/ir/base/scriptFunction.h"
#include "plugins/ecmascript/es2panda/ir/expressions/identifier.h"

namespace panda::es2panda::checker {

void ETSObjectType::Iterate(const PropertyTraverser &cb) const
{
    for (const auto *prop : GetAllProperties()) {
        cb(prop);
    }

    if (super_type_ != nullptr) {
        super_type_->Iterate(cb);
    }

    for (const auto *interface : interfaces_) {
        interface->Iterate(cb);
    }
}

binder::LocalVariable *ETSObjectType::GetProperty(const util::StringView &name, PropertySearchFlags flags) const
{
    binder::LocalVariable *res {};
    if ((flags & PropertySearchFlags::SEARCH_INSTANCE_FIELD) != 0) {
        res = GetOwnProperty<PropertyType::INSTANCE_FIELD>(name);
    }

    if (res == nullptr && ((flags & PropertySearchFlags::SEARCH_STATIC_FIELD) != 0)) {
        res = GetOwnProperty<PropertyType::STATIC_FIELD>(name);
    }

    if (res == nullptr && ((flags & PropertySearchFlags::SEARCH_INSTANCE_DECL) != 0)) {
        res = GetOwnProperty<PropertyType::INSTANCE_DECL>(name);
    }

    if (res == nullptr && ((flags & PropertySearchFlags::SEARCH_STATIC_DECL) != 0)) {
        res = GetOwnProperty<PropertyType::STATIC_DECL>(name);
    }

    if (res == nullptr && (flags & PropertySearchFlags::SEARCH_METHOD) != 0) {
        if ((flags & PropertySearchFlags::DISALLOW_SYNTHETIC_METHOD_CREATION) != 0) {
            if ((flags & PropertySearchFlags::SEARCH_INSTANCE_METHOD) != 0) {
                res = GetOwnProperty<PropertyType::INSTANCE_METHOD>(name);
            }

            if (res == nullptr && ((flags & PropertySearchFlags::SEARCH_STATIC_METHOD) != 0)) {
                res = GetOwnProperty<PropertyType::STATIC_METHOD>(name);
            }
        } else {
            res = CreateSyntheticVarFromEverySignature(name, flags);
        }
    }

    if ((flags & (PropertySearchFlags::SEARCH_IN_INTERFACES | PropertySearchFlags::SEARCH_IN_BASE)) == 0) {
        return res;
    }

    if (res != nullptr) {
        return res;
    }

    if ((flags & PropertySearchFlags::SEARCH_IN_INTERFACES) != 0) {
        for (auto *interface : interfaces_) {
            res = interface->GetProperty(name, flags);

            if (res != nullptr) {
                return res;
            }
        }
    }

    if (super_type_ != nullptr && ((flags & PropertySearchFlags::SEARCH_IN_BASE) != 0)) {
        res = super_type_->GetProperty(name, flags);
    }

    return res;
}

binder::LocalVariable *ETSObjectType::CreateSyntheticVarFromEverySignature(const util::StringView &name,
                                                                           PropertySearchFlags flags) const
{
    binder::LocalVariable *res =
        allocator_->New<binder::LocalVariable>(binder::VariableFlags::SYNTHETIC | binder::VariableFlags::METHOD);
    ETSFunctionType *func_type = allocator_->New<ETSFunctionType>(name, allocator_);
    func_type->AddTypeFlag(TypeFlag::SYNTHETIC);

    binder::LocalVariable *functional_interface = CollectSignaturesForSyntheticType(func_type, name, flags);
    if (functional_interface != nullptr) {
        return functional_interface;
    }

    if (func_type->CallSignatures().empty()) {
        return nullptr;
    }

    res->SetTsType(func_type);
    func_type->SetVariable(res);
    return res;
}

binder::LocalVariable *ETSObjectType::CollectSignaturesForSyntheticType(ETSFunctionType *func_type,
                                                                        const util::StringView &name,
                                                                        PropertySearchFlags flags) const
{
    // During function reference resolution, if the found properties type is not a function type, then it is a
    // functional interface, because no other property can be found in the methods of the class. We have to
    // return the found property, because we doesn't need to create a synthetic variable for functional
    // interfaces due to the fact, that by nature they behave as fields, and can't have overloads, and they are
    // subjected to hiding
    if ((flags & PropertySearchFlags::SEARCH_STATIC_METHOD) != 0) {
        auto *found = GetOwnProperty<PropertyType::STATIC_METHOD>(name);
        if (found != nullptr) {
            if (found->HasFlag(binder::VariableFlags::METHOD_REFERENCE)) {
                // Functional interface found
                return found;
            }

            ASSERT(found->TsType()->IsETSFunctionType());
            for (auto *it : found->TsType()->AsETSFunctionType()->CallSignatures()) {
                if (((flags & PropertySearchFlags::IGNORE_ABSTRACT) != 0) &&
                    it->HasSignatureFlag(SignatureFlags::ABSTRACT)) {
                    continue;
                }

                func_type->AddCallSignature(it);
            }
        }
    }

    if ((flags & PropertySearchFlags::SEARCH_INSTANCE_METHOD) != 0) {
        auto *found = GetOwnProperty<PropertyType::INSTANCE_METHOD>(name);
        if (found != nullptr) {
            if (found->HasFlag(binder::VariableFlags::METHOD_REFERENCE)) {
                // Functional interface found
                return found;
            }

            ASSERT(found->TsType()->IsETSFunctionType());
            for (auto *it : found->TsType()->AsETSFunctionType()->CallSignatures()) {
                if (((flags & PropertySearchFlags::IGNORE_ABSTRACT) != 0) &&
                    it->HasSignatureFlag(SignatureFlags::ABSTRACT)) {
                    continue;
                }

                func_type->AddCallSignature(it);
            }
        }
    }

    if (super_type_ != nullptr && ((flags & PropertySearchFlags::SEARCH_IN_BASE) != 0)) {
        return super_type_->CollectSignaturesForSyntheticType(func_type, name, flags);
    }

    return nullptr;
}

std::vector<binder::LocalVariable *> ETSObjectType::GetAllProperties() const
{
    std::vector<binder::LocalVariable *> all_properties;
    for (const auto &[_, prop] : InstanceFields()) {
        (void)_;
        all_properties.push_back(prop);
    }

    for (const auto &[_, prop] : StaticFields()) {
        (void)_;
        all_properties.push_back(prop);
    }

    for (const auto &[_, prop] : InstanceMethods()) {
        (void)_;
        all_properties.push_back(prop);
    }

    for (const auto &[_, prop] : StaticMethods()) {
        (void)_;
        all_properties.push_back(prop);
    }

    for (const auto &[_, prop] : InstanceDecls()) {
        (void)_;
        all_properties.push_back(prop);
    }

    for (const auto &[_, prop] : StaticDecls()) {
        (void)_;
        all_properties.push_back(prop);
    }

    return all_properties;
}

std::vector<binder::LocalVariable *> ETSObjectType::Methods() const
{
    std::vector<binder::LocalVariable *> methods;
    for (const auto &[_, prop] : InstanceMethods()) {
        (void)_;
        methods.push_back(prop);
    }

    for (const auto &[_, prop] : StaticMethods()) {
        (void)_;
        methods.push_back(prop);
    }

    return methods;
}

std::vector<binder::LocalVariable *> ETSObjectType::Fields() const
{
    std::vector<binder::LocalVariable *> fields;
    for (const auto &[_, prop] : InstanceFields()) {
        (void)_;
        fields.push_back(prop);
    }

    for (const auto &[_, prop] : StaticFields()) {
        (void)_;
        fields.push_back(prop);
    }

    return fields;
}

std::vector<const binder::LocalVariable *> ETSObjectType::ForeignProperties() const
{
    std::vector<const binder::LocalVariable *> foreign_props;
    std::unordered_set<util::StringView> own_props;
    own_props.reserve(properties_.size());

    for (const auto *prop : GetAllProperties()) {
        own_props.insert(prop->Name());
    }

    auto all_props = CollectAllProperties();
    for (const auto &[name, var] : all_props) {
        if (own_props.find(name) == own_props.end()) {
            foreign_props.push_back(var);
        }
    }

    return foreign_props;
}

std::unordered_map<util::StringView, const binder::LocalVariable *> ETSObjectType::CollectAllProperties() const
{
    std::unordered_map<util::StringView, const binder::LocalVariable *> prop_map;
    prop_map.reserve(properties_.size());
    Iterate([&prop_map](const binder::LocalVariable *var) { prop_map.insert({var->Name(), var}); });

    return prop_map;
}

void ETSObjectType::ToString(std::stringstream &ss) const
{
    ss << name_;
}

void ETSObjectType::Identical(TypeRelation *relation, Type *other)
{
    other = relation->GetChecker()->AsETSChecker()->GetReferredTypeFromETSTypeReference(other);

    if (!other->IsETSObjectType() || !CheckIdenticalFlags(other->AsETSObjectType()->ObjectFlags())) {
        return;
    }

    if (relation->IgnoreTypeParameters() || (this == other)) {
        relation->Result(true);
        return;
    }

    auto const other_type_arguments = other->AsETSObjectType()->TypeArguments();

    if (HasTypeFlag(TypeFlag::GENERIC) || HasTypeFlag(TypeFlag::NULLABLE)) {
        if (relation->GetChecker()->AsETSChecker()->GetOriginalBaseType(this)->Variable() !=
            relation->GetChecker()->AsETSChecker()->GetOriginalBaseType(other)->Variable()) {
            return;
        }
        if (!HasTypeFlag(TypeFlag::GENERIC)) {
            relation->Result(true);
            return;
        }
        if (type_arguments_.empty() != other_type_arguments.empty()) {
            return;
        }
        ASSERT(type_arguments_.size() == other_type_arguments.size());
        for (size_t idx = 0; idx < type_arguments_.size(); idx++) {
            if (!(type_arguments_[idx]->IsWildcardType() || other_type_arguments[idx]->IsWildcardType())) {
                const auto get_original_base_type_or_type = [&relation](Type *const original_type) {
                    auto *const base_type = relation->GetChecker()->AsETSChecker()->GetOriginalBaseType(original_type);
                    return base_type == nullptr ? original_type : base_type;
                };

                auto *const type_arg_type = relation->GetChecker()->AsETSChecker()->GetReferredTypeFromETSTypeReference(
                    get_original_base_type_or_type(type_arguments_[idx]));
                auto *const other_type_arg_type =
                    relation->GetChecker()->AsETSChecker()->GetReferredTypeFromETSTypeReference(
                        get_original_base_type_or_type(other_type_arguments[idx]));

                type_arg_type->Identical(relation, other_type_arg_type);

                if (!relation->IsTrue()) {
                    return;
                }
            }
        }
    } else {
        if (relation->GetChecker()->AsETSChecker()->GetOriginalBaseType(this)->Variable() !=
            relation->GetChecker()->AsETSChecker()->GetOriginalBaseType(other)->Variable()) {
            return;
        }
        if (HasObjectFlag(ETSObjectFlags::FUNCTIONAL)) {
            auto get_invoke_signature = [](const ETSObjectType *type) {
                auto const prop_invoke =
                    type->GetProperty(util::StringView("invoke"), PropertySearchFlags::SEARCH_INSTANCE_METHOD);
                ASSERT(prop_invoke != nullptr);
                return prop_invoke->TsType()->AsETSFunctionType()->CallSignatures()[0];
            };

            auto *const this_invoke_signature = get_invoke_signature(this);
            auto *const other_invoke_signature = get_invoke_signature(other->AsETSObjectType());

            relation->IsIdenticalTo(this_invoke_signature, other_invoke_signature);
            return;
        }
    }

    relation->Result(true);
}

bool ETSObjectType::CheckIdenticalFlags(ETSObjectFlags target) const
{
    auto cleaned_target_flags = static_cast<ETSObjectFlags>(target & (~ETSObjectFlags::COMPLETELY_RESOLVED));
    cleaned_target_flags &= ~ETSObjectFlags::INCOMPLETE_INSTANTIATION;
    cleaned_target_flags &= ~ETSObjectFlags::CHECKED_COMPATIBLE_ABSTRACTS;
    auto cleaned_self_flags = static_cast<ETSObjectFlags>(ObjectFlags() & (~ETSObjectFlags::COMPLETELY_RESOLVED));
    cleaned_self_flags &= ~ETSObjectFlags::INCOMPLETE_INSTANTIATION;
    cleaned_self_flags &= ~ETSObjectFlags::CHECKED_COMPATIBLE_ABSTRACTS;
    return cleaned_self_flags == cleaned_target_flags;
}

bool ETSObjectType::AssignmentSource([[maybe_unused]] TypeRelation *relation, Type *target)
{
    if (HasObjectFlag(ETSObjectFlags::ENUM)) {
        relation->GetChecker()->ThrowTypeError("Can not assignable to Enum object.", this->GetDeclNode()->Start());
    }

    return target->IsETSNullType();
}

void ETSObjectType::AssignmentTarget(TypeRelation *relation, Type *source)
{
    source = relation->GetChecker()->AsETSChecker()->GetReferredTypeFromETSTypeReference(source);

    if (source->IsETSNullType()) {
        relation->Result(true);
        return;
    }

    if (HasObjectFlag(ETSObjectFlags::FUNCTIONAL)) {
        auto found = properties_[static_cast<size_t>(PropertyType::INSTANCE_METHOD)].find("invoke");
        ASSERT(found != properties_[static_cast<size_t>(PropertyType::INSTANCE_METHOD)].end());
        relation->IsAssignableTo(source, found->second->TsType());
        return;
    }

    IsSubtype(relation, source);
}

void ETSObjectType::Cast(TypeRelation *const relation, Type *const target)
{
    conversion::Identity(relation, this, target);
    if (relation->IsTrue()) {
        return;
    }

    if (this->HasObjectFlag(ETSObjectFlags::NULL_TYPE)) {
        if (target->HasTypeFlag(TypeFlag::ETS_OBJECT)) {
            relation->GetNode()->SetTsType(target);
            relation->Result(true);
            return;
        }

        conversion::Forbidden(relation);
        return;
    }

    if (this->HasObjectFlag(ETSObjectFlags::BUILTIN_BYTE)) {
        if (target->HasTypeFlag(TypeFlag::BYTE)) {
            conversion::Unboxing(relation, this);
            return;
        }

        if (target->HasTypeFlag(TypeFlag::SHORT | TypeFlag::INT | TypeFlag::LONG | TypeFlag::FLOAT |
                                TypeFlag::DOUBLE)) {
            conversion::UnboxingWideningPrimitive(relation, this, target);
            return;
        }

        if (target->HasTypeFlag(TypeFlag::ETS_OBJECT)) {
            conversion::WideningReference(relation, this, target->AsETSObjectType());
            return;
        }

        conversion::Forbidden(relation);
        return;
    }

    if (this->HasObjectFlag(ETSObjectFlags::BUILTIN_SHORT)) {
        if (target->HasTypeFlag(TypeFlag::SHORT)) {
            conversion::Unboxing(relation, this);
            return;
        }

        if (target->HasTypeFlag(TypeFlag::INT | TypeFlag::LONG | TypeFlag::FLOAT | TypeFlag::DOUBLE)) {
            conversion::UnboxingWideningPrimitive(relation, this, target);
            return;
        }

        if (target->HasTypeFlag(TypeFlag::ETS_OBJECT)) {
            conversion::WideningReference(relation, this, target->AsETSObjectType());
            return;
        }

        conversion::Forbidden(relation);
        return;
    }

    if (this->HasObjectFlag(ETSObjectFlags::BUILTIN_CHAR)) {
        if (target->HasTypeFlag(TypeFlag::CHAR)) {
            conversion::Unboxing(relation, this);
            return;
        }

        if (target->HasTypeFlag(TypeFlag::INT | TypeFlag::LONG | TypeFlag::FLOAT | TypeFlag::DOUBLE)) {
            conversion::UnboxingWideningPrimitive(relation, this, target);
            return;
        }

        if (target->HasTypeFlag(TypeFlag::ETS_OBJECT)) {
            conversion::WideningReference(relation, this, target->AsETSObjectType());
            return;
        }

        conversion::Forbidden(relation);
        return;
    }

    if (this->HasObjectFlag(ETSObjectFlags::BUILTIN_INT)) {
        if (target->HasTypeFlag(TypeFlag::INT)) {
            conversion::Unboxing(relation, this);
            return;
        }

        if (target->HasTypeFlag(TypeFlag::LONG | TypeFlag::FLOAT | TypeFlag::DOUBLE)) {
            conversion::UnboxingWideningPrimitive(relation, this, target);
            return;
        }

        if (target->HasTypeFlag(TypeFlag::ETS_OBJECT)) {
            conversion::WideningReference(relation, this, target->AsETSObjectType());
            return;
        }

        conversion::Forbidden(relation);
        return;
    }

    if (this->HasObjectFlag(ETSObjectFlags::BUILTIN_LONG)) {
        if (target->HasTypeFlag(TypeFlag::LONG)) {
            conversion::Unboxing(relation, this);
            return;
        }

        if (target->HasTypeFlag(TypeFlag::FLOAT | TypeFlag::DOUBLE)) {
            conversion::UnboxingWideningPrimitive(relation, this, target);
            return;
        }

        if (target->HasTypeFlag(TypeFlag::ETS_OBJECT)) {
            conversion::WideningReference(relation, this, target->AsETSObjectType());
            return;
        }

        conversion::Forbidden(relation);
        return;
    }

    if (this->HasObjectFlag(ETSObjectFlags::BUILTIN_FLOAT)) {
        if (target->HasTypeFlag(TypeFlag::FLOAT)) {
            conversion::Unboxing(relation, this);
            return;
        }

        if (target->HasTypeFlag(TypeFlag::DOUBLE)) {
            conversion::UnboxingWideningPrimitive(relation, this, target);
            return;
        }

        if (target->HasTypeFlag(TypeFlag::ETS_OBJECT)) {
            conversion::WideningReference(relation, this, target->AsETSObjectType());
            return;
        }

        conversion::Forbidden(relation);
        return;
    }

    if (this->HasObjectFlag(ETSObjectFlags::BUILTIN_DOUBLE)) {
        if (target->HasTypeFlag(TypeFlag::DOUBLE)) {
            conversion::Unboxing(relation, this);
            return;
        }

        if (target->HasTypeFlag(TypeFlag::ETS_OBJECT)) {
            conversion::WideningReference(relation, this, target->AsETSObjectType());
            return;
        }

        conversion::Forbidden(relation);
        return;
    }

    if (this->HasObjectFlag(ETSObjectFlags::BUILTIN_BOOLEAN)) {
        if (target->HasTypeFlag(TypeFlag::ETS_BOOLEAN)) {
            conversion::Unboxing(relation, this);
            return;
        }

        if (target->HasTypeFlag(TypeFlag::ETS_OBJECT)) {
            conversion::WideningReference(relation, this, target->AsETSObjectType());
            return;
        }

        conversion::Forbidden(relation);
        return;
    }

    if (target->HasTypeFlag(TypeFlag::BYTE | TypeFlag::SHORT | TypeFlag::CHAR | TypeFlag::INT | TypeFlag::LONG |
                            TypeFlag::FLOAT | TypeFlag::DOUBLE | TypeFlag::ETS_BOOLEAN)) {
        conversion::NarrowingReferenceUnboxing(relation, this, target);
        return;
    }

    if (target->HasTypeFlag(TypeFlag::ETS_ARRAY)) {
        conversion::NarrowingReference(relation, this, target->AsETSArrayType());
        return;
    }

    if (target->HasTypeFlag(TypeFlag::ETS_OBJECT)) {
        conversion::WideningReference(relation, this, target->AsETSObjectType());
        if (relation->IsTrue()) {
            return;
        }

        conversion::NarrowingReference(relation, this, target->AsETSObjectType());
        if (relation->IsTrue()) {
            return;
        }
    }

    conversion::Forbidden(relation);
}

void ETSObjectType::IsSubtype(TypeRelation *relation, Type *source)
{
    // TODO(mmartin): need check for interface types

    auto *const ets_checker = relation->GetChecker()->AsETSChecker();
    // 3.8.3 Subtyping among Array Types
    if (this == ets_checker->GlobalETSObjectType() && source->IsETSArrayType()) {
        relation->Result(true);
        return;
    }

    if (!source->IsETSObjectType() ||
        !source->AsETSObjectType()->HasObjectFlag(ETSObjectFlags::CLASS | ETSObjectFlags::INTERFACE)) {
        return;
    }

    ETSObjectType *const source_obj = source->AsETSObjectType();

    if (!(source->HasTypeFlag(TypeFlag::GENERIC) || HasTypeFlag(TypeFlag::GENERIC))) {
        if (const Type *const sup = source_obj->AsSuper(relation->GetChecker(), variable_); sup == nullptr) {
            return;
        }

        relation->Result(true);
        return;
    }

    auto *const source_base = ets_checker->GetOriginalBaseType(source);
    auto *const source_base_obj = source_base != nullptr ? source_base : source_obj;
    auto *const base_var = GetBaseType() == nullptr ? variable_ : GetBaseType()->variable_;

    if (const Type *const sup = source_base_obj->AsSuper(relation->GetChecker(), base_var); sup == nullptr) {
        return;
    }

    if (!(source->HasTypeFlag(TypeFlag::GENERIC) && HasTypeFlag(TypeFlag::GENERIC))) {
        relation->Result(true);
        return;
    }

    if (relation->IgnoreTypeParameters()) {
        relation->Result(true);
        return;
    }

    if (type_arguments_.size() != source_obj->TypeArguments().size()) {
        return;
    }

    if (!type_arguments_.empty()) {
        ASSERT(type_arguments_.size() == source_obj->TypeArguments().size());
        for (size_t idx = 0; idx < type_arguments_.size(); idx++) {
            ets_checker->GetReferredTypeFromETSTypeReference(type_arguments_[idx])
                ->Identical(relation,
                            ets_checker->GetReferredTypeFromETSTypeReference(source_obj->TypeArguments()[idx]));
            if (!relation->IsTrue()) {
                return;
            }
        }
    }

    relation->Result(true);
}

Type *ETSObjectType::AsSuper(Checker *checker, binder::Variable *source_var)
{
    if (source_var == nullptr) {
        return nullptr;
    }

    if (variable_ == source_var) {
        return this;
    }

    if (HasObjectFlag(ETSObjectFlags::INTERFACE)) {
        Type *res = nullptr;
        for (auto *const it : checker->AsETSChecker()->GetInterfaces(this)) {
            res = it->AsSuper(checker, source_var);
            if (res != nullptr) {
                return res;
            }
        }
        return checker->GetGlobalTypesHolder()->GlobalETSObjectType()->AsSuper(checker, source_var);
    }

    Type *const super_type = checker->AsETSChecker()->GetSuperType(this);

    if (super_type == nullptr) {
        return nullptr;
    }

    if (!super_type->IsETSObjectType()) {
        return nullptr;
    }

    if (ETSObjectType *const super_obj = super_type->AsETSObjectType();
        super_obj->HasObjectFlag(ETSObjectFlags::CLASS)) {
        Type *const res = super_obj->AsSuper(checker, source_var);
        if (res != nullptr) {
            return res;
        }
    }

    if (source_var->TsType()->IsETSObjectType() &&
        source_var->TsType()->AsETSObjectType()->HasObjectFlag(ETSObjectFlags::INTERFACE)) {
        for (auto *const it : checker->AsETSChecker()->GetInterfaces(this)) {
            Type *const res = it->AsSuper(checker, source_var);
            if (res != nullptr) {
                return res;
            }
        }
    }

    return nullptr;
}

binder::LocalVariable *ETSObjectType::CopyProperty(binder::LocalVariable *prop, ArenaAllocator *allocator,
                                                   TypeRelation *relation, GlobalTypesHolder *global_types)
{
    auto *const copied_prop = prop->Copy(allocator, prop->Declaration());
    auto *const copied_prop_type = ETSChecker::TryToInstantiate(
        relation->GetChecker()->AsETSChecker()->GetTypeOfVariable(prop), allocator, relation, global_types);
    copied_prop_type->SetVariable(copied_prop);
    copied_prop->SetTsType(copied_prop_type);
    return copied_prop;
}

Type *ETSObjectType::Instantiate(ArenaAllocator *const allocator, TypeRelation *const relation,
                                 GlobalTypesHolder *const global_types)
{
    auto *const checker = relation->GetChecker()->AsETSChecker();

    if ((!relation->TypeInstantiationPossible(checker->GetOriginalBaseType(this))) || IsETSNullType()) {
        return this;
    }
    relation->IncreaseTypeRecursionCount(checker->GetOriginalBaseType(this));

    auto *const copied_type = checker->CreateNewETSObjectType(name_, decl_node_, flags_);
    copied_type->type_flags_ = type_flags_;
    copied_type->RemoveObjectFlag(ETSObjectFlags::CHECKED_COMPATIBLE_ABSTRACTS |
                                  ETSObjectFlags::INCOMPLETE_INSTANTIATION);

    copied_type->SetVariable(variable_);
    copied_type->SetSuperType(super_type_);

    for (auto *const it : interfaces_) {
        copied_type->AddInterface(it);
    }

    for (auto const &[_, prop] : InstanceFields()) {
        (void)_;
        auto *const copied_prop = CopyProperty(prop, allocator, relation, global_types);
        copied_type->AddProperty<PropertyType::INSTANCE_FIELD>(copied_prop);
    }

    for (auto const &[_, prop] : StaticFields()) {
        (void)_;
        auto *const copied_prop = CopyProperty(prop, allocator, relation, global_types);
        copied_type->AddProperty<PropertyType::STATIC_FIELD>(copied_prop);
    }

    for (auto const &[_, prop] : InstanceMethods()) {
        (void)_;
        auto *const copied_prop = CopyProperty(prop, allocator, relation, global_types);
        copied_type->AddProperty<PropertyType::INSTANCE_METHOD>(copied_prop);
    }

    for (auto const &[_, prop] : StaticMethods()) {
        (void)_;
        auto *const copied_prop = CopyProperty(prop, allocator, relation, global_types);
        copied_type->AddProperty<PropertyType::STATIC_METHOD>(copied_prop);
    }

    for (auto const &[_, prop] : InstanceDecls()) {
        (void)_;
        auto *const copied_prop = CopyProperty(prop, allocator, relation, global_types);
        copied_type->AddProperty<PropertyType::INSTANCE_DECL>(copied_prop);
    }

    for (auto const &[_, prop] : StaticDecls()) {
        (void)_;
        auto *const copied_prop = CopyProperty(prop, allocator, relation, global_types);
        copied_type->AddProperty<PropertyType::STATIC_DECL>(copied_prop);
    }

    for (auto *const it : construct_signatures_) {
        copied_type->AddConstructSignature(it->Copy(allocator, relation, global_types));
    }

    for (auto *const type_argument : TypeArguments()) {
        copied_type->TypeArguments().emplace_back(type_argument->Instantiate(allocator, relation, global_types));
    }

    relation->DecreaseTypeRecursionCount(checker->GetOriginalBaseType(this));
    copied_type->SetBaseType(base_type_ == nullptr ? this : base_type_);

    return copied_type;
}
}  // namespace panda::es2panda::checker
