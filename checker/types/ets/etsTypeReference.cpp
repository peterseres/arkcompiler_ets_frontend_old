/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "etsTypeReference.h"
#include "plugins/ecmascript/es2panda/checker/types/typeRelation.h"
#include "plugins/ecmascript/es2panda/checker/ETSchecker.h"

namespace panda::es2panda::checker {

util::StringView ETSTypeReference::ReferencedName() const
{
    return var_ref_->Name();
}

void ETSTypeReference::ToString(std::stringstream &ss) const
{
    ss << ReferencedName();
}

void ETSTypeReference::ToAssemblerType(std::stringstream &ss) const
{
    ASSERT(*assembler_ref_);
    (*assembler_ref_)->ToAssemblerTypeWithRank(ss);
}

void ETSTypeReference::ToDebugInfoType(std::stringstream &ss) const
{
    ToAssemblerType(ss);
}

void ETSTypeReference::Identical(TypeRelation *relation, Type *other)
{
    if (!other->IsETSTypeReference()) {
        return;
    }

    if (this == other) {
        relation->Result(true);
        return;
    }

    if (HasRefType()) {
        (*ref_)->Identical(relation, other->AsETSTypeReference()->Ref());
        return;
    }

    relation->Result(var_ref_ == other->AsETSTypeReference()->VarRef());
}

void ETSTypeReference::AssignmentTarget(TypeRelation *relation, Type *source)
{
    if (!HasRefType()) {
        (*ref_) = relation->GetChecker()->AsETSChecker()->GlobalETSObjectType();
    }

    relation->IsAssignableTo(source, *ref_);
}

bool ETSTypeReference::AssignmentSource([[maybe_unused]] TypeRelation *relation, [[maybe_unused]] Type *target)
{
    if (!HasRefType()) {
        return false;
    }

    if (!relation->IsAssignableTo((*ref_), target)) {
        return false;
    }

    relation->Result(true);
    return true;
}

Type *ETSTypeReference::Instantiate([[maybe_unused]] ArenaAllocator *allocator, TypeRelation *relation,
                                    [[maybe_unused]] GlobalTypesHolder *global_types)
{
    if (!relation->TypeInstantiationPossible(this)) {
        return this;
    }
    relation->IncreaseTypeRecursionCount(this);

    Type *return_type = relation->GetChecker()->AsETSChecker()->CreateTypeReference(ref_, assembler_ref_, var_ref_);

    if (HasRefType()) {
        return_type = ETSChecker::TryToInstantiate(*ref_, allocator, relation, global_types);
    }

    relation->DecreaseTypeRecursionCount(this);

    return return_type;
}
}  // namespace panda::es2panda::checker
