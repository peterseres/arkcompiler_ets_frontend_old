{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "x",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 16,
                  "column": 5
                },
                "end": {
                  "line": 16,
                  "column": 6
                }
              }
            },
            "value": {
              "type": "ObjectExpression",
              "properties": [],
              "loc": {
                "start": {
                  "line": 16,
                  "column": 9
                },
                "end": {
                  "line": 16,
                  "column": 11
                }
              }
            },
            "accessibility": "public",
            "static": true,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 17,
      "column": 1
    }
  }
}
TypeError: Cannot infer type for x because class composite needs an explicit target type [objectLiteralNoContextType.ets:16:5]
