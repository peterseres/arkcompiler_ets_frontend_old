{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "B",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 7
            },
            "end": {
              "line": 1,
              "column": 8
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 2,
                  "column": 3
                },
                "end": {
                  "line": 2,
                  "column": 6
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 3
                    },
                    "end": {
                      "line": 2,
                      "column": 6
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 10
                    },
                    "end": {
                      "line": 2,
                      "column": 14
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 15
                    },
                    "end": {
                      "line": 2,
                      "column": 17
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 6
                  },
                  "end": {
                    "line": 2,
                    "column": 17
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 2,
                  "column": 6
                },
                "end": {
                  "line": 2,
                  "column": 17
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 3
              },
              "end": {
                "line": 2,
                "column": 17
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 3,
                "column": 2
              },
              "end": {
                "line": 3,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 9
          },
          "end": {
            "line": 3,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 3,
          "column": 2
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "A",
          "decorators": [],
          "loc": {
            "start": {
              "line": 5,
              "column": 7
            },
            "end": {
              "line": 5,
              "column": 8
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "b",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 6,
                  "column": 3
                },
                "end": {
                  "line": 6,
                  "column": 4
                }
              }
            },
            "value": {
              "type": "ETSNewClassInstanceExpression",
              "typeReference": {
                "type": "ETSTypeReference",
                "part": {
                  "type": "ETSTypeReferencePart",
                  "name": {
                    "type": "Identifier",
                    "name": "B",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 6,
                        "column": 14
                      },
                      "end": {
                        "line": 6,
                        "column": 15
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 6,
                      "column": 14
                    },
                    "end": {
                      "line": 6,
                      "column": 16
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 6,
                    "column": 14
                  },
                  "end": {
                    "line": 6,
                    "column": 16
                  }
                }
              },
              "arguments": [],
              "loc": {
                "start": {
                  "line": 6,
                  "column": 10
                },
                "end": {
                  "line": 6,
                  "column": 18
                }
              }
            },
            "accessibility": "public",
            "static": false,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSTypeReference",
              "part": {
                "type": "ETSTypeReferencePart",
                "name": {
                  "type": "Identifier",
                  "name": "B",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 6,
                      "column": 6
                    },
                    "end": {
                      "line": 6,
                      "column": 7
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 6,
                    "column": 6
                  },
                  "end": {
                    "line": 6,
                    "column": 9
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 6,
                  "column": 6
                },
                "end": {
                  "line": 6,
                  "column": 9
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "bar",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 8,
                  "column": 3
                },
                "end": {
                  "line": 8,
                  "column": 6
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "bar",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 8,
                      "column": 3
                    },
                    "end": {
                      "line": 8,
                      "column": 6
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 8,
                      "column": 10
                    },
                    "end": {
                      "line": 8,
                      "column": 14
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "VariableDeclaration",
                      "declarations": [
                        {
                          "type": "VariableDeclarator",
                          "id": {
                            "type": "Identifier",
                            "name": "a",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 9,
                                "column": 9
                              },
                              "end": {
                                "line": 9,
                                "column": 10
                              }
                            }
                          },
                          "init": {
                            "type": "BinaryExpression",
                            "operator": "+",
                            "left": {
                              "type": "MemberExpression",
                              "object": {
                                "type": "MemberExpression",
                                "object": {
                                  "type": "ThisExpression",
                                  "loc": {
                                    "start": {
                                      "line": 9,
                                      "column": 13
                                    },
                                    "end": {
                                      "line": 9,
                                      "column": 17
                                    }
                                  }
                                },
                                "property": {
                                  "type": "Identifier",
                                  "name": "b",
                                  "decorators": [],
                                  "loc": {
                                    "start": {
                                      "line": 9,
                                      "column": 18
                                    },
                                    "end": {
                                      "line": 9,
                                      "column": 19
                                    }
                                  }
                                },
                                "computed": false,
                                "optional": false,
                                "loc": {
                                  "start": {
                                    "line": 9,
                                    "column": 13
                                  },
                                  "end": {
                                    "line": 9,
                                    "column": 19
                                  }
                                }
                              },
                              "property": {
                                "type": "Identifier",
                                "name": "foo",
                                "decorators": [],
                                "loc": {
                                  "start": {
                                    "line": 9,
                                    "column": 20
                                  },
                                  "end": {
                                    "line": 9,
                                    "column": 23
                                  }
                                }
                              },
                              "computed": false,
                              "optional": false,
                              "loc": {
                                "start": {
                                  "line": 9,
                                  "column": 13
                                },
                                "end": {
                                  "line": 9,
                                  "column": 23
                                }
                              }
                            },
                            "right": {
                              "type": "NumberLiteral",
                              "value": 1,
                              "loc": {
                                "start": {
                                  "line": 9,
                                  "column": 26
                                },
                                "end": {
                                  "line": 9,
                                  "column": 27
                                }
                              }
                            },
                            "loc": {
                              "start": {
                                "line": 9,
                                "column": 13
                              },
                              "end": {
                                "line": 9,
                                "column": 27
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 9,
                              "column": 9
                            },
                            "end": {
                              "line": 9,
                              "column": 27
                            }
                          }
                        }
                      ],
                      "kind": "let",
                      "loc": {
                        "start": {
                          "line": 9,
                          "column": 5
                        },
                        "end": {
                          "line": 9,
                          "column": 28
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 8,
                      "column": 15
                    },
                    "end": {
                      "line": 10,
                      "column": 4
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 8,
                    "column": 6
                  },
                  "end": {
                    "line": 10,
                    "column": 4
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 8,
                  "column": 6
                },
                "end": {
                  "line": 10,
                  "column": 4
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 8,
                "column": 3
              },
              "end": {
                "line": 10,
                "column": 4
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 11,
                "column": 2
              },
              "end": {
                "line": 11,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 5,
            "column": 9
          },
          "end": {
            "line": 11,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 5,
          "column": 1
        },
        "end": {
          "line": 11,
          "column": 2
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 12,
      "column": 1
    }
  }
}
TypeError: Property 'foo' does not exist on type 'B' [identifierReference13.ets:9:20]
