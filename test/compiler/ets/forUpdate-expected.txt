{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 2,
                  "column": 11
                },
                "end": {
                  "line": 2,
                  "column": 14
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 11
                    },
                    "end": {
                      "line": 2,
                      "column": 14
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 18
                    },
                    "end": {
                      "line": 2,
                      "column": 22
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "VariableDeclaration",
                      "declarations": [
                        {
                          "type": "VariableDeclarator",
                          "id": {
                            "type": "Identifier",
                            "name": "length",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 3,
                                "column": 7
                              },
                              "end": {
                                "line": 3,
                                "column": 13
                              }
                            }
                          },
                          "init": {
                            "type": "NumberLiteral",
                            "value": 10,
                            "loc": {
                              "start": {
                                "line": 3,
                                "column": 16
                              },
                              "end": {
                                "line": 3,
                                "column": 18
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 3,
                              "column": 7
                            },
                            "end": {
                              "line": 3,
                              "column": 18
                            }
                          }
                        }
                      ],
                      "kind": "let",
                      "loc": {
                        "start": {
                          "line": 3,
                          "column": 3
                        },
                        "end": {
                          "line": 3,
                          "column": 19
                        }
                      }
                    },
                    {
                      "type": "ForUpdateStatement",
                      "init": {
                        "type": "VariableDeclaration",
                        "declarations": [
                          {
                            "type": "VariableDeclarator",
                            "id": {
                              "type": "Identifier",
                              "name": "i",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 4,
                                  "column": 12
                                },
                                "end": {
                                  "line": 4,
                                  "column": 13
                                }
                              }
                            },
                            "init": {
                              "type": "NumberLiteral",
                              "value": 0,
                              "loc": {
                                "start": {
                                  "line": 4,
                                  "column": 16
                                },
                                "end": {
                                  "line": 4,
                                  "column": 17
                                }
                              }
                            },
                            "loc": {
                              "start": {
                                "line": 4,
                                "column": 12
                              },
                              "end": {
                                "line": 4,
                                "column": 17
                              }
                            }
                          }
                        ],
                        "kind": "let",
                        "loc": {
                          "start": {
                            "line": 4,
                            "column": 8
                          },
                          "end": {
                            "line": 4,
                            "column": 17
                          }
                        }
                      },
                      "test": {
                        "type": "BinaryExpression",
                        "operator": "<",
                        "left": {
                          "type": "Identifier",
                          "name": "i",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 4,
                              "column": 19
                            },
                            "end": {
                              "line": 4,
                              "column": 20
                            }
                          }
                        },
                        "right": {
                          "type": "BinaryExpression",
                          "operator": "-",
                          "left": {
                            "type": "Identifier",
                            "name": "length",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 4,
                                "column": 24
                              },
                              "end": {
                                "line": 4,
                                "column": 30
                              }
                            }
                          },
                          "right": {
                            "type": "NumberLiteral",
                            "value": 2,
                            "loc": {
                              "start": {
                                "line": 4,
                                "column": 33
                              },
                              "end": {
                                "line": 4,
                                "column": 34
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 4,
                              "column": 23
                            },
                            "end": {
                              "line": 4,
                              "column": 35
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 4,
                            "column": 19
                          },
                          "end": {
                            "line": 4,
                            "column": 35
                          }
                        }
                      },
                      "update": {
                        "type": "AssignmentExpression",
                        "operator": "+=",
                        "left": {
                          "type": "Identifier",
                          "name": "i",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 4,
                              "column": 37
                            },
                            "end": {
                              "line": 4,
                              "column": 38
                            }
                          }
                        },
                        "right": {
                          "type": "NumberLiteral",
                          "value": 3,
                          "loc": {
                            "start": {
                              "line": 4,
                              "column": 42
                            },
                            "end": {
                              "line": 4,
                              "column": 43
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 4,
                            "column": 37
                          },
                          "end": {
                            "line": 4,
                            "column": 43
                          }
                        }
                      },
                      "body": {
                        "type": "BlockStatement",
                        "statements": [],
                        "loc": {
                          "start": {
                            "line": 4,
                            "column": 45
                          },
                          "end": {
                            "line": 5,
                            "column": 4
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 4,
                          "column": 3
                        },
                        "end": {
                          "line": 5,
                          "column": 4
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 23
                    },
                    "end": {
                      "line": 6,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 14
                  },
                  "end": {
                    "line": 6,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 2,
                  "column": 14
                },
                "end": {
                  "line": 6,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 2
              },
              "end": {
                "line": 6,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 7,
      "column": 1
    }
  }
}
