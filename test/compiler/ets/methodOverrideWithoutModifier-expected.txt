{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "A",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 7
            },
            "end": {
              "line": 1,
              "column": 8
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 2,
                  "column": 12
                },
                "end": {
                  "line": 2,
                  "column": 15
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 12
                    },
                    "end": {
                      "line": 2,
                      "column": 15
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 19
                    },
                    "end": {
                      "line": 2,
                      "column": 23
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 24
                    },
                    "end": {
                      "line": 4,
                      "column": 6
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 15
                  },
                  "end": {
                    "line": 4,
                    "column": 6
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 2,
                  "column": 15
                },
                "end": {
                  "line": 4,
                  "column": 6
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 4,
                "column": 6
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 5,
                "column": 2
              },
              "end": {
                "line": 5,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 9
          },
          "end": {
            "line": 5,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 5,
          "column": 2
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "B",
          "decorators": [],
          "loc": {
            "start": {
              "line": 7,
              "column": 7
            },
            "end": {
              "line": 7,
              "column": 8
            }
          }
        },
        "superClass": {
          "type": "ETSTypeReference",
          "part": {
            "type": "ETSTypeReferencePart",
            "name": {
              "type": "Identifier",
              "name": "A",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 7,
                  "column": 17
                },
                "end": {
                  "line": 7,
                  "column": 18
                }
              }
            },
            "loc": {
              "start": {
                "line": 7,
                "column": 17
              },
              "end": {
                "line": 7,
                "column": 20
              }
            }
          },
          "loc": {
            "start": {
              "line": 7,
              "column": 17
            },
            "end": {
              "line": 7,
              "column": 20
            }
          }
        },
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 8,
                  "column": 12
                },
                "end": {
                  "line": 8,
                  "column": 15
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 8,
                      "column": 12
                    },
                    "end": {
                      "line": 8,
                      "column": 15
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 8,
                      "column": 19
                    },
                    "end": {
                      "line": 8,
                      "column": 23
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 8,
                      "column": 24
                    },
                    "end": {
                      "line": 10,
                      "column": 6
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 8,
                    "column": 15
                  },
                  "end": {
                    "line": 10,
                    "column": 6
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 8,
                  "column": 15
                },
                "end": {
                  "line": 10,
                  "column": 6
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 8,
                "column": 5
              },
              "end": {
                "line": 10,
                "column": 6
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 11,
                "column": 2
              },
              "end": {
                "line": 11,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 7,
            "column": 19
          },
          "end": {
            "line": 11,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 7,
          "column": 1
        },
        "end": {
          "line": 11,
          "column": 2
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 12,
      "column": 1
    }
  }
}
TypeError: Method overriding requires 'override' modifier [methodOverrideWithoutModifier.ets:8:15]
