{
  "type": "Program",
  "statements": [
    {
      "type": "TSTypeAliasDeclaration",
      "id": {
        "type": "Identifier",
        "name": "x",
        "decorators": [],
        "loc": {
          "start": {
            "line": 16,
            "column": 6
          },
          "end": {
            "line": 16,
            "column": 7
          }
        }
      },
      "typeAnnotation": {
        "type": "ETSPrimitiveType",
        "loc": {
          "start": {
            "line": 16,
            "column": 10
          },
          "end": {
            "line": 16,
            "column": 13
          }
        }
      },
      "loc": {
        "start": {
          "line": 16,
          "column": 1
        },
        "end": {
          "line": 16,
          "column": 14
        }
      }
    },
    {
      "type": "TSTypeAliasDeclaration",
      "id": {
        "type": "Identifier",
        "name": "Matrix",
        "decorators": [],
        "loc": {
          "start": {
            "line": 17,
            "column": 6
          },
          "end": {
            "line": 17,
            "column": 12
          }
        }
      },
      "typeAnnotation": {
        "type": "TSArrayType",
        "elementType": {
          "type": "TSArrayType",
          "elementType": {
            "type": "ETSPrimitiveType",
            "loc": {
              "start": {
                "line": 17,
                "column": 15
              },
              "end": {
                "line": 17,
                "column": 21
              }
            }
          },
          "loc": {
            "start": {
              "line": 17,
              "column": 23
            },
            "end": {
              "line": 17,
              "column": 24
            }
          }
        },
        "loc": {
          "start": {
            "line": 17,
            "column": 25
          },
          "end": {
            "line": 17,
            "column": 26
          }
        }
      },
      "loc": {
        "start": {
          "line": 17,
          "column": 1
        },
        "end": {
          "line": 17,
          "column": 26
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "num",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 19,
                  "column": 5
                },
                "end": {
                  "line": 19,
                  "column": 8
                }
              }
            },
            "value": {
              "type": "NumberLiteral",
              "value": 32,
              "loc": {
                "start": {
                  "line": 19,
                  "column": 14
                },
                "end": {
                  "line": 19,
                  "column": 16
                }
              }
            },
            "accessibility": "public",
            "static": true,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSTypeReference",
              "part": {
                "type": "ETSTypeReferencePart",
                "name": {
                  "type": "Identifier",
                  "name": "x",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 19,
                      "column": 10
                    },
                    "end": {
                      "line": 19,
                      "column": 11
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 19,
                    "column": 10
                  },
                  "end": {
                    "line": 19,
                    "column": 13
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 19,
                  "column": 10
                },
                "end": {
                  "line": 19,
                  "column": 13
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "m",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 20,
                  "column": 5
                },
                "end": {
                  "line": 20,
                  "column": 6
                }
              }
            },
            "value": {
              "type": "ArrayExpression",
              "elements": [
                {
                  "type": "ArrayExpression",
                  "elements": [
                    {
                      "type": "NumberLiteral",
                      "value": 1.1,
                      "loc": {
                        "start": {
                          "line": 20,
                          "column": 19
                        },
                        "end": {
                          "line": 20,
                          "column": 22
                        }
                      }
                    },
                    {
                      "type": "NumberLiteral",
                      "value": 1.1,
                      "loc": {
                        "start": {
                          "line": 20,
                          "column": 24
                        },
                        "end": {
                          "line": 20,
                          "column": 27
                        }
                      }
                    },
                    {
                      "type": "NumberLiteral",
                      "value": 1.1,
                      "loc": {
                        "start": {
                          "line": 20,
                          "column": 29
                        },
                        "end": {
                          "line": 20,
                          "column": 32
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 20,
                      "column": 18
                    },
                    "end": {
                      "line": 20,
                      "column": 33
                    }
                  }
                },
                {
                  "type": "ArrayExpression",
                  "elements": [
                    {
                      "type": "NumberLiteral",
                      "value": 2.1,
                      "loc": {
                        "start": {
                          "line": 20,
                          "column": 34
                        },
                        "end": {
                          "line": 20,
                          "column": 37
                        }
                      }
                    },
                    {
                      "type": "NumberLiteral",
                      "value": 2.1,
                      "loc": {
                        "start": {
                          "line": 20,
                          "column": 39
                        },
                        "end": {
                          "line": 20,
                          "column": 42
                        }
                      }
                    },
                    {
                      "type": "NumberLiteral",
                      "value": 2.1,
                      "loc": {
                        "start": {
                          "line": 20,
                          "column": 44
                        },
                        "end": {
                          "line": 20,
                          "column": 47
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 20,
                      "column": 33
                    },
                    "end": {
                      "line": 20,
                      "column": 48
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 20,
                  "column": 17
                },
                "end": {
                  "line": 20,
                  "column": 49
                }
              }
            },
            "accessibility": "public",
            "static": true,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSTypeReference",
              "part": {
                "type": "ETSTypeReferencePart",
                "name": {
                  "type": "Identifier",
                  "name": "Matrix",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 20,
                      "column": 8
                    },
                    "end": {
                      "line": 20,
                      "column": 14
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 20,
                    "column": 8
                  },
                  "end": {
                    "line": 20,
                    "column": 16
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 20,
                  "column": 8
                },
                "end": {
                  "line": 20,
                  "column": 16
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 21,
      "column": 1
    }
  }
}
