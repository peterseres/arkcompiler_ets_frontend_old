{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "lambda",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 16,
                  "column": 5
                },
                "end": {
                  "line": 16,
                  "column": 11
                }
              }
            },
            "value": {
              "type": "ArrowFunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": null,
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 16,
                      "column": 37
                    },
                    "end": {
                      "line": 16,
                      "column": 41
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 16,
                      "column": 52
                    },
                    "end": {
                      "line": 16,
                      "column": 54
                    }
                  }
                }
                "throwMarker": "throws",
                "loc": {
                  "start": {
                    "line": 16,
                    "column": 33
                  },
                  "end": {
                    "line": 16,
                    "column": 54
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 16,
                  "column": 33
                },
                "end": {
                  "line": 16,
                  "column": 54
                }
              }
            },
            "accessibility": "public",
            "static": true,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSFunctionType",
              "params": [],
              "returnType": {
                "type": "ETSPrimitiveType",
                "loc": {
                  "start": {
                    "line": 16,
                    "column": 19
                  },
                  "end": {
                    "line": 16,
                    "column": 23
                  }
                }
              }
              "throwMarker": "throws",
              "loc": {
                "start": {
                  "line": 16,
                  "column": 13
                },
                "end": {
                  "line": 16,
                  "column": 23
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "lambda2",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 18,
                  "column": 5
                },
                "end": {
                  "line": 18,
                  "column": 12
                }
              }
            },
            "value": {
              "type": "ArrowFunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": null,
                "generator": false,
                "async": false,
                "expression": false,
                "params": [
                  {
                    "type": "Identifier",
                    "name": "param",
                    "typeAnnotation": {
                      "type": "ETSFunctionType",
                      "params": [
                        {
                          "type": "Identifier",
                          "name": "c",
                          "typeAnnotation": {
                            "type": "ETSPrimitiveType",
                            "loc": {
                              "start": {
                                "line": 18,
                                "column": 84
                              },
                              "end": {
                                "line": 18,
                                "column": 87
                              }
                            }
                          },
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 18,
                              "column": 81
                            },
                            "end": {
                              "line": 18,
                              "column": 87
                            }
                          }
                        },
                        {
                          "type": "Identifier",
                          "name": "b",
                          "typeAnnotation": {
                            "type": "ETSPrimitiveType",
                            "loc": {
                              "start": {
                                "line": 18,
                                "column": 92
                              },
                              "end": {
                                "line": 18,
                                "column": 95
                              }
                            }
                          },
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 18,
                              "column": 89
                            },
                            "end": {
                              "line": 18,
                              "column": 95
                            }
                          }
                        }
                      ],
                      "returnType": {
                        "type": "ETSPrimitiveType",
                        "loc": {
                          "start": {
                            "line": 18,
                            "column": 100
                          },
                          "end": {
                            "line": 18,
                            "column": 104
                          }
                        }
                      }
                      "throwMarker": "throws",
                      "loc": {
                        "start": {
                          "line": 18,
                          "column": 80
                        },
                        "end": {
                          "line": 18,
                          "column": 104
                        }
                      }
                    },
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 18,
                        "column": 73
                      },
                      "end": {
                        "line": 18,
                        "column": 104
                      }
                    }
                  }
                ],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 18,
                      "column": 114
                    },
                    "end": {
                      "line": 18,
                      "column": 118
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 18,
                      "column": 131
                    },
                    "end": {
                      "line": 18,
                      "column": 133
                    }
                  }
                }
                "throwMarker": "rethrows",
                "loc": {
                  "start": {
                    "line": 18,
                    "column": 72
                  },
                  "end": {
                    "line": 18,
                    "column": 133
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 18,
                  "column": 72
                },
                "end": {
                  "line": 18,
                  "column": 133
                }
              }
            },
            "accessibility": "public",
            "static": true,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSFunctionType",
              "params": [
                {
                  "type": "Identifier",
                  "name": "param",
                  "typeAnnotation": {
                    "type": "ETSFunctionType",
                    "params": [
                      {
                        "type": "Identifier",
                        "name": "c",
                        "typeAnnotation": {
                          "type": "ETSPrimitiveType",
                          "loc": {
                            "start": {
                              "line": 18,
                              "column": 26
                            },
                            "end": {
                              "line": 18,
                              "column": 29
                            }
                          }
                        },
                        "decorators": [],
                        "loc": {
                          "start": {
                            "line": 18,
                            "column": 23
                          },
                          "end": {
                            "line": 18,
                            "column": 29
                          }
                        }
                      },
                      {
                        "type": "Identifier",
                        "name": "b",
                        "typeAnnotation": {
                          "type": "ETSPrimitiveType",
                          "loc": {
                            "start": {
                              "line": 18,
                              "column": 34
                            },
                            "end": {
                              "line": 18,
                              "column": 37
                            }
                          }
                        },
                        "decorators": [],
                        "loc": {
                          "start": {
                            "line": 18,
                            "column": 31
                          },
                          "end": {
                            "line": 18,
                            "column": 37
                          }
                        }
                      }
                    ],
                    "returnType": {
                      "type": "ETSPrimitiveType",
                      "loc": {
                        "start": {
                          "line": 18,
                          "column": 42
                        },
                        "end": {
                          "line": 18,
                          "column": 46
                        }
                      }
                    }
                    "throwMarker": "throws",
                    "loc": {
                      "start": {
                        "line": 18,
                        "column": 22
                      },
                      "end": {
                        "line": 18,
                        "column": 46
                      }
                    }
                  },
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 18,
                      "column": 15
                    },
                    "end": {
                      "line": 18,
                      "column": 46
                    }
                  }
                }
              ],
              "returnType": {
                "type": "ETSPrimitiveType",
                "loc": {
                  "start": {
                    "line": 18,
                    "column": 58
                  },
                  "end": {
                    "line": 18,
                    "column": 62
                  }
                }
              }
              "throwMarker": "throws",
              "loc": {
                "start": {
                  "line": 18,
                  "column": 14
                },
                "end": {
                  "line": 18,
                  "column": 62
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 19,
      "column": 1
    }
  }
}
