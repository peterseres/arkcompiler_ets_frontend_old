{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "Class",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 7
            },
            "end": {
              "line": 1,
              "column": 12
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "bar",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 2,
                  "column": 18
                },
                "end": {
                  "line": 2,
                  "column": 21
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "bar",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 18
                    },
                    "end": {
                      "line": 2,
                      "column": 21
                    }
                  }
                },
                "generator": false,
                "async": true,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSTypeReference",
                  "part": {
                    "type": "ETSTypeReferencePart",
                    "name": {
                      "type": "Identifier",
                      "name": "Promise",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 2,
                          "column": 25
                        },
                        "end": {
                          "line": 2,
                          "column": 32
                        }
                      }
                    },
                    "typeParams": {
                      "type": "TSTypeParameterInstantiation",
                      "params": [
                        {
                          "type": "ETSTypeReference",
                          "part": {
                            "type": "ETSTypeReferencePart",
                            "name": {
                              "type": "Identifier",
                              "name": "Object",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 2,
                                  "column": 33
                                },
                                "end": {
                                  "line": 2,
                                  "column": 39
                                }
                              }
                            },
                            "loc": {
                              "start": {
                                "line": 2,
                                "column": 33
                              },
                              "end": {
                                "line": 2,
                                "column": 40
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 2,
                              "column": 33
                            },
                            "end": {
                              "line": 2,
                              "column": 40
                            }
                          }
                        }
                      ],
                      "loc": {
                        "start": {
                          "line": 2,
                          "column": 32
                        },
                        "end": {
                          "line": 2,
                          "column": 40
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 2,
                        "column": 25
                      },
                      "end": {
                        "line": 2,
                        "column": 42
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 25
                    },
                    "end": {
                      "line": 2,
                      "column": 42
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "ReturnStatement",
                      "argument": {
                        "type": "NullLiteral",
                        "value": null,
                        "loc": {
                          "start": {
                            "line": 2,
                            "column": 50
                          },
                          "end": {
                            "line": 2,
                            "column": 54
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 2,
                          "column": 43
                        },
                        "end": {
                          "line": 2,
                          "column": 55
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 41
                    },
                    "end": {
                      "line": 2,
                      "column": 57
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 21
                  },
                  "end": {
                    "line": 2,
                    "column": 57
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 2,
                  "column": 21
                },
                "end": {
                  "line": 2,
                  "column": 57
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 2,
                "column": 57
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 3,
                "column": 2
              },
              "end": {
                "line": 3,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 13
          },
          "end": {
            "line": 3,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 3,
          "column": 2
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 5,
                  "column": 16
                },
                "end": {
                  "line": 5,
                  "column": 19
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 5,
                      "column": 16
                    },
                    "end": {
                      "line": 5,
                      "column": 19
                    }
                  }
                },
                "generator": false,
                "async": true,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSTypeReference",
                  "part": {
                    "type": "ETSTypeReferencePart",
                    "name": {
                      "type": "Identifier",
                      "name": "Promise",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 5,
                          "column": 23
                        },
                        "end": {
                          "line": 5,
                          "column": 30
                        }
                      }
                    },
                    "typeParams": {
                      "type": "TSTypeParameterInstantiation",
                      "params": [
                        {
                          "type": "ETSTypeReference",
                          "part": {
                            "type": "ETSTypeReferencePart",
                            "name": {
                              "type": "Identifier",
                              "name": "Object",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 5,
                                  "column": 31
                                },
                                "end": {
                                  "line": 5,
                                  "column": 37
                                }
                              }
                            },
                            "loc": {
                              "start": {
                                "line": 5,
                                "column": 31
                              },
                              "end": {
                                "line": 5,
                                "column": 38
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 5,
                              "column": 31
                            },
                            "end": {
                              "line": 5,
                              "column": 38
                            }
                          }
                        }
                      ],
                      "loc": {
                        "start": {
                          "line": 5,
                          "column": 30
                        },
                        "end": {
                          "line": 5,
                          "column": 38
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 5,
                        "column": 23
                      },
                      "end": {
                        "line": 5,
                        "column": 40
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 5,
                      "column": 23
                    },
                    "end": {
                      "line": 5,
                      "column": 40
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "ReturnStatement",
                      "argument": {
                        "type": "NullLiteral",
                        "value": null,
                        "loc": {
                          "start": {
                            "line": 5,
                            "column": 48
                          },
                          "end": {
                            "line": 5,
                            "column": 52
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 5,
                          "column": 41
                        },
                        "end": {
                          "line": 5,
                          "column": 53
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 5,
                      "column": 39
                    },
                    "end": {
                      "line": 5,
                      "column": 55
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 5,
                    "column": 19
                  },
                  "end": {
                    "line": 5,
                    "column": 55
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 5,
                  "column": 19
                },
                "end": {
                  "line": 5,
                  "column": 55
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 5,
                "column": 1
              },
              "end": {
                "line": 5,
                "column": 55
              }
            }
          },
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "lambda",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 7,
                  "column": 5
                },
                "end": {
                  "line": 7,
                  "column": 11
                }
              }
            },
            "value": {
              "type": "ArrowFunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": null,
                "generator": false,
                "async": true,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSTypeReference",
                  "part": {
                    "type": "ETSTypeReferencePart",
                    "name": {
                      "type": "Identifier",
                      "name": "Promise",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 7,
                          "column": 47
                        },
                        "end": {
                          "line": 7,
                          "column": 54
                        }
                      }
                    },
                    "typeParams": {
                      "type": "TSTypeParameterInstantiation",
                      "params": [
                        {
                          "type": "ETSTypeReference",
                          "part": {
                            "type": "ETSTypeReferencePart",
                            "name": {
                              "type": "Identifier",
                              "name": "Object",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 7,
                                  "column": 55
                                },
                                "end": {
                                  "line": 7,
                                  "column": 61
                                }
                              }
                            },
                            "loc": {
                              "start": {
                                "line": 7,
                                "column": 55
                              },
                              "end": {
                                "line": 7,
                                "column": 62
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 7,
                              "column": 55
                            },
                            "end": {
                              "line": 7,
                              "column": 62
                            }
                          }
                        }
                      ],
                      "loc": {
                        "start": {
                          "line": 7,
                          "column": 54
                        },
                        "end": {
                          "line": 7,
                          "column": 62
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 7,
                        "column": 47
                      },
                      "end": {
                        "line": 7,
                        "column": 65
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 7,
                      "column": 47
                    },
                    "end": {
                      "line": 7,
                      "column": 65
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "ReturnStatement",
                      "argument": {
                        "type": "NullLiteral",
                        "value": null,
                        "loc": {
                          "start": {
                            "line": 7,
                            "column": 75
                          },
                          "end": {
                            "line": 7,
                            "column": 79
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 7,
                          "column": 68
                        },
                        "end": {
                          "line": 7,
                          "column": 80
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 7,
                      "column": 66
                    },
                    "end": {
                      "line": 7,
                      "column": 82
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 7,
                    "column": 43
                  },
                  "end": {
                    "line": 7,
                    "column": 82
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 7,
                  "column": 43
                },
                "end": {
                  "line": 7,
                  "column": 82
                }
              }
            },
            "accessibility": "public",
            "static": true,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSFunctionType",
              "params": [],
              "returnType": {
                "type": "ETSTypeReference",
                "part": {
                  "type": "ETSTypeReferencePart",
                  "name": {
                    "type": "Identifier",
                    "name": "Promise",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 7,
                        "column": 19
                      },
                      "end": {
                        "line": 7,
                        "column": 26
                      }
                    }
                  },
                  "typeParams": {
                    "type": "TSTypeParameterInstantiation",
                    "params": [
                      {
                        "type": "ETSTypeReference",
                        "part": {
                          "type": "ETSTypeReferencePart",
                          "name": {
                            "type": "Identifier",
                            "name": "Object",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 7,
                                "column": 27
                              },
                              "end": {
                                "line": 7,
                                "column": 33
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 7,
                              "column": 27
                            },
                            "end": {
                              "line": 7,
                              "column": 34
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 7,
                            "column": 27
                          },
                          "end": {
                            "line": 7,
                            "column": 34
                          }
                        }
                      }
                    ],
                    "loc": {
                      "start": {
                        "line": 7,
                        "column": 26
                      },
                      "end": {
                        "line": 7,
                        "column": 34
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 7,
                      "column": 19
                    },
                    "end": {
                      "line": 7,
                      "column": 36
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 7,
                    "column": 19
                  },
                  "end": {
                    "line": 7,
                    "column": 36
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 7,
                  "column": 13
                },
                "end": {
                  "line": 7,
                  "column": 36
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 8,
      "column": 1
    }
  }
}
