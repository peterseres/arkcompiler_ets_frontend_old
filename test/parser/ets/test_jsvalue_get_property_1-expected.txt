{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "v",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 16,
                  "column": 5
                },
                "end": {
                  "line": 16,
                  "column": 6
                }
              }
            },
            "accessibility": "public",
            "static": true,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSTypeReference",
              "part": {
                "type": "ETSTypeReferencePart",
                "name": {
                  "type": "Identifier",
                  "name": "JSValue",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 16,
                      "column": 8
                    },
                    "end": {
                      "line": 16,
                      "column": 15
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 16,
                    "column": 8
                  },
                  "end": {
                    "line": 16,
                    "column": 16
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 16,
                  "column": 8
                },
                "end": {
                  "line": 16,
                  "column": 16
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "prop",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 17,
                  "column": 5
                },
                "end": {
                  "line": 17,
                  "column": 9
                }
              }
            },
            "value": {
              "type": "MemberExpression",
              "object": {
                "type": "Identifier",
                "name": "v",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 17,
                    "column": 21
                  },
                  "end": {
                    "line": 17,
                    "column": 22
                  }
                }
              },
              "property": {
                "type": "Identifier",
                "name": "prop_name",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 17,
                    "column": 23
                  },
                  "end": {
                    "line": 17,
                    "column": 32
                  }
                }
              },
              "computed": false,
              "optional": false,
              "loc": {
                "start": {
                  "line": 17,
                  "column": 21
                },
                "end": {
                  "line": 17,
                  "column": 32
                }
              }
            },
            "accessibility": "public",
            "static": true,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSTypeReference",
              "part": {
                "type": "ETSTypeReferencePart",
                "name": {
                  "type": "Identifier",
                  "name": "JSValue",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 17,
                      "column": 11
                    },
                    "end": {
                      "line": 17,
                      "column": 18
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 17,
                    "column": 11
                  },
                  "end": {
                    "line": 17,
                    "column": 20
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 17,
                  "column": 11
                },
                "end": {
                  "line": 17,
                  "column": 20
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 18,
      "column": 1
    }
  }
}
