{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "a",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 16,
                  "column": 5
                },
                "end": {
                  "line": 16,
                  "column": 6
                }
              }
            },
            "accessibility": "public",
            "static": true,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSFunctionType",
              "params": [
                {
                  "type": "Identifier",
                  "name": "c",
                  "typeAnnotation": {
                    "type": "ETSPrimitiveType",
                    "loc": {
                      "start": {
                        "line": 16,
                        "column": 12
                      },
                      "end": {
                        "line": 16,
                        "column": 15
                      }
                    }
                  },
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 16,
                      "column": 9
                    },
                    "end": {
                      "line": 16,
                      "column": 15
                    }
                  }
                },
                {
                  "type": "Identifier",
                  "name": "b",
                  "typeAnnotation": {
                    "type": "ETSPrimitiveType",
                    "loc": {
                      "start": {
                        "line": 16,
                        "column": 20
                      },
                      "end": {
                        "line": 16,
                        "column": 23
                      }
                    }
                  },
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 16,
                      "column": 17
                    },
                    "end": {
                      "line": 16,
                      "column": 23
                    }
                  }
                }
              ],
              "returnType": {
                "type": "ETSPrimitiveType",
                "loc": {
                  "start": {
                    "line": 16,
                    "column": 28
                  },
                  "end": {
                    "line": 16,
                    "column": 32
                  }
                }
              }
              "throwMarker": "throws",
              "loc": {
                "start": {
                  "line": 16,
                  "column": 8
                },
                "end": {
                  "line": 16,
                  "column": 32
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 17,
      "column": 1
    }
  }
}
