#  Export list declaration is not supported

Rule ``arkts-no-export-list-decl``

**Severity: error**

ArkTS does not support syntax of export list declarations. All exported
entities must be explicitly annotated with the ``export`` keyword.


## TypeScript


```

    export { x }
    export { x } from "mod"
    export { x, y as b, z as c }

```

## ArkTS


```

    let x = 1
    class MyClass {}
    export let y = x, z: number = 2
    export RenamedClass = MyClass

```

## See also

- Recipe 123:  Renaming in export declarations is not supported (``arkts-no-export-renaming``)
- Recipe 125:  Re-exporting is not supported (``arkts-no-reexport``)
- Recipe 126:  ``export = ...`` assignment is not supported (``arkts-no-export-assignment``)


