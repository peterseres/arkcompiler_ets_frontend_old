#  Mapped type expression is not supported

Rule ``arkts-no-mapped-types``

**Severity: error**

ArkTS does not support mapped types. Use other language idioms and regular
classes to achieve the same behaviour.


## TypeScript


```

    type OptionsFlags<Type> = {
        [Property in keyof Type]: boolean
    }

```

## See also

- Recipe 097:  `keyof` operator is not supported (``arkts-no-keyof``)


