#  Use arrow functions instead of function expressions

Rule ``arkts-no-func-expressions``

**Severity: warning**

ArkTS does not support function expressions, use arrow functions instead
to be explicitly specified.


## TypeScript


```

    let f = function (s: string) {
        console.log(s)
    }

```

## ArkTS


```

    let f = (s: string) => {
        console.log(s)
    }

```


