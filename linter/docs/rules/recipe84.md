#  ``with`` statement is not supported

Rule ``arkts-no-with``

**Severity: error**

ArkTS does not support the ``with`` statement. Use other language idioms
(including fully qualified names of functions) to achieve the same behaviour.



