#  Class literals are not supported

Rule ``arkts-no-class-literals``

**Severity: error**

ArkTS does not support class literals. A new named class type must be
introduced explicitly.


## TypeScript


```

    const Rectangle = class {
        constructor(height: number, width: number) {
            this.heigth = height
            this.width = width
        }

        heigth
        width
    }

    const rectangle = new Rectangle(0.0, 0.0)

```

## ArkTS


```

    class Rectangle {
        constructor(height: number, width: number) {
            this.heigth = height
            this.width = width
        }

        heigth: number
        width: number
    }

    const rectangle = new Rectangle(0.0, 0.0)

```


