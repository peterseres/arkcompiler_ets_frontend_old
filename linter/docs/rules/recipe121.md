#  ``require`` is not supported

Rule ``arkts-no-require``

**Severity: error**

ArkTS does not support importing via ``require``. Use ``import`` instead.


## TypeScript


```

    import m = require("mod")

```

## ArkTS


```

    import * as m from "mod"

```


