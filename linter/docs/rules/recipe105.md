#  Property-based runtime type checks are not supported

Rule ``arkts-no-prop-existence-check``

**Severity: error**

ArkTS requires that object layout is determined in compile-time and cannot
be changed at runtime. There for no runtime property-based checks are supported.
If you need to do a type cast, use ``as`` operator and use desired properties
and methods. If some property doesn't exist then an attempt to reference it
will result in a compile-time error.


## TypeScript


```

    class A {
        foo() {}
        bar() {}
    }

    function getSomeObject() {
        return new A()
    }

    let obj: any = getSomeObject()
    if (obj && obj.foo && obj.bar) {
        console.log("Yes")  // prints "Yes" in this example
    } else {
        console.log("No")
    }

```

## ArkTS


```

    class A {
        foo(): void {}
        bar(): void {}
    }

    function getSomeObject(): A {
        return new A()
    }

    function main(): void {
        let tmp: Object = getSomeObject()
        let obj: A = tmp as A
        obj.foo()       // OK
        obj.bar()       // OK
        obj.some_foo()  // Compile-time error: Method some_foo does not exist on this type
    }

```

## See also

- Recipe 001:  Objects with property names that are not identifiers are not supported (``arkts-no-computed-props``)
- Recipe 002:  ``Symbol()`` API is not supported (``arkts-no-symbol``)
- Recipe 052:  Attempt to access an undefined property is a compile-time error (``arkts-no-undefined-prop-access``)
- Recipe 059:  ``delete`` operator is not supported (``arkts-no-delete``)
- Recipe 060:  ``typeof`` is allowed only in expression contexts (``arkts-no-type-query``)
- Recipe 066:  ``in`` operator is not supported (``arkts-no-in``)
- Recipe 109:  Dynamic property declaration is not supported (``arkts-no-dyn-prop-decl``)


