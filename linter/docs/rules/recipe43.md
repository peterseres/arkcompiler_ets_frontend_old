#  Untyped array literals are not supported

Rule ``arkts-no-noninferrable-arr-literals``

**Severity: error**

ArkTS does not support the usage of untyped array literals.  The type of an
array element must be inferred from the context. Use the type ``Object`` to
define mixed types array.


## TypeScript


```

    let x1 = [1, 2]
    let x2 = [1, "aa"]

```

## ArkTS


```

    let x1: Object[] = [new Number(1), new Number(2)]

    // Implicit boxing of numbers to the object type Number
    let x2: Object[] = [1, 2]

    // Implicit boxing of number and string to the object types Number and String
    let x3: Object[] = [1, "aa"]

```

## See also

- Recipe 038:  Object literal must correspond to explicitly declared class or interface (``arkts-no-untyped-obj-literals``)
- Recipe 040:  Object literals cannot be used as type declarations (``arkts-no-obj-literals-as-types``)


