/*
 * Copyright (c) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export function animations() {
  let anyvar: any = undefined;
  let symvar: symbol;
  let unknownType: unknown;
  let undefType: undefined;
  let nullType: null;
  const state = 0;

  const flag = false;

  const status = { name: 'A', idx: 0, handler: 'foo' };

  type Person = [string, number];
  const user: Person = ['John', 32];
  const age = user[1];

  type Point = { x: number; y: number };
  type P = keyof Point;
  type AxeX = Point['x'];
  type P_NULL = P | null;
  type P_UNKNOWN = P | unknown;
  type P_ANY = P | any;
  type P_P = P | Person;
  type P_P_NULL = P | Person | null;

  const typeU = typeof user;

  function isNumber(x: any): x is number {
    return typeof x === 'number';
  }

  const regex = /go*d/;

  throw new TypeError('labuda');
}

const c = 'c';
const d = 10;
type ComputedPropertyT = {
  a: string; // String-like name
  5: string; // Number-like name
  [c]: string; // String-like name
  [d]: string; // Number-like name
};

class LiteralAsPropertyName {
  2: string;
  'Two': number;
}

const litAsPropName: LiteralAsPropertyName = {
  2: 'two',
  'Two': 2,
};

type Dictionary = {
  [key: string]: unknown;
};
let dict: Dictionary;

function bar(key: string, val: any) {
  if (key in dict) {
    dict[key] = val;
  }
}

interface I1 {
  m1(): number;
  m2(): void;
}

interface I2 {
  m2(): string;
  m3(): boolean;
}

type IntersectionT = I1 & I2;

type DescribableFunction = {
  description: string;
  (someArg: number): boolean;
};
function callFunctionObject(fn: DescribableFunction) {
  console.log(fn.description + ' returned ' + fn(5));
}
const funcWithDescr: DescribableFunction = (x: number) => x % 2 == 0;
funcWithDescr.description = 'isEven';
callFunctionObject(funcWithDescr);

class G<T> {
  val: T;
  getVal(): T {
    return this.val;
  }
}
class H extends G<{ x: 2 }> {}
const g: G<{ y: string }> = new G<{ y: 'constant' }>();
function generic<T>(t: T): number {
  return 10;
}
generic<{ z: boolean }>({ z: true });

function typeAssertions(): void {
  const num = <any>1;
  const myCanvas = <HTMLCanvasElement>document.getElementById('main_canvas');
}

function dynamicProperties(): void {
  const x = { a: 5, b: 'text' };
  x['c'] = 100200;
  console.log(x['c']);

  const y: any = { q: 100, w: true };
  y.e = 'dynamic';
  console.log(y.e);
}

function genericArrayType(): void {
  const x: Array<string> = ['1', '2', '3'];
  const y: Array<number> = new Array(1, 2, 3);
  const z: number[] = [1, 2, 3];

  function arrayFunc<T extends object>(array: Array<T>): Array<string> {
    return array.map((x) => x.toString());
  }
}

class C {
  constructor(n: number) {
  }
}
function undefinedValue(n: string): void {
  n = undefined;
  undefinedValue(undefined);
  let c = new C(undefined);
}
