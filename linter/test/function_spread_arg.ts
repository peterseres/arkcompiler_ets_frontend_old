/*
 * Copyright (c) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

function foo(x, y, z) {
  console.log(x, y, z);
}

const args = [0, 'str', false];
foo(...args);

function bar(x, y, ...z) {
  console.log(x, y, z);
}

bar(-1, 0, ...args);

let arr: number[] = [ 1, 2, 3 ];
bar(-1, 0, ...arr);
