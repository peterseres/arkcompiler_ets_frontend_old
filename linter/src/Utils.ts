/*
 * Copyright (c) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import * as ts from 'typescript';
import { ProblemInfo } from './TypeScriptLinter';
import { AutofixInfo } from './Common';

export function logTscDiagnostic(diagnostics: readonly ts.Diagnostic[], log: (message: any, ...args: any[]) => void) {
  diagnostics.forEach((diagnostic) => {
    let message = ts.flattenDiagnosticMessageText(diagnostic.messageText, '\n');

    if (diagnostic.file && diagnostic.start) {
      const { line, character } = ts.getLineAndCharacterOfPosition(diagnostic.file, diagnostic.start);
      message = `${diagnostic.file.fileName} (${line + 1}, ${character + 1}): ${message}`;
    }

    log(message);
  });
}

export function encodeProblemInfo(problem: ProblemInfo): string {
  return `${problem.problem}%${problem.start}%${problem.end}`;
}

export function decodeAutofixInfo(info: string): AutofixInfo {
  let infos = info.split('%');
  return { problemID: infos[0], start: Number.parseInt(infos[1]), end: Number.parseInt(infos[2]) };
}

export class TsUtils {
  static statementKinds = [
    ts.SyntaxKind.Block,ts.SyntaxKind.EmptyStatement,ts.SyntaxKind.VariableStatement,ts.SyntaxKind.ExpressionStatement,
    ts.SyntaxKind.IfStatement,ts.SyntaxKind.DoStatement,ts.SyntaxKind.WhileStatement,ts.SyntaxKind.ForStatement,
    ts.SyntaxKind.ForInStatement,ts.SyntaxKind.ForOfStatement,ts.SyntaxKind.ContinueStatement,
    ts.SyntaxKind.BreakStatement,ts.SyntaxKind.ReturnStatement,ts.SyntaxKind.WithStatement,
    ts.SyntaxKind.SwitchStatement,ts.SyntaxKind.LabeledStatement,ts.SyntaxKind.ThrowStatement,
    ts.SyntaxKind.TryStatement,ts.SyntaxKind.DebuggerStatement,
  ];

  static LIMITED_STD_GLOBAL_FUNC = [
    'eval', 'isFinite', 'isNaN', 'parseFloat', 'parseInt', 'encodeURI', 'encodeURIComponent', 'Encode', 'decodeURI',
    'decodeURIComponent', 'Decode', 'escape', 'unescape', 'ParseHexOctet'
  ];
  static LIMITED_STD_GLOBAL_VAR = ['Infinity', 'NaN'];
  static LIMITED_STD_OBJECT_API = [
    '__proto__', '__defineGetter__', '__defineSetter__', '__lookupGetter__', '__lookupSetter__', 'assign', 'create',
    'defineProperties', 'defineProperty', 'entries', 'freeze', 'fromEntries', 'getOwnPropertyDescriptor', 
    'getOwnPropertyDescriptors', 'getOwnPropertyNames', 'getOwnPropertySymbols', 'getPrototypeOf', 'hasOwn',
    'hasOwnProperty', 'is', 'isExtensible', 'isFrozen', 'isPrototypeOf', 'isSealed', 'keys', 'preventExtensions',
    'propertyIsEnumerable', 'seal', 'setPrototypeOf', 'values'
  ];
  static LIMITED_STD_REFLECT_API = [
    'apply', 'construct', 'defineProperty', 'deleteProperty', 'get', 'getOwnPropertyDescriptor', 'getPrototypeOf',
    'has', 'isExtensible', 'ownKeys', 'preventExtensions', 'set', 'setPrototypeOf'
  ];
  static LIMITED_STD_PROXYHANDLER_API = [
    'apply', 'construct', 'defineProperty', 'deleteProperty', 'get', 'getOwnPropertyDescriptor', 'getPrototypeOf', 
    'has', 'isExtensible', 'ownKeys', 'preventExtensions', 'set', 'setPrototypeOf'
  ];
  static LIMITED_STD_ARRAY_API = ['isArray'];
  static LIMITED_STD_ARRAYBUFFER_API = ['isView'];

  constructor(private tsTypeChecker: ts.TypeChecker) {
  }

  public isStatementKindNode(tsNode: ts.Node): boolean {
    return TsUtils.statementKinds.includes(tsNode.kind);
  }

  public isAssignmentOperator(tsBinOp: ts.BinaryOperatorToken): boolean {
    return tsBinOp.kind >= ts.SyntaxKind.FirstAssignment && tsBinOp.kind <= ts.SyntaxKind.LastAssignment;
  }

  public isArrayNotTupleType(tsType: ts.TypeNode | undefined): boolean {
    if (tsType && tsType.kind && ts.isArrayTypeNode(tsType)) {
      // Check that element type is not a union type to filter out tuple types induced by tuple literals.
      const tsElemType = this.unwrapParenthesizedType(tsType.elementType);
      return !ts.isUnionTypeNode(tsElemType);
    }

    return false;
  }

  public isNumberType(tsType: ts.Type): boolean {
    if (tsType.isUnion()) {
      for (let tsCompType of tsType.types) {
        if ((tsCompType.flags & ts.TypeFlags.NumberLike) === 0) return false;
      }
      return true;
    }
    return (tsType.getFlags() & ts.TypeFlags.NumberLike) !== 0;
  }

  public isBooleanType(tsType: ts.Type): boolean {
    return (tsType.getFlags() & ts.TypeFlags.BooleanLike) !== 0;
  }

  public isStringType(tsType: ts.Type): boolean {
    if (tsType.isUnion()) {
      for (let tsCompType of tsType.types) {
        if ((tsCompType.flags & ts.TypeFlags.StringLike) === 0) return false;
      }
      return true;
    }
    return (tsType.getFlags() & ts.TypeFlags.StringLike) !== 0;
  }

  public unwrapParenthesizedType(tsType: ts.TypeNode): ts.TypeNode {
    while (ts.isParenthesizedTypeNode(tsType)) {
      tsType = tsType.type;
    }
    return tsType;
  }

  public findParentIf(asExpr: ts.AsExpression): ts.IfStatement | null {
    let node = asExpr.parent;
    while (node) {
      if (node.kind === ts.SyntaxKind.IfStatement) 
        return node as ts.IfStatement;

      node = node.parent;
    }

    return null;
  }

  public isDestructuringAssignmentLHS(
    tsExpr: ts.ArrayLiteralExpression | ts.ObjectLiteralExpression
  ): boolean {
    // Check whether given expression is the LHS part of the destructuring
    // assignment (or is a nested element of destructuring pattern).
    let tsParent = tsExpr.parent;
    let tsCurrentExpr: ts.Node = tsExpr;
    while (tsParent) {
      if (
        ts.isBinaryExpression(tsParent) && this.isAssignmentOperator(tsParent.operatorToken) &&
        tsParent.left === tsCurrentExpr
      )
        return true;

      if (
        (ts.isForStatement(tsParent) || ts.isForInStatement(tsParent) || ts.isForOfStatement(tsParent)) &&
        tsParent.initializer && tsParent.initializer === tsCurrentExpr
      )
        return true;

      tsCurrentExpr = tsParent;
      tsParent = tsParent.parent;
    }

    return false;
  }

  public isEnumType(tsType: ts.Type): boolean {
    // Note: For some reason, test (tsType.flags & ts.TypeFlags.Enum) != 0 doesn't work here.
    // Must use SymbolFlags to figure out if this is an enum type.
    return tsType.symbol && (tsType.symbol.flags & ts.SymbolFlags.Enum) !== 0;
  }

  public isObjectLiteralType(tsType: ts.Type): boolean {
    return tsType.symbol && (tsType.symbol.flags & ts.SymbolFlags.ObjectLiteral) !== 0;
  }

  public isNumberLikeType(tsType: ts.Type): boolean {
    return (tsType.getFlags() & ts.TypeFlags.NumberLike) !== 0;
  }

  public hasModifier(tsModifiers: readonly ts.Modifier[] | undefined, tsModifierKind: number): boolean {
    // Sanity check.
    if (!tsModifiers) return false;

    for (const tsModifier of tsModifiers) {
      if (tsModifier.kind === tsModifierKind) return true;
    }

    return false;
  }

  public unwrapParenthesized(tsExpr: ts.Expression): ts.Expression {
    let unwrappedExpr = tsExpr;
    while (ts.isParenthesizedExpression(unwrappedExpr))
      unwrappedExpr = unwrappedExpr.expression;

    return unwrappedExpr;
  }

  public symbolHasDuplicateName(symbol: ts.Symbol, tsDeclKind: ts.SyntaxKind): boolean {
    // Type Checker merges all declarations with the same name in one scope into one symbol. 
    // Thus, check whether the symbol of certain declaration has any declaration with
    // different syntax kind.
    const symbolDecls = symbol?.getDeclarations();
    if (symbolDecls) {
      for (const symDecl of symbolDecls) {
        // Don't count declarations with 'Identifier' syntax kind as those
        // usually depict declaring an object's property through assignment.
        if (symDecl.kind !== ts.SyntaxKind.Identifier && symDecl.kind !== tsDeclKind) return true;
      }
    }

    return false;
  }

  public isReferenceType(tsType: ts.Type): boolean {
    const f = tsType.getFlags();
    return (
      (f & ts.TypeFlags.InstantiableNonPrimitive) != 0 || (f & ts.TypeFlags.Object) != 0 ||
      (f & ts.TypeFlags.Boolean) != 0 || (f & ts.TypeFlags.Enum) != 0 || (f & ts.TypeFlags.NonPrimitive) != 0 ||
      (f & ts.TypeFlags.Number) != 0 || (f & ts.TypeFlags.String) != 0
    );
  }

  public isPrimitiveType(type: ts.Type): boolean {
    const f = type.getFlags();
    return (
      (f & ts.TypeFlags.Boolean) != 0 || (f & ts.TypeFlags.BooleanLiteral) != 0 || 
      (f & ts.TypeFlags.Number) != 0 || (f & ts.TypeFlags.NumberLiteral) != 0 
      // In ArkTS 'string' is not a primitive type. So for the common subset 'string' 
      // should be considered as a reference type. That is why next line is commented out.
      //(f & ts.TypeFlags.String) != 0 || (f & ts.TypeFlags.StringLiteral) != 0
    );
  }

  public isTypeSymbol(symbol: ts.Symbol | undefined): boolean {
    return (
      !!symbol && !!symbol.flags &&
      ((symbol.flags & ts.SymbolFlags.Class) !== 0 || (symbol.flags & ts.SymbolFlags.Interface) !== 0)
    );
  }

  // Check whether type is generic 'Array<T>' type defined in TypeScript standard library.
  public isGenericArrayType(tsType: ts.Type): tsType is ts.TypeReference {
    return (
      this.isTypeReference(tsType) && tsType.typeArguments?.length === 1 && tsType.target.typeParameters?.length === 1 &&
      tsType.getSymbol()?.getName() === 'Array'
    );
  }

  public isTypeReference(tsType: ts.Type): tsType is ts.TypeReference {
    return (
      (tsType.getFlags() & ts.TypeFlags.Object) !== 0 &&
      ((tsType as ts.ObjectType).objectFlags & ts.ObjectFlags.Reference) !== 0
    );
  }

  public isNullType(tsTypeNode: ts.TypeNode): boolean {
    return (ts.isLiteralTypeNode(tsTypeNode) && tsTypeNode.literal.kind === ts.SyntaxKind.NullKeyword);
  }

  public isThisOrSuperExpr(tsExpr: ts.Expression): boolean {
    return (tsExpr.kind == ts.SyntaxKind.ThisKeyword || tsExpr.kind == ts.SyntaxKind.SuperKeyword);
  }

  public isPrototypeSymbol(symbol: ts.Symbol | undefined): boolean {
    return (!!symbol && !!symbol.flags && (symbol.flags & ts.SymbolFlags.Prototype) !== 0);
  }

  public isFunctionSymbol(symbol: ts.Symbol | undefined): boolean {
    return (!!symbol && !!symbol.flags && (symbol.flags & ts.SymbolFlags.Function) !== 0);
  }

  public isInterfaceType(tsType: ts.Type | undefined): boolean {
    return (
      !!tsType && !!tsType.symbol && !!tsType.symbol.flags &&
      (tsType.symbol.flags & ts.SymbolFlags.Interface) !== 0
    );
  }

  public isAnyType(tsType: ts.Type): tsType is ts.TypeReference {
    return (tsType.getFlags() & ts.TypeFlags.Any) !== 0;
  }

  public isUnsupportedType(tsType: ts.Type): boolean {
    return (
      !!tsType.flags && ((tsType.flags & ts.TypeFlags.Any) !== 0 || (tsType.flags & ts.TypeFlags.Unknown) !== 0 ||
      (tsType.flags & ts.TypeFlags.Intersection) !== 0) || this.isUnsupportedUnionType(tsType)
    );
  }

  public isUnsupportedUnionType(tsType: ts.Type): boolean {
    if (tsType.isUnion()) {
      return !this.isNullableUnionType(tsType) && !this.isBooleanUnionType(tsType);
    }
    return false;
  }

  private isNullableUnionType(tsUnionType: ts.UnionType): boolean {
    let tsTypes = tsUnionType.types;
    return (
      tsTypes.length === 2 &&
      ((tsTypes[0].flags & ts.TypeFlags.Null) !== 0 || (tsTypes[1].flags & ts.TypeFlags.Null) !== 0)
    );
  }

  private isBooleanUnionType(tsUnionType: ts.UnionType): boolean {
    // For some reason, 'boolean' type is also represented as as union 
    // of 'true' and 'false' literal types. This form of 'union' type
    // should be considered as supported.
    let tsCompTypes = tsUnionType.types;
    return (
      tsUnionType.flags === (ts.TypeFlags.Boolean | ts.TypeFlags.Union) && tsCompTypes.length === 2 &&
      tsCompTypes[0].flags === ts.TypeFlags.BooleanLiteral && (tsCompTypes[1].flags === ts.TypeFlags.BooleanLiteral)
    );
  }

  public isFunctionOrMethod(tsSymbol: ts.Symbol | undefined): boolean {
    return (
      !!tsSymbol &&
      ((tsSymbol.flags & ts.SymbolFlags.Function) !== 0 || (tsSymbol.flags & ts.SymbolFlags.Method) !== 0)
    );
  }

  public isMethodAssignment(tsSymbol: ts.Symbol | undefined): boolean {
    return (
      !!tsSymbol &&
      ((tsSymbol.flags & ts.SymbolFlags.Method) !== 0 && (tsSymbol.flags & ts.SymbolFlags.Assignment) !== 0)
    );
  }

  private getDeclaration(tsSymbol: ts.Symbol | undefined): ts.Declaration | null {
    if (tsSymbol && tsSymbol.declarations && tsSymbol.declarations.length > 0)
      return tsSymbol.declarations[0];
    return null;
  }

  private isVarDeclaration(tsDecl: ts.Declaration): boolean {
    return ts.isVariableDeclaration(tsDecl) && ts.isVariableDeclarationList(tsDecl.parent);
  }

  public isValidEnumMemberInit(tsExpr: ts.Expression): boolean {
    if (this.isIntegerConstantValue(tsExpr.parent as ts.EnumMember))
      return true;
    if (this.isStringConstantValue(tsExpr.parent as ts.EnumMember))
      return true;
    return this.isCompileTimeExpression(tsExpr);
  }

  public isCompileTimeExpression(tsExpr: ts.Expression): boolean {    
    if (
      ts.isParenthesizedExpression(tsExpr) ||
      (ts.isAsExpression(tsExpr) && tsExpr.type.kind === ts.SyntaxKind.NumberKeyword))
      return this.isCompileTimeExpression(tsExpr.expression);

    switch (tsExpr.kind) {
      case ts.SyntaxKind.PrefixUnaryExpression:
        return this.isPrefixUnaryExprValidEnumMemberInit(tsExpr as ts.PrefixUnaryExpression);
      case ts.SyntaxKind.ParenthesizedExpression:
      case ts.SyntaxKind.BinaryExpression:
        return this.isBinaryExprValidEnumMemberInit(tsExpr as ts.BinaryExpression);  
      case ts.SyntaxKind.ConditionalExpression:
        return this.isConditionalExprValidEnumMemberInit(tsExpr as ts.ConditionalExpression);
      case ts.SyntaxKind.Identifier:
        return this.isIdentifierValidEnumMemberInit(tsExpr as ts.Identifier);
      case ts.SyntaxKind.NumericLiteral:
        return this.isIntegerConstantValue(tsExpr as ts.NumericLiteral);
      case ts.SyntaxKind.PropertyAccessExpression: {
        // if enum member is in current enum declaration try to get value
        // if it comes from another enum consider as constant
        const propertyAccess = tsExpr as ts.PropertyAccessExpression;
        if(this.isIntegerConstantValue(propertyAccess))
          return true;
        const leftHandSymbol = this.tsTypeChecker.getSymbolAtLocation(propertyAccess.expression);
        if( !leftHandSymbol )
          return false;
        const decls = leftHandSymbol.getDeclarations();
        if (!decls || decls.length !== 1) 
          return false;
        return ts.isEnumDeclaration(decls[0]);
      }
      default:
        return false;
    }
  }

  private isPrefixUnaryExprValidEnumMemberInit(tsExpr: ts.PrefixUnaryExpression): boolean {
    return (this.isUnaryOpAllowedForEnumMemberInit(tsExpr.operator) && this.isCompileTimeExpression(tsExpr.operand));
  }

  private isBinaryExprValidEnumMemberInit(tsExpr: ts.BinaryExpression): boolean {
    return (
      this.isBinaryOpAllowedForEnumMemberInit(tsExpr.operatorToken) && this.isCompileTimeExpression(tsExpr.left) &&
      this.isCompileTimeExpression(tsExpr.right)
    );
  }

  private isConditionalExprValidEnumMemberInit(tsExpr: ts.ConditionalExpression): boolean {
    return (this.isCompileTimeExpression(tsExpr.whenTrue) && this.isCompileTimeExpression(tsExpr.whenFalse));
  }

  private isIdentifierValidEnumMemberInit(tsExpr: ts.Identifier): boolean {
    let tsSymbol = this.tsTypeChecker.getSymbolAtLocation(tsExpr);
    let tsDecl = this.getDeclaration(tsSymbol);
    return (!!tsDecl && 
              ( (this.isVarDeclaration(tsDecl) && this.isConst(tsDecl.parent)) ||
                (tsDecl.kind === ts.SyntaxKind.EnumMember)
              )
    );
  }

  private isUnaryOpAllowedForEnumMemberInit(tsPrefixUnaryOp: ts.PrefixUnaryOperator): boolean {
    return (
      tsPrefixUnaryOp === ts.SyntaxKind.PlusToken || tsPrefixUnaryOp === ts.SyntaxKind.MinusToken ||
      tsPrefixUnaryOp === ts.SyntaxKind.TildeToken
    );
  }

  private isBinaryOpAllowedForEnumMemberInit(tsBinaryOp: ts.BinaryOperatorToken): boolean {
    return (
      tsBinaryOp.kind === ts.SyntaxKind.AsteriskToken || tsBinaryOp.kind === ts.SyntaxKind.SlashToken ||
      tsBinaryOp.kind === ts.SyntaxKind.PercentToken || tsBinaryOp.kind === ts.SyntaxKind.MinusToken ||
      tsBinaryOp.kind === ts.SyntaxKind.PlusToken || tsBinaryOp.kind === ts.SyntaxKind.LessThanLessThanToken ||
      tsBinaryOp.kind === ts.SyntaxKind.GreaterThanGreaterThanToken || tsBinaryOp.kind === ts.SyntaxKind.BarBarToken ||
      tsBinaryOp.kind === ts.SyntaxKind.GreaterThanGreaterThanGreaterThanToken ||
      tsBinaryOp.kind === ts.SyntaxKind.AmpersandToken || tsBinaryOp.kind === ts.SyntaxKind.CaretToken ||
      tsBinaryOp.kind === ts.SyntaxKind.BarToken || tsBinaryOp.kind === ts.SyntaxKind.AmpersandAmpersandToken
    );
  }

  public isConst(tsNode: ts.Node): boolean {
    return !!(ts.getCombinedNodeFlags(tsNode) & ts.NodeFlags.Const);
  }

  public isIntegerConstantValue(
    tsExpr: ts.EnumMember | ts.PropertyAccessExpression | ts.ElementAccessExpression | ts.NumericLiteral
  ): boolean {

    const tsConstValue = (tsExpr.kind === ts.SyntaxKind.NumericLiteral) ? 
      Number(tsExpr.getText()) :
      this.tsTypeChecker.getConstantValue(tsExpr);
    return (
      tsConstValue !== undefined && typeof tsConstValue === 'number' &&
      tsConstValue.toFixed(0) === tsConstValue.toString()
    );
  } 

  public isStringConstantValue(
    tsExpr: ts.EnumMember | ts.PropertyAccessExpression | ts.ElementAccessExpression
  ): boolean {
    const tsConstValue = this.tsTypeChecker.getConstantValue(tsExpr);
    return (
      tsConstValue !== undefined && typeof tsConstValue === 'string' 
    );
  }

  // Returns true iff typeA is a subtype of typeB
  public relatedByInheritanceOrIdentical(typeA: ts.Type, typeB: ts.Type): boolean {
    if (this.isTypeReference(typeA) && typeA.target !== typeA) typeA = typeA.target;
    if (this.isTypeReference(typeB) && typeB.target !== typeB) typeB = typeB.target;
    
    if (typeA === typeB || this.isObjectType(typeB)) return true;
    if (!typeA.symbol || !typeA.symbol.declarations) return false;

    for (let typeADecl of typeA.symbol.declarations) {
      if (
        (!ts.isClassDeclaration(typeADecl) && !ts.isInterfaceDeclaration(typeADecl)) || 
        !typeADecl.heritageClauses
      ) continue;
      for (let heritageClause of typeADecl.heritageClauses) {
        if (this.processParentTypes(heritageClause.types, typeB)) return true;
      }
    }

    return false;
  }

  private processParentTypes(parentTypes: ts.NodeArray<ts.Expression>, typeB: ts.Type): boolean {
    for (let baseTypeExpr of parentTypes) {
      let baseType = this.tsTypeChecker.getTypeAtLocation(baseTypeExpr);
      if (baseType && this.relatedByInheritanceOrIdentical(baseType, typeB)) return true;
    }
    return false;
  }

  public isObjectType(tsType: ts.Type): boolean {
    return (
      tsType && tsType.isClassOrInterface() && tsType.symbol && 
      (tsType.symbol.name === 'Object' || tsType.symbol.name === 'object')
    );
  }

  public isCallToFunctionWithOmittedReturnType(tsExpr: ts.Expression): boolean {
    if (ts.isCallExpression(tsExpr)) {
      let tsCallSignature = this.tsTypeChecker.getResolvedSignature(tsExpr);
      if (tsCallSignature) {
        const tsSignDecl = tsCallSignature.getDeclaration();
        // `tsSignDecl` is undefined when `getResolvedSignature` returns `unknownSignature`
        if (!tsSignDecl || !tsSignDecl.type) return true;
      }
    }

    return false;
  }

  private hasReadonlyFields(type: ts.Type): boolean {
    if (type.symbol.members === undefined) return false; // No members -> no readonly fields

    let result: boolean = false;

    type.symbol.members.forEach((value, key) => {
      if (
        value.declarations !== undefined && value.declarations.length > 0 && 
        ts.isPropertyDeclaration(value.declarations[0])
      ) {
        let propmMods = ts.getModifiers(value.declarations[0] as ts.PropertyDeclaration);
        if (this.hasModifier(propmMods, ts.SyntaxKind.ReadonlyKeyword)) {
          result = true;
          return;
        }
      }
    });

    return result;
  }

  private hasDefaultCtor(type: ts.Type): boolean {
    if (type.symbol.members === undefined) return true; // No members -> no explicite constructors -> there is default ctor

    let hasCtor: boolean = false; // has any constructor 
    let hasDefaultCtor: boolean = false; // has default constructor

    type.symbol.members.forEach((value, key) => {
      if ((value.flags & ts.SymbolFlags.Constructor) !== 0) {
        hasCtor = true;

        if (value.declarations !== undefined && value.declarations.length > 0) {
          let declCtor = value.declarations[0] as ts.ConstructorDeclaration;
          if (declCtor.parameters.length === 0) {
            hasDefaultCtor = true;
            return;
          }
        }
      }
    });

    return !hasCtor || hasDefaultCtor; // Has no any explicite constructor -> has implicite default constructor.
  }

  private isAbstractClass(type: ts.Type): boolean {
    if (type.isClass() && type.symbol.declarations && type.symbol.declarations.length > 0) {
      let declClass = type.symbol.declarations[0] as ts.ClassDeclaration;
      let classMods = ts.getModifiers(declClass);
      if (this.hasModifier(classMods, ts.SyntaxKind.AbstractKeyword))
        return true;
    }

    return false;
  }

  public validateObjectLiteralType(type: ts.Type | undefined): boolean {
    return (
      type != undefined && type.isClassOrInterface() && this.hasDefaultCtor(type) && 
      !this.hasReadonlyFields(type) && !this.isAbstractClass(type)
    );
  }

  public hasMemberFunction(objectLiteral: ts.ObjectLiteralExpression): boolean {
    for (let i = 0; i < objectLiteral.properties.length; i++) {
      let prop = objectLiteral.properties[i];
      if (ts.isPropertyAssignment(prop)) { 
        let propAssignment = prop as ts.PropertyAssignment;
        if (ts.isArrowFunction(propAssignment.initializer) || ts.isFunctionExpression(propAssignment.initializer)) {
          return true;
        }
      }
    };

    return false;
  }

  private findDelaration(type: ts.ClassDeclaration | ts.InterfaceDeclaration, name: string)
    : ts.NamedDeclaration | undefined {
    let members: ts.NodeArray<ts.ClassElement> | ts.NodeArray<ts.TypeElement> = type.members;
    let declFound: ts.NamedDeclaration | undefined;

    for(let m of members) {
      if (m.name && m.name.getText() === name) {
        declFound = m;
        break;
      }
    }

    if (declFound || !type.heritageClauses) return declFound;

    // Search in base classes/interfaces
    for (let i1 = 0; i1 < type.heritageClauses.length; i1++) {
      let v1 = type.heritageClauses[i1];
      for (let i2 = 0; i2 < v1.types.length; i2++) {
        let v2 = v1.types[i2];
        let symbol = this.tsTypeChecker.getTypeAtLocation(v2.expression).symbol;
        if (
          (symbol.flags === ts.SymbolFlags.Class || symbol.flags === ts.SymbolFlags.Interface) &&
          symbol.declarations && symbol.declarations.length > 0
        ) {
          declFound = this.findDelaration(symbol.declarations[0] as (ts.ClassDeclaration | ts.InterfaceDeclaration), name);
        }

        if (declFound) break;
      };

      if (declFound) break;
    };

    return declFound;
  }

  private areTypesAssignable(lhsType: ts.Type, rhsExpr: ts.Expression): boolean {
    if (lhsType.isClassOrInterface()) {
      if (!ts.isObjectLiteralExpression(rhsExpr)) return false;

      return this.validateFields(lhsType, rhsExpr);
    }

    let rhsType = this.tsTypeChecker.getTypeAtLocation(rhsExpr);

    if (rhsType.isLiteral() || (rhsType.flags & ts.TypeFlags.BooleanLiteral) !== 0)
      rhsType = this.tsTypeChecker.getBaseTypeOfLiteralType(rhsType);

    return lhsType === rhsType;
  }

  public validateFields(type: ts.Type | undefined, objectLiteral: ts.ObjectLiteralExpression): boolean {
    if (type === undefined || type.symbol.declarations === undefined) return false;

    let declType = type.symbol.declarations[0] as (ts.ClassDeclaration | ts.InterfaceDeclaration);

    for (let i = 0; i < objectLiteral.properties.length; i++) {
      let prop = objectLiteral.properties[i];
      if (ts.isPropertyAssignment(prop)) { 
        let propAssignment = prop as ts.PropertyAssignment;
        let propName = propAssignment.name.getText();
        let decl = this.findDelaration(declType, propName);
        if (!decl) {
          return false;
        }

        if (!this.areTypesAssignable(this.tsTypeChecker.getTypeAtLocation(decl), propAssignment.initializer)) {
          return false;
        }
      }
    };

    return true;
  }

  private isSupportedTypeNodeKind(kind: ts.SyntaxKind): boolean {
    return kind !== ts.SyntaxKind.AnyKeyword && kind !== ts.SyntaxKind.UnknownKeyword &&
      kind !== ts.SyntaxKind.SymbolKeyword && kind !== ts.SyntaxKind.UndefinedKeyword &&
      kind !== ts.SyntaxKind.ConditionalType && kind !== ts.SyntaxKind.MappedType &&
      kind !== ts.SyntaxKind.InferType && kind !== ts.SyntaxKind.IndexedAccessType;
  }

  public isSupportedType(typeNode: ts.TypeNode): boolean {
    if (ts.isParenthesizedTypeNode(typeNode)) return this.isSupportedType(typeNode.type);

    if (ts.isArrayTypeNode(typeNode)) return this.isSupportedType(typeNode.elementType);

    if (ts.isTypeReferenceNode(typeNode) && typeNode.typeArguments) {
      for (const typeArg of typeNode.typeArguments)
        if (!this.isSupportedType(typeArg)) return false;
      return true;
    }

    if (ts.isUnionTypeNode(typeNode)) {
      for (const unionTypeElem of typeNode.types)
        if (!this.isSupportedType(unionTypeElem)) return false;
      return true;
    }

    return !ts.isTypeLiteralNode(typeNode) && !ts.isTypeQueryNode(typeNode) &&
      !ts.isIntersectionTypeNode(typeNode) && !ts.isTupleTypeNode(typeNode) &&
      this.isSupportedTypeNodeKind(typeNode.kind);
  }

  private getParentSymbolName(symbol: ts.Symbol): string | undefined {
    const name = this.tsTypeChecker.getFullyQualifiedName(symbol);
    const dotPosition = name.lastIndexOf('.');
    return (dotPosition === -1) ? undefined : name.substring(0, dotPosition);
  }

  public isGlobalSymbol(symbol: ts.Symbol): boolean {
    let parentName = this.getParentSymbolName(symbol);
    return !parentName || parentName === 'global';
  }
  public isStdObjectAPI(symbol: ts.Symbol): boolean {
    let parentName = this.getParentSymbolName(symbol);
    return !!parentName && (parentName === 'Object' || parentName === 'ObjectConstructor');
  }
  public isStdReflectAPI(symbol: ts.Symbol): boolean {
    let parentName = this.getParentSymbolName(symbol);
    return !!parentName && (parentName === 'Reflect');
  }
  public isStdProxyHandlerAPI(symbol: ts.Symbol): boolean {
    let parentName = this.getParentSymbolName(symbol);
    return !!parentName && (parentName === 'ProxyHandler');
  }
  public isStdArrayAPI(symbol: ts.Symbol): boolean {
    let parentName = this.getParentSymbolName(symbol);
    return !!parentName && (parentName === 'Array');
  }
  public isStdArrayBufferAPI(symbol: ts.Symbol): boolean {
    let parentName = this.getParentSymbolName(symbol);
    return !!parentName && (parentName === 'ArrayBuffer');
  }

  public isDefaultImport(importSpec: ts.ImportSpecifier): boolean {
    return importSpec?.propertyName?.text === 'default';
  }
}
