/*
 * Copyright (c) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import * as ts from 'typescript';
import { TsUtils } from './Utils';
import { FaultID, faultsAttrs } from './Problems';
import { cookBookMsg, cookBookTag } from './CookBookMsg';
import { LinterConfig } from './TypeScriptLinterConfig';
import { Autofix, AutofixInfoSet } from './Autofixer';
import * as Autofixer from './Autofixer';
import Logger from '../utils/logger';

const logger = Logger.getLogger();

export function consoleLog(...args: any[]): void {
  if (TypeScriptLinter.ideMode) return;

  let outLine = '';
  for (let k = 0; k < args.length; k++) {
    outLine += `${args[k]} `;
  }

  logger.info(outLine);
}

enum ProblemSeverity { WARNING = 1, ERROR = 2 }
export interface ProblemInfo {
  line: number;
  column: number;
  endColumn: number;
  start: number;
  end: number;
  type: string;
  severity: number;
  problem: string;
  suggest: string;
  rule: string;
  ruleTag: number;
  autofixable: boolean;
  autofix?: Autofix[];
}

export class TypeScriptLinter {
  totalVisitedNodes: number = 0;
  nodeCounters: number[] = [];
  lineCounters: number[] = [];

  totalErrorLines: number = 0;
  errorLineNumbersString: string = '';
  totalWarningLines: number = 0;
  warningLineNumbersString: string = '';

  problemsInfos: ProblemInfo[] = [];

  tsUtils: TsUtils;

  currentErrorLine: number;
  currentWarningLine: number;
  staticBlocks: Set<string>;

  private sourceFile?: ts.SourceFile;

  static ideMode: boolean = false;

  constructor(
    private tsTypeChecker: ts.TypeChecker,
    private autofixesInfo: AutofixInfoSet,
    public strictMode: boolean,
    public warningsAsErrors: boolean,
  ) {
    this.tsUtils = new TsUtils(this.tsTypeChecker);
    this.currentErrorLine = 0;
    this.currentWarningLine = 0;
    this.staticBlocks = new Set<string>();

    for (let i = 0; i < FaultID.LAST_ID; i++) {
      this.nodeCounters[i] = 0;
      this.lineCounters[i] = 0;
    }
  }

  readonly handlersMap = new Map([
    [ts.SyntaxKind.ObjectLiteralExpression, this.handleObjectLiteralExpression],
    [ts.SyntaxKind.ArrayLiteralExpression, this.handleArrayLiteralExpression],
    [ts.SyntaxKind.Parameter, this.handleParameter], [ts.SyntaxKind.TypeOperator, this.handleTypeOperator],
    [ts.SyntaxKind.EnumDeclaration, this.handleEnumDeclaration],
    [ts.SyntaxKind.InterfaceDeclaration, this.handleInterfaceDeclaration],
    [ts.SyntaxKind.ThrowStatement, this.handleThrowStatement], [ts.SyntaxKind.ImportClause, this.handleImportClause],
    [ts.SyntaxKind.ForStatement, this.handleForStatement],
    [ts.SyntaxKind.ForInStatement, this.handleForInStatement], 
    [ts.SyntaxKind.ForOfStatement, this.handleForOfStatement], 
    [ts.SyntaxKind.ImportDeclaration, this.handleImportDeclaration],
    [ts.SyntaxKind.PropertyAccessExpression, this.handlePropertyAccessExpression],
    [ts.SyntaxKind.PropertyDeclaration, this.handlePropertyAssignmentOrDeclaration],
    [ts.SyntaxKind.PropertyAssignment, this.handlePropertyAssignmentOrDeclaration],
    [ts.SyntaxKind.FunctionExpression, this.handleFunctionExpression],
    [ts.SyntaxKind.ArrowFunction, this.handleArrowFunction],
    [ts.SyntaxKind.ClassExpression, this.handleClassExpression], [ts.SyntaxKind.CatchClause, this.handleCatchClause],
    [ts.SyntaxKind.FunctionDeclaration, this.handleFunctionDeclaration],
    [ts.SyntaxKind.PrefixUnaryExpression, this.handlePrefixUnaryExpression],
    [ts.SyntaxKind.BinaryExpression, this.handleBinaryExpression],
    [ts.SyntaxKind.VariableDeclarationList, this.handleVariableDeclarationList],
    [ts.SyntaxKind.VariableDeclaration, this.handleVariableDeclaration],
    [ts.SyntaxKind.ClassDeclaration, this.handleClassDeclaration],
    [ts.SyntaxKind.ModuleDeclaration, this.handleModuleDeclaration],
    [ts.SyntaxKind.TypeAliasDeclaration, this.handleTypeAliasDeclaration],
    [ts.SyntaxKind.ImportSpecifier, this.handleImportSpecifier],
    [ts.SyntaxKind.NamespaceImport, this.handleNamespaceImport],
    [ts.SyntaxKind.TypeAssertionExpression, this.handleTypeAssertionExpression],
    [ts.SyntaxKind.MethodDeclaration, this.handleMethodDeclaration],
    [ts.SyntaxKind.ClassStaticBlockDeclaration, this.handleClassStaticBlockDeclaration],
    [ts.SyntaxKind.SwitchStatement, this.handleSwitchStatement], [ts.SyntaxKind.Identifier, this.handleIdentifier],
    [ts.SyntaxKind.ElementAccessExpression, this.handleElementAccessExpression],
    [ts.SyntaxKind.EnumMember, this.handleEnumMember], [ts.SyntaxKind.TypeReference, this.handleTypeReference],
    [ts.SyntaxKind.ExportDeclaration, this.handleExportDeclaration],
    [ts.SyntaxKind.ExportAssignment, this.handleExportAssignment],
    [ts.SyntaxKind.CallExpression, this.handleCallExpression], [ts.SyntaxKind.MetaProperty, this.handleMetaProperty],
    [ts.SyntaxKind.NewExpression, this.handleNewExpression], [ts.SyntaxKind.AsExpression, this.handleAsExpression],
    [ts.SyntaxKind.BigIntLiteral, this.handleBigIntLiteral],
    [ts.SyntaxKind.SpreadElement, this.handleSpreadOp], [ts.SyntaxKind.SpreadAssignment, this.handleSpreadOp],
    [ts.SyntaxKind.NonNullExpression, this.handleNonNullExpression],
  ]);

  public incrementCounters(node: ts.Node, faultId: number, autofixable: boolean = false, autofix?: Autofix[],) {
    if (!this.strictMode && faultsAttrs[faultId].migratable) // In relax mode skip migratable
      return;

    this.nodeCounters[faultId]++;
    // TSC counts lines and columns from zero
    let { line, character } = this.sourceFile!.getLineAndCharacterOfPosition(node.getStart());
    ++line;
    ++character;

    let faultDescr = LinterConfig.nodeDesc[faultId];
    let faultType = LinterConfig.tsSyntaxKindNames[node.kind];
    
    if (TypeScriptLinter.ideMode) {
      const cookBookMsgNum = faultsAttrs[faultId] ? Number(faultsAttrs[faultId].cookBookRef) : 0;
      const cookBookTg = cookBookTag[cookBookMsgNum];
      let severity = ProblemSeverity.ERROR;
      if (faultsAttrs[faultId] && faultsAttrs[faultId].warning)
        severity = ProblemSeverity.WARNING;

      const badNodeInfo: ProblemInfo = {
        line: line,
        column: character,
        endColumn: this.getNodeOrLineEnd(node, line),
        start: node.getStart(),
        end: node.getEnd(),
        type: faultType,
        severity: severity,
        problem: FaultID[faultId],
        suggest: cookBookMsgNum > 0 ? cookBookMsg[cookBookMsgNum] : '',
        rule: cookBookMsgNum > 0 && cookBookTg !== '' ? cookBookTg : faultDescr ? faultDescr : faultType,
        ruleTag: cookBookMsgNum,
        autofixable: autofixable,
        autofix: autofix,
      };

      this.problemsInfos.push(badNodeInfo);
    } else {
      logger.info(
        `Warning: ${this.sourceFile!.fileName} (${line}, ${character}): ${faultDescr ? faultDescr : faultType}`
      );
    }

    this.lineCounters[faultId]++;

    if (faultsAttrs[faultId].warning) {
      if (line != this.currentWarningLine) {
        this.currentWarningLine = line;
        ++this.totalWarningLines;
        this.warningLineNumbersString += line + ', '
      }
    } else if (line != this.currentErrorLine) {
      this.currentErrorLine = line;
      ++this.totalErrorLines;
      this.errorLineNumbersString += line + ', ';
    }
  }

  private visitTSNode(node: ts.Node): void {
    const self = this;
    visitTSNodeImpl(node);
    function visitTSNodeImpl(node: ts.Node): void {
      if (node === null || node.kind === null)
        return;

      self.totalVisitedNodes++;

      if (LinterConfig.tsSyntaxKindNames[node.kind] === 'StructDeclaration') {
        self.handleStructDeclaration(node);
        return;
      } 

      if (LinterConfig.terminalTokens.has(node.kind)) return;

      let incrementedType = LinterConfig.incrementOnlyTokens.get(node.kind);
      if (incrementedType !== undefined) {
        self.incrementCounters(node, incrementedType);
      } else {
        let handler = self.handlersMap.get(node.kind);
        if (handler !== undefined) {
          handler.call(self, node);
        }
      }

      ts.forEachChild(node, visitTSNodeImpl);
    }
  }

  private countInterfaceExtendsDifferentPropertyTypes(
    node: ts.Node,
    prop2type: Map<string, string>,
    propName: string,
    type: ts.TypeNode | undefined
  ) {
    if (type) {
      const methodType = type.getText();
      const propType = prop2type.get(propName);
      if (!propType) {
        prop2type.set(propName, methodType);
      } else if (propType !== methodType) {
        this.incrementCounters(node, FaultID.IntefaceExtendDifProps);
      }
    }
  }

  private typeHierarchyHasTypeError(type: ts.Type): boolean {
    const symbol = type.getSymbol();
    if (symbol?.getName() === 'Error') return true;
    
    const baseTypes = type.getBaseTypes();
    if (baseTypes) {
      for (const baseType of baseTypes) {
        if (this.typeHierarchyHasTypeError(baseType)) return true;
      }
    }
    return false;
  }

  private countDeclarationsWithDuplicateName(
    symbol: ts.Symbol | undefined, tsDeclNode: ts.Node, tsDeclKind?: ts.SyntaxKind
  ): void {
    // Sanity check.
    if (!symbol) return;

    // If specific declaration kind is provided, check against it.
    // Otherwise, use syntax kind of corresponding declaration node.
    if (this.tsUtils.symbolHasDuplicateName(symbol, tsDeclKind ?? tsDeclNode.kind))
      this.incrementCounters(tsDeclNode, FaultID.DeclWithDuplicateName);
  }

  private countClassMembersWithDuplicateName(tsClassDecl: ts.ClassDeclaration): void {
    for (const tsCurrentMember of tsClassDecl.members) {
      if (
        !tsCurrentMember.name ||
        !(ts.isIdentifier(tsCurrentMember.name) || ts.isPrivateIdentifier(tsCurrentMember.name))
      )
        continue;

      for (const tsClassMember of tsClassDecl.members) {
        if (tsCurrentMember === tsClassMember) continue;

        if (
          !tsClassMember.name ||
          !(ts.isIdentifier(tsClassMember.name) || ts.isPrivateIdentifier(tsClassMember.name))
        )
          continue;

        if (
          ts.isIdentifier(tsCurrentMember.name) &&
          ts.isPrivateIdentifier(tsClassMember.name) &&
          tsCurrentMember.name.text === tsClassMember.name.text.substring(1)
        ) {
          this.incrementCounters(tsCurrentMember, FaultID.DeclWithDuplicateName);
          break;
        }

        if (
          ts.isPrivateIdentifier(tsCurrentMember.name) &&
          ts.isIdentifier(tsClassMember.name) &&
          tsCurrentMember.name.text.substring(1) === tsClassMember.name.text
        ) {
          this.incrementCounters(tsCurrentMember, FaultID.DeclWithDuplicateName);
          break;
        }
      }
    }
  }

  private functionContainsThis(tsNode: ts.Node): boolean {
    let found = false;

    function visitNode(tsNode: ts.Node) {
      // Stop visiting child nodes if finished searching.
      if (found) return;

      if (tsNode.kind === ts.SyntaxKind.ThisKeyword) {
        found = true;
        return;
      }

      // Visit children nodes. Skip any local declaration that defines
      // its own scope as it needs to be checked separately.
      if (
        !ts.isClassDeclaration(tsNode) &&
        !ts.isClassExpression(tsNode) &&
        !ts.isModuleDeclaration(tsNode) &&
        !ts.isFunctionDeclaration(tsNode) &&
        !ts.isFunctionExpression(tsNode)
      ) 
        tsNode.forEachChild(visitNode);
    }

    visitNode(tsNode);

    return found;
  }

  private isPropertyRuntimeCheck(expr: ts.PropertyAccessExpression): boolean {
    // Check whether base expression is 'any' type and its property
    // is being checked in runtime (i.e. expression appears as condition
    // of if/for/while, or is an operand of '&&', '||' or '!' operators).
    const tsBaseExprType = this.tsTypeChecker.getTypeAtLocation(expr.expression);

    // Get parent node of the expression, pass through enclosing parentheses if needed.
    let exprParent = expr.parent;
    while (ts.isParenthesizedExpression(exprParent))
      exprParent = exprParent.parent;

    return (
      this.tsUtils.isAnyType(tsBaseExprType) &&
      (
        (ts.isIfStatement(exprParent) && expr === this.tsUtils.unwrapParenthesized(exprParent.expression)) ||
        (ts.isWhileStatement(exprParent) && expr === this.tsUtils.unwrapParenthesized(exprParent.expression)) ||
        (ts.isDoStatement(exprParent) && expr === this.tsUtils.unwrapParenthesized(exprParent.expression)) ||
        (ts.isForStatement(exprParent) && exprParent.condition && 
          expr === this.tsUtils.unwrapParenthesized(exprParent.condition)) ||
        (ts.isConditionalExpression(exprParent) && expr === this.tsUtils.unwrapParenthesized(exprParent.condition)) ||
        (ts.isBinaryExpression(exprParent) &&
          (exprParent.operatorToken.kind === ts.SyntaxKind.AmpersandAmpersandToken ||
            exprParent.operatorToken.kind === ts.SyntaxKind.BarBarToken)) ||
        (ts.isPrefixUnaryExpression(exprParent) && exprParent.operator === ts.SyntaxKind.ExclamationToken)
      )
    );
  }

  private isIIFEasNamespace(tsExpr: ts.PropertyAccessExpression): boolean {
    const nameSymbol = this.tsTypeChecker.getSymbolAtLocation(tsExpr.name);
    if (!nameSymbol) {
      const leftHandSymbol = this.tsTypeChecker.getSymbolAtLocation(tsExpr.expression);
      if (leftHandSymbol) {
        const decls = leftHandSymbol.getDeclarations();
        if (!decls || decls.length !== 1) return false;

        const leftHandDecl = decls[0];
        if (!ts.isVariableDeclaration(leftHandDecl)) return false;

        const varDecl = leftHandDecl as ts.VariableDeclaration;
        if (varDecl.initializer && ts.isCallExpression(varDecl.initializer)) {
          const callExpr = varDecl.initializer as ts.CallExpression;
          const expr = this.tsUtils.unwrapParenthesized(callExpr.expression);
          if (ts.isFunctionExpression(expr)) return true;
        }
      }
    }

    return false;
  }

  private isPrototypePropertyAccess(tsPropertyAccess: ts.PropertyAccessExpression): boolean {
    if (!(ts.isIdentifier(tsPropertyAccess.name) && tsPropertyAccess.name.text === 'prototype'))
      return false;

    // Check if property symbol is 'Prototype'
    const propAccessSym = this.tsTypeChecker.getSymbolAtLocation(tsPropertyAccess);
    if (this.tsUtils.isPrototypeSymbol(propAccessSym)) return true;

    // Check if symbol of LHS-expression is Class or Function.
    const tsBaseExpr = tsPropertyAccess.expression;
    const baseExprSym = this.tsTypeChecker.getSymbolAtLocation(tsBaseExpr);
    if (this.tsUtils.isTypeSymbol(baseExprSym) || this.tsUtils.isFunctionSymbol(baseExprSym))
      return true;

    // Check if type of LHS expression Function type or Any type.
    // The latter check is to cover cases with multiple prototype
    // chain (as the 'Prototype' property should be 'Any' type):
    //      X.prototype.prototype.prototype = ...
    const baseExprType = this.tsTypeChecker.getTypeAtLocation(tsBaseExpr);
    const baseExprTypeNode = this.tsTypeChecker.typeToTypeNode(
      baseExprType, undefined, ts.NodeBuilderFlags.None
    );

    return ((baseExprTypeNode && ts.isFunctionTypeNode(baseExprTypeNode)) || this.tsUtils.isAnyType(baseExprType));
  }

  private interfaceInharitanceLint(node: ts.Node, heritageClauses: ts.NodeArray<ts.HeritageClause>): void {
    for (const hClause of heritageClauses) {
      if (hClause.token !== ts.SyntaxKind.ExtendsKeyword) continue;

      const prop2type = new Map<string, string>();
      for (const tsTypeExpr of hClause.types) {
        const tsExprType = this.tsTypeChecker.getTypeAtLocation(tsTypeExpr.expression);
        if (tsExprType.isClass())
          this.incrementCounters(node, FaultID.InterfaceExtendsClass);
        else if (tsExprType.isClassOrInterface())
          this.lintForInterfaceExtendsDifferentPorpertyTypes(node, tsExprType, prop2type);
      }
    }
  }

  private lintForInterfaceExtendsDifferentPorpertyTypes(
    node: ts.Node, tsExprType: ts.Type, prop2type: Map<string, string>
  ): void {
    const props = tsExprType.getProperties();
    for (const p of props) {
      if (!p.declarations) continue;

      const decl: ts.Declaration = p.declarations[0];
      if (decl.kind === ts.SyntaxKind.MethodSignature) {
        this.countInterfaceExtendsDifferentPropertyTypes(
          node, prop2type, p.name, (decl as ts.MethodSignature).type
        );
      } else if (decl.kind === ts.SyntaxKind.MethodDeclaration) {
        this.countInterfaceExtendsDifferentPropertyTypes(
          node, prop2type, p.name, (decl as ts.MethodDeclaration).type
        );
      } else if (decl.kind === ts.SyntaxKind.PropertyDeclaration) {
        this.countInterfaceExtendsDifferentPropertyTypes(
          node, prop2type, p.name, (decl as ts.PropertyDeclaration).type
        );
      } else if (decl.kind == ts.SyntaxKind.PropertySignature) {
        this.countInterfaceExtendsDifferentPropertyTypes(
          node, prop2type, p.name, (decl as ts.PropertySignature).type
        );
      }
    }
  }

  /**
   * @param node AST node belonging to this `sourceFile`
   * @param startLine node's start line
   * @returns column index of the node's end if node is located on one line, line's end otherwise
   */
  private getNodeOrLineEnd(node: ts.Node, startLine: number): number {
    const pos = this.sourceFile!.getLineAndCharacterOfPosition(node.getEnd());
    return pos.line === startLine
      ? pos.character
      : this.sourceFile!.getLineEndOfPosition(node.getStart());
  }

  private handleObjectLiteralExpression(node: ts.Node) {
    let objectLiteralExpr = node as ts.ObjectLiteralExpression;

    // If object literal is a part of destructuring assignment, then don't process it further.
    if (this.tsUtils.isDestructuringAssignmentLHS(objectLiteralExpr))
      return;

    let objectLiteralType = this.tsTypeChecker.getContextualType(objectLiteralExpr);
    if (
      !this.tsUtils.validateObjectLiteralType(objectLiteralType) || this.tsUtils.hasMemberFunction(objectLiteralExpr) ||
      !this.tsUtils.validateFields(objectLiteralType, objectLiteralExpr)
    )
      this.incrementCounters(node, FaultID.ObjectLiteralNoContextType);
  }

  private handleArrayLiteralExpression(node: ts.Node) {
    // If array literal is a part of destructuring assignment, then
    // don't process it further.
    if (this.tsUtils.isDestructuringAssignmentLHS(node as ts.ArrayLiteralExpression)) return;

    let arrayLitNode = node as ts.ArrayLiteralExpression;

    // check element types
    if (ts.isUnionTypeNode(arrayLitNode)) this.incrementCounters(node, FaultID.TupleLiteral);

    let noContextTypeForArrayLiteral = false;
 
    // check that array literal consists of inferrable types 
    // e.g. there is no element which is untyped object literals
    let arrayLitElements = arrayLitNode.elements;
    for(let element of arrayLitElements ) {
      if( element.kind === ts.SyntaxKind.ObjectLiteralExpression ) {
        let objectLiteralType = this.tsTypeChecker.getContextualType(element);
        if ( !this.tsUtils.validateObjectLiteralType(objectLiteralType) ) {
          noContextTypeForArrayLiteral = true;
          break;
        }
      }
    }

    if (noContextTypeForArrayLiteral)  
      this.incrementCounters(node, FaultID.ArrayLiteralNoContextType);
  }

  private handleParameter(node: ts.Node) {
    let tsParam = node as ts.ParameterDeclaration;
    if (ts.isArrayBindingPattern(tsParam.name) || ts.isObjectBindingPattern(tsParam.name))
      this.incrementCounters(node, FaultID.DestructuringParameter);

    let tsParamMods = ts.getModifiers(tsParam);
    if (
      tsParamMods &&
      (this.tsUtils.hasModifier(tsParamMods, ts.SyntaxKind.PublicKeyword) ||
        this.tsUtils.hasModifier(tsParamMods, ts.SyntaxKind.ProtectedKeyword) ||
        this.tsUtils.hasModifier(tsParamMods, ts.SyntaxKind.PrivateKeyword))
    )
      this.incrementCounters(node, FaultID.ParameterProperties);
  }

  private handleEnumDeclaration(node: ts.Node) {
    let enumNode = node as ts.EnumDeclaration;
    this.countDeclarationsWithDuplicateName(
      this.tsTypeChecker.getSymbolAtLocation(enumNode.name), enumNode
    );

    let enumSymbol = this.tsTypeChecker.getSymbolAtLocation(enumNode.name);
    if (!enumSymbol) return;

    let enumDecls = enumSymbol.getDeclarations();
    if (!enumDecls) return;

    // Since type checker merges all declarations with the same name
    // into one symbol, we need to check that there's more than one
    // enum declaration related to that specific symbol.
    // See 'countDeclarationsWithDuplicateName' method for details.
    let enumDeclCount = 0;
    for (const decl of enumDecls) {
      if (decl.kind === ts.SyntaxKind.EnumDeclaration) enumDeclCount++;
    }

    if (enumDeclCount > 1) this.incrementCounters(node, FaultID.InterfaceOrEnumMerging);
  }

  private handleInterfaceDeclaration(node: ts.Node) {
    let interfaceNode = node as ts.InterfaceDeclaration;
    let iSymbol = this.tsTypeChecker.getSymbolAtLocation(interfaceNode.name);
    let iDecls = iSymbol ? iSymbol.getDeclarations() : null;
    if (iDecls) {
      // Since type checker merges all declarations with the same name
      // into one symbol, we need to check that there's more than one
      // interface declaration related to that specific symbol.
      // See 'countDeclarationsWithDuplicateName' method for details.
      let iDeclCount = 0;
      for (const decl of iDecls) {
        if (decl.kind === ts.SyntaxKind.InterfaceDeclaration) iDeclCount++;
      }

      if (iDeclCount > 1) this.incrementCounters(node, FaultID.InterfaceOrEnumMerging);
    }

    if (interfaceNode.heritageClauses) this.interfaceInharitanceLint(node, interfaceNode.heritageClauses);

    for (const typeElem of interfaceNode.members) {
      // ArkTs does not support otional properties of primitive types.
      if (typeElem.questionToken && ts.isPropertySignature(typeElem)) {
        let type = this.tsTypeChecker.getTypeAtLocation(typeElem.name);
        if (type && this.tsUtils.isPrimitiveType(type))
          this.incrementCounters(typeElem, FaultID.InterfaceOptionalProp);
      }
    }

    this.countDeclarationsWithDuplicateName(
      this.tsTypeChecker.getSymbolAtLocation(interfaceNode.name), interfaceNode
    );
  }

  private handleThrowStatement(node: ts.Node) {
    let throwStmt = node as ts.ThrowStatement;
    let throwExprType = this.tsTypeChecker.getTypeAtLocation(throwStmt.expression);
    if (!throwExprType.isClassOrInterface() || !this.typeHierarchyHasTypeError(throwExprType)) {
      let autofix: Autofix[] | undefined = undefined;
      if (this.autofixesInfo.shouldAutofix(throwStmt, FaultID.ThrowStatement))
        autofix = [ Autofixer.wrapExpressionInError(throwStmt.expression, this.tsUtils.isStringType(throwExprType)) ];
      this.incrementCounters(node, FaultID.ThrowStatement, true, autofix);
    }
  }

  private handleForStatement(node: ts.Node) {
    let tsForStmt = node as ts.ForStatement;
    let tsForInit = tsForStmt.initializer;
    if (tsForInit && (ts.isArrayLiteralExpression(tsForInit) || ts.isObjectLiteralExpression(tsForInit)))
      this.incrementCounters(tsForInit, FaultID.DestructuringAssignment);
  }

  private handleForInStatement(node: ts.Node) {
    let tsForInStmt = node as ts.ForInStatement;
    let tsForInInit = tsForInStmt.initializer;
    if (ts.isArrayLiteralExpression(tsForInInit) || ts.isObjectLiteralExpression(tsForInInit))
      this.incrementCounters(tsForInInit, FaultID.DestructuringAssignment);
    this.incrementCounters(node, FaultID.ForInStatement);
  }

  private handleForOfStatement(node: ts.Node) {
    let tsForOfStmt = node as ts.ForOfStatement;
    let tsForOfInit = tsForOfStmt.initializer;
    if (ts.isArrayLiteralExpression(tsForOfInit) || ts.isObjectLiteralExpression(tsForOfInit))
      this.incrementCounters(tsForOfInit, FaultID.DestructuringAssignment);

    let expr = tsForOfStmt.expression;
    let exprType = this.tsTypeChecker.getTypeAtLocation(expr);
    let exprTypeNode = this.tsTypeChecker.typeToTypeNode(
      exprType, undefined, ts.NodeBuilderFlags.None
    );

    if (!(ts.isArrayLiteralExpression(expr) || this.tsUtils.isArrayNotTupleType(exprTypeNode)))
      this.incrementCounters(node, FaultID.ForOfNonArray);
  }

  private handleTypeOperator(node: ts.Node) {
    const fullText = node.getFullText().trim();
    if (fullText.startsWith('keyof'))
      this.incrementCounters(node, FaultID.KeyOfOperator);
    else if (fullText.startsWith('readonly'))
      this.incrementCounters(node, FaultID.ReadonlyArr, true, [ Autofixer.fixReadonlyArr(node) ]);
  }

  private handleImportDeclaration(node: ts.Node) {
    let importDeclNode = node as ts.ImportDeclaration;
    let expr1 = importDeclNode.moduleSpecifier;
    if (expr1.kind === ts.SyntaxKind.StringLiteral) {
      if (!importDeclNode.importClause) this.incrementCounters(node, FaultID.ImportFromPath);

      const text = expr1.getText();
      if (text.endsWith('.js"') || text.endsWith('.js\''))
        this.incrementCounters(node, FaultID.JSExtensionInModuleIdent);

      if (importDeclNode.assertClause)
        this.incrementCounters(importDeclNode.assertClause, FaultID.ImportAssertion);
    }
  }

  private handlePropertyAccessExpression(node: ts.Node) {
    let propertyAccessNode = node as ts.PropertyAccessExpression;
    if (this.isPropertyRuntimeCheck(propertyAccessNode)) this.incrementCounters(node, FaultID.PropertyRuntimeCheck);
    if (this.isIIFEasNamespace(propertyAccessNode)) this.incrementCounters(node, FaultID.IifeAsNamespace);
    if (this.isPrototypePropertyAccess(propertyAccessNode))
      this.incrementCounters(propertyAccessNode.name, FaultID.Prototype);
  }

  private handlePropertyAssignmentOrDeclaration(node: ts.Node) {
    let prop = (node as ts.PropertyAssignment | ts.PropertyDeclaration).name;

    if (prop && (prop.kind === ts.SyntaxKind.NumericLiteral || prop.kind === ts.SyntaxKind.StringLiteral)) {
      let autofix : Autofix[] | undefined = Autofixer.fixLiteralAsPropertyName(node);
      let autofixable = autofix != undefined;
      if (!this.autofixesInfo.shouldAutofix(node, FaultID.LiteralAsPropertyName)) {
        autofix = undefined;
      }

      this.incrementCounters(node, FaultID.LiteralAsPropertyName, autofixable, autofix);
    }
  }

  private handleFunctionExpression(node: ts.Node) {
    let funcExpr = node as ts.FunctionExpression;
    let isGenerator = funcExpr.asteriskToken !== undefined;
    let containsThis = this.functionContainsThis(funcExpr.body);
    let isGeneric = funcExpr.typeParameters !== undefined && funcExpr.typeParameters.length > 0;

    let newParams = this.handleMissingParameterTypes(funcExpr);
    
    let [hasUnfixableReturnType, newRetTypeNode] = this.handleMissingReturnType(funcExpr);

    let autofixable = !isGeneric && !isGenerator && !containsThis &&
      newParams && !hasUnfixableReturnType;

    let autofix: Autofix[] | undefined;
    if (autofixable && this.autofixesInfo.shouldAutofix(node, FaultID.FunctionExpression)) {
      autofix = [ Autofixer.fixFunctionExpression(funcExpr, newParams, newRetTypeNode) ];
    }

    this.incrementCounters(node, FaultID.FunctionExpression, autofixable, autofix);
    if (!newParams) this.incrementCounters(funcExpr, FaultID.ArrowFunctionWithOmittedTypes, false);
    if (isGeneric) this.incrementCounters(funcExpr, FaultID.LambdaWithTypeParameters);
    if (isGenerator) this.incrementCounters(funcExpr, FaultID.GeneratorFunction);
    if (containsThis) this.incrementCounters(funcExpr, FaultID.FunctionContainsThis);
    if (hasUnfixableReturnType) this.incrementCounters(funcExpr, FaultID.LimitedReturnTypeInference);
  }

  private handleArrowFunction(node: ts.Node) {
    let arrowFunc = node as ts.ArrowFunction;
    this.handleMissingParameterTypes(arrowFunc);
    if (!arrowFunc.type) this.handleMissingReturnType(arrowFunc);

    if (arrowFunc.typeParameters && arrowFunc.typeParameters.length > 0)
      this.incrementCounters(node, FaultID.LambdaWithTypeParameters);
  }

  private handleMissingParameterTypes(signDecl: ts.SignatureDeclaration): 
  ts.NodeArray<ts.ParameterDeclaration> | undefined {
    let hasOmittedType = false;
    let autofixable = true;
    let autofix: Autofix[] | undefined;
    let isFuncExpr = signDecl.kind === ts.SyntaxKind.FunctionExpression;
    let newParams: ts.ParameterDeclaration[] = [];
    for (const param of signDecl.parameters) {
      if (isFuncExpr) newParams.push(param);
      if (param.type) continue;

      hasOmittedType = true;
      let paramType = this.tsTypeChecker.getTypeAtLocation(param);
      let paramTypeNode = this.tsTypeChecker.typeToTypeNode(paramType, param, ts.NodeBuilderFlags.None);
      if (!paramType || this.tsUtils.isUnsupportedType(paramType) || !paramTypeNode) {
        autofixable = false;
        continue;
      }

      if (isFuncExpr) {
        let newParam = Autofixer.fixParamWithoutType(param, paramTypeNode, true) as ts.ParameterDeclaration;
        newParams[newParams.length-1] = newParam;
      } else if (this.autofixesInfo.shouldAutofix(signDecl, FaultID.ArrowFunctionWithOmittedTypes)) {
        if (!autofix) autofix = [];
        autofix.push(Autofixer.fixParamWithoutType(param, paramTypeNode) as Autofix);
      }
    }
    // Don't report here if in function expression context.
    // See handleFunctionExpression for details.
    if (hasOmittedType && !isFuncExpr)
      this.incrementCounters(signDecl, FaultID.ArrowFunctionWithOmittedTypes, autofixable, autofix);

    return isFuncExpr && autofixable ? ts.factory.createNodeArray(newParams) : undefined;
  }

  private handleClassExpression(node: ts.Node) {
    let tsClassExpr = node as ts.ClassExpression;
    this.incrementCounters(node, FaultID.ClassExpression);
  }

  private handleFunctionDeclaration(node: ts.Node) {
    let tsFunctionDeclaration = node as ts.FunctionDeclaration;
    if (!tsFunctionDeclaration.type) this.handleMissingReturnType(tsFunctionDeclaration);
    if (tsFunctionDeclaration.name)
      this.countDeclarationsWithDuplicateName(
        this.tsTypeChecker.getSymbolAtLocation(tsFunctionDeclaration.name), tsFunctionDeclaration
      );

    let tsParams = tsFunctionDeclaration.parameters;
    for (const tsParam of tsParams)
      if (tsParam.questionToken) this.handleOptionalParam(tsParam);

    if (tsFunctionDeclaration.body && this.functionContainsThis(tsFunctionDeclaration.body))
      this.incrementCounters(node, FaultID.FunctionContainsThis);

    if (!ts.isSourceFile(tsFunctionDeclaration.parent) && !ts.isModuleBlock(tsFunctionDeclaration.parent))
      this.incrementCounters(tsFunctionDeclaration, FaultID.LocalFunction);

    if (tsFunctionDeclaration.asteriskToken) this.incrementCounters(node, FaultID.GeneratorFunction);
  }

  private handleMissingReturnType(funcLikeDecl: ts.FunctionLikeDeclaration): [boolean, ts.TypeNode | undefined] {
    // Note: Return type can't be inferred for function without body.
    if (!funcLikeDecl.body) return [false, undefined];

    let autofixable = false;
    let autofix : Autofix[] | undefined;
    let newRetTypeNode: ts.TypeNode | undefined;
    let isFuncExpr = ts.isFunctionExpression(funcLikeDecl);

    // Currently, ArkTS can't infer return type of function, when expression
    // in the return statement is a call to a function or method whose return
    // value type is omitted. In that case, we attempt to prepare an autofix.
    let hasLimitedRetTypeInference = this.hasLimitedTypeInferenceFromReturnExpr(funcLikeDecl.body);

    let tsSignature = this.tsTypeChecker.getSignatureFromDeclaration(funcLikeDecl);
    if (tsSignature) {
      let tsRetType = this.tsTypeChecker.getReturnTypeOfSignature(tsSignature);

      if (!tsRetType || this.tsUtils.isUnsupportedType(tsRetType)) {
        hasLimitedRetTypeInference = true;
      } else if (hasLimitedRetTypeInference) {
        newRetTypeNode = this.tsTypeChecker.typeToTypeNode(tsRetType, funcLikeDecl, ts.NodeBuilderFlags.None);
        if (newRetTypeNode && !isFuncExpr) {
          autofixable = true;
          if (this.autofixesInfo.shouldAutofix(funcLikeDecl, FaultID.LimitedReturnTypeInference)) {
            autofix = [Autofixer.fixReturnType(funcLikeDecl, newRetTypeNode)];
          }
        }
      }
    }

    // Don't report here if in function expression context.
    // See handleFunctionExpression for details.
    if (hasLimitedRetTypeInference && !isFuncExpr)
      this.incrementCounters(funcLikeDecl, FaultID.LimitedReturnTypeInference, autofixable, autofix);

    return [hasLimitedRetTypeInference && !newRetTypeNode, newRetTypeNode];
  }
  
  private hasLimitedTypeInferenceFromReturnExpr(funBody: ts.ConciseBody): boolean {
    let hasLimitedTypeInference = false;
    const self = this;
    function visitNode(tsNode: ts.Node): void {
      if (hasLimitedTypeInference) return;

      if (
        ts.isReturnStatement(tsNode) && tsNode.expression &&
        self.tsUtils.isCallToFunctionWithOmittedReturnType(self.tsUtils.unwrapParenthesized(tsNode.expression))
      ) {
        hasLimitedTypeInference = true;
        return;
      }

      // Visit children nodes. Don't traverse other nested function-like declarations.
      if (
        !ts.isFunctionDeclaration(tsNode) &&
        !ts.isFunctionExpression(tsNode) &&
        !ts.isMethodDeclaration(tsNode) &&
        !ts.isAccessor(tsNode) &&
        !ts.isArrowFunction(tsNode)
      )
        tsNode.forEachChild(visitNode);
    }

    if (ts.isBlock(funBody)) {
      visitNode(funBody);
    } else {
      const tsExpr = this.tsUtils.unwrapParenthesized(funBody);
      hasLimitedTypeInference = this.tsUtils.isCallToFunctionWithOmittedReturnType(tsExpr);
    }

    return hasLimitedTypeInference;
  }

  private handleOptionalParam(tsParam: ts.ParameterDeclaration) {
    const paramType = this.tsTypeChecker.getTypeAtLocation(tsParam);
    if (this.tsUtils.isNumberLikeType(paramType) || this.tsUtils.isBooleanType(paramType)) {
      this.incrementCounters(tsParam, FaultID.FuncOptionalParams);
    }
  }

  private handlePrefixUnaryExpression(node: ts.Node) {
    let tsUnaryArithm = node as ts.PrefixUnaryExpression;
    let tsUnaryOp = tsUnaryArithm.operator;
    if (
      tsUnaryOp === ts.SyntaxKind.PlusToken ||
      tsUnaryOp === ts.SyntaxKind.MinusToken ||
      tsUnaryOp === ts.SyntaxKind.TildeToken
    ) {
      const tsOperatndType = this.tsTypeChecker.getTypeAtLocation(tsUnaryArithm.operand);
      if (!(tsOperatndType.getFlags() & ts.TypeFlags.NumberLike))
        this.incrementCounters(node, FaultID.UnaryArithmNotNumber);
    }
  }

  private handleBinaryExpression(node: ts.Node) {
    let tsBinaryExpr = node as ts.BinaryExpression;
    let tsLhsExpr = tsBinaryExpr.left;
    let tsRhsExpr = tsBinaryExpr.right;

    if (this.tsUtils.isAssignmentOperator(tsBinaryExpr.operatorToken)) {
      if (ts.isObjectLiteralExpression(tsLhsExpr) || ts.isArrayLiteralExpression(tsLhsExpr))
        this.incrementCounters(node, FaultID.DestructuringAssignment);

      if (ts.isPropertyAccessExpression(tsLhsExpr)) {
        const tsLhsSymbol = this.tsTypeChecker.getSymbolAtLocation(tsLhsExpr);
        const tsLhsBaseSymbol = this.tsTypeChecker.getSymbolAtLocation(tsLhsExpr.expression);
        if (
          this.tsUtils.isMethodAssignment(tsLhsSymbol) && tsLhsBaseSymbol &&
          (tsLhsBaseSymbol.flags & ts.SymbolFlags.Function) !== 0
        )
          this.incrementCounters(tsLhsExpr, FaultID.PropertyDeclOnFunction);
      }
    }

    let leftOperandType = this.tsTypeChecker.getTypeAtLocation(tsLhsExpr);
    let rightOperandType = this.tsTypeChecker.getTypeAtLocation(tsRhsExpr);

    if (tsBinaryExpr.operatorToken.kind === ts.SyntaxKind.PlusToken) {
      if (this.tsUtils.isNumberType(leftOperandType) && this.tsUtils.isNumberType(rightOperandType))
        return;
      else if (this.tsUtils.isStringType(leftOperandType) || this.tsUtils.isStringType(rightOperandType))
        return;
      else
        this.incrementCounters(node, FaultID.AddWithWrongType);
    } else if (
      tsBinaryExpr.operatorToken.kind === ts.SyntaxKind.AmpersandToken ||
      tsBinaryExpr.operatorToken.kind === ts.SyntaxKind.BarToken ||
      tsBinaryExpr.operatorToken.kind === ts.SyntaxKind.CaretToken ||
      tsBinaryExpr.operatorToken.kind === ts.SyntaxKind.LessThanLessThanToken ||
      tsBinaryExpr.operatorToken.kind === ts.SyntaxKind.GreaterThanGreaterThanToken ||
      tsBinaryExpr.operatorToken.kind === ts.SyntaxKind.GreaterThanGreaterThanGreaterThanToken
    ) {
      if (!(this.tsUtils.isNumberType(leftOperandType) && this.tsUtils.isNumberType(rightOperandType)))
        this.incrementCounters(node, FaultID.BitOpWithWrongType);
    } else if (tsBinaryExpr.operatorToken.kind === ts.SyntaxKind.CommaToken) {
      // CommaOpertor is allowed in 'for' statement initalizer and incrementor
      let tsExprNode: ts.Node = tsBinaryExpr;
      let tsParentNode = tsExprNode.parent;
      while (tsParentNode && tsParentNode.kind === ts.SyntaxKind.BinaryExpression) {
        tsExprNode = tsParentNode;
        tsParentNode = tsExprNode.parent;
      }

      if (tsParentNode && tsParentNode.kind === ts.SyntaxKind.ForStatement) {
        const tsForNode = tsParentNode as ts.ForStatement;
        if (tsExprNode === tsForNode.initializer || tsExprNode === tsForNode.incrementor) return;
      }
      this.incrementCounters(node, FaultID.CommaOperator);
    } else if (tsBinaryExpr.operatorToken.kind === ts.SyntaxKind.InstanceOfKeyword) {
      const leftExpr = this.tsUtils.unwrapParenthesized(tsBinaryExpr.left);
      const leftSymbol = this.tsTypeChecker.getSymbolAtLocation(leftExpr);
      // In STS, the left-hand side expression may be of any reference type, otherwise
      // a compile-time error occurs. In addition, the left operand in STS cannot be a type.
      if (ts.isTypeNode(leftExpr) || !this.tsUtils.isReferenceType(leftOperandType) || this.tsUtils.isTypeSymbol(leftSymbol))
        this.incrementCounters(node, FaultID.InstanceofUnsupported);
    }
    else if (tsBinaryExpr.operatorToken.kind === ts.SyntaxKind.EqualsToken) {
      if (
        leftOperandType.isClassOrInterface() && rightOperandType.isClassOrInterface() &&
        !this.tsUtils.relatedByInheritanceOrIdentical(rightOperandType, leftOperandType)
      )
        this.incrementCounters(tsBinaryExpr, FaultID.StructuralIdentity);
      if (
        ts.isIdentifier(tsRhsExpr) && tsRhsExpr.originalKeywordKind === ts.SyntaxKind.UndefinedKeyword &&
        (!leftOperandType.isUnion() || this.tsUtils.isUnsupportedUnionType(leftOperandType))
      )
        this.incrementCounters(tsRhsExpr, FaultID.UndefinedValue);
    }
  }

  private handleVariableDeclarationList(node: ts.Node) {
    let varDeclFlags = ts.getCombinedNodeFlags(node);
    if (!(varDeclFlags & (ts.NodeFlags.Let | ts.NodeFlags.Const)))
      this.incrementCounters(node, FaultID.VarDeclaration);
  }

  private handleVariableDeclaration(node: ts.Node) {
    let tsVarDecl = node as ts.VariableDeclaration;
    if (ts.isArrayBindingPattern(tsVarDecl.name) || ts.isObjectBindingPattern(tsVarDecl.name))
      this.incrementCounters(node, FaultID.DestructuringDeclaration);

    {
      // Check variable declaration for duplicate name.
      const visitBindingPatternNames = (tsBindingName: ts.BindingName) => {
        if (ts.isIdentifier(tsBindingName))
          // The syntax kind of the declaration is defined here by the parent of 'BindingName' node.
          this.countDeclarationsWithDuplicateName(
            this.tsTypeChecker.getSymbolAtLocation(tsBindingName), tsBindingName,
            tsBindingName.parent.kind
          );
        else {
          for (const tsBindingElem of tsBindingName.elements) {
            if (ts.isOmittedExpression(tsBindingElem)) continue;

            visitBindingPatternNames(tsBindingElem.name);
          }
        }
      };
      
      if (tsVarDecl.exclamationToken) this.incrementCounters(node, FaultID.DefiniteAssignment);

      visitBindingPatternNames(tsVarDecl.name);
    }

    if (tsVarDecl.type && tsVarDecl.initializer) {
      let tsVarInit = tsVarDecl.initializer;
      let tsVarType = this.tsTypeChecker.getTypeAtLocation(tsVarDecl.type);
      let tsInitType = this.tsTypeChecker.getTypeAtLocation(tsVarInit);
      if (
        tsVarType.isClassOrInterface() && tsInitType.isClassOrInterface() &&
        !this.tsUtils.relatedByInheritanceOrIdentical(tsInitType, tsVarType)
      )
        this.incrementCounters(tsVarDecl, FaultID.StructuralIdentity);
      if (
        ts.isIdentifier(tsVarInit) && tsVarInit.originalKeywordKind === ts.SyntaxKind.UndefinedKeyword &&
        (!tsVarType.isUnion() || this.tsUtils.isUnsupportedUnionType(tsVarType))
      )
        this.incrementCounters(tsVarDecl.initializer, FaultID.UndefinedValue);
    }
  }

  private handleCatchClause(node: ts.Node) {
    let tsCatch = node as ts.CatchClause;
    // In TS catch clause doesn't permit specification of the exception varible type except 'any' or 'unknown'.
    // It is not compatible with STS 'catch' where the exception variable has to be of type
    // Error or derived from it.
    // So each 'catch' which has explicit type for the exception object goes to problems in strict mode.
    if (tsCatch.variableDeclaration && tsCatch.variableDeclaration.type) {
      let autofix: Autofix[] | undefined = undefined;
      if (this.autofixesInfo.shouldAutofix(tsCatch, FaultID.CatchWithUnsupportedType))
        autofix = [ Autofixer.dropTypeOnVarDecl(tsCatch.variableDeclaration) ];
      this.incrementCounters(node, FaultID.CatchWithUnsupportedType, true, autofix);
    }
  }

  private handleClassDeclaration(node: ts.Node) {
    let tsClassDecl = node as ts.ClassDeclaration;
    if (tsClassDecl.name)
      this.countDeclarationsWithDuplicateName(
        this.tsTypeChecker.getSymbolAtLocation(tsClassDecl.name),
        tsClassDecl
      );

    this.countClassMembersWithDuplicateName(tsClassDecl);

    if (tsClassDecl.heritageClauses) {
      for (const hClause of tsClassDecl.heritageClauses) {
        if (hClause && hClause.token === ts.SyntaxKind.ImplementsKeyword) {
          for (const tsTypeExpr of hClause.types) {
            const tsExprType = this.tsTypeChecker.getTypeAtLocation(tsTypeExpr.expression);
            if (tsExprType.isClass()) this.incrementCounters(tsTypeExpr, FaultID.ImplementsClass);
          }
        }
      }
    }
  }

  private handleModuleDeclaration(node: ts.Node) {
    let tsModuleDecl = node as ts.ModuleDeclaration;
    this.countDeclarationsWithDuplicateName(
      this.tsTypeChecker.getSymbolAtLocation(tsModuleDecl.name),
      tsModuleDecl
    );

    let tsModuleBody = tsModuleDecl.body;
    let tsModifiers = ts.getModifiers(tsModuleDecl);
    if (tsModuleBody) {
      if (ts.isModuleBlock(tsModuleBody)) {
        for (const tsModuleStmt of tsModuleBody.statements) {
          switch (tsModuleStmt.kind) {
            case ts.SyntaxKind.VariableStatement:
            case ts.SyntaxKind.FunctionDeclaration:
            case ts.SyntaxKind.ClassDeclaration:
            case ts.SyntaxKind.InterfaceDeclaration:
            case ts.SyntaxKind.TypeAliasDeclaration:
            case ts.SyntaxKind.EnumDeclaration:
              break;
            // Nested namespace declarations are prohibited
            // but there is no cookbook recipe for it!
            case ts.SyntaxKind.ModuleDeclaration:
              break;
            default:
              this.incrementCounters(tsModuleStmt, FaultID.NonDeclarationInNamespace);
              break;
          }
        }
      }
    } else if (this.tsUtils.hasModifier(tsModifiers, ts.SyntaxKind.DeclareKeyword)) {
      this.incrementCounters(tsModuleDecl, FaultID.ShorthandAmbientModuleDecl);
    }

    if (ts.isStringLiteral(tsModuleDecl.name) && tsModuleDecl.name.text.includes('*'))
      this.incrementCounters(tsModuleDecl, FaultID.WildcardsInModuleName);
  }

  private handleTypeAliasDeclaration(node: ts.Node) {
    let tsTypeAlias = node as ts.TypeAliasDeclaration;
    this.countDeclarationsWithDuplicateName(
      this.tsTypeChecker.getSymbolAtLocation(tsTypeAlias.name), tsTypeAlias
    );
  }

  private handleImportClause(node: ts.Node) {
    let tsImportClause = node as ts.ImportClause;
    if (tsImportClause.name) {
      this.countDeclarationsWithDuplicateName(
        this.tsTypeChecker.getSymbolAtLocation(tsImportClause.name), tsImportClause
      );
    }

    if (tsImportClause.namedBindings && ts.isNamedImports(tsImportClause.namedBindings)) {
      let nonDefaultSpecs: ts.ImportSpecifier[] = [];
      let defaultSpec: ts.ImportSpecifier | undefined = undefined;
      for (const importSpec of tsImportClause.namedBindings.elements) {
        if (this.tsUtils.isDefaultImport(importSpec)) defaultSpec = importSpec;
        else nonDefaultSpecs.push(importSpec);
      }
      if (defaultSpec) {
        let autofix: Autofix[] | undefined = undefined;
        if (this.autofixesInfo.shouldAutofix(defaultSpec, FaultID.DefaultImport)) 
          autofix = [ Autofixer.fixDefaultImport(tsImportClause, defaultSpec, nonDefaultSpecs) ];
        this.incrementCounters(defaultSpec, FaultID.DefaultImport, true, autofix);
      }
    }

    if (tsImportClause.isTypeOnly) {
      let autofix: Autofix[] | undefined = undefined;
      if (this.autofixesInfo.shouldAutofix(node, FaultID.TypeOnlyImport)) 
        autofix = [ Autofixer.dropTypeOnlyFlag(tsImportClause) ];
      this.incrementCounters(node, FaultID.TypeOnlyImport, true, autofix);
    }
  }

  private handleImportSpecifier(node: ts.Node) {
    let importSpec = node as ts.ImportSpecifier;
    this.countDeclarationsWithDuplicateName(
      this.tsTypeChecker.getSymbolAtLocation(importSpec.name), importSpec
    );

    // Don't report or autofix type-only flag on default import if the latter has been autofixed already.
    if (
      importSpec.isTypeOnly && 
      (!this.tsUtils.isDefaultImport(importSpec) || !this.autofixesInfo.shouldAutofix(importSpec, FaultID.DefaultImport))
    ) {
      let autofix: Autofix[] | undefined = undefined;
      if (this.autofixesInfo.shouldAutofix(node, FaultID.TypeOnlyImport)) 
        autofix = [ Autofixer.dropTypeOnlyFlag(importSpec) ];
      this.incrementCounters(node, FaultID.TypeOnlyImport, true, autofix);
    }
  }

  private handleNamespaceImport(node: ts.Node) {
    let tsNamespaceImport = node as ts.NamespaceImport;
    this.countDeclarationsWithDuplicateName(
      this.tsTypeChecker.getSymbolAtLocation(tsNamespaceImport.name), tsNamespaceImport
    );
  }

  private handleTypeAssertionExpression(node: ts.Node) {
    let tsTypeAssertion = node as ts.TypeAssertion;
    if (tsTypeAssertion.type.getText() === 'const')
      this.incrementCounters(tsTypeAssertion, FaultID.ConstAssertion);
    else
      this.incrementCounters(node, FaultID.TypeAssertion, true, [ Autofixer.fixTypeAssertion(tsTypeAssertion) ]);
  }

  private handleMethodDeclaration(node: ts.Node) {
    let tsMethodDecl = node as ts.MethodDeclaration;
    if (!tsMethodDecl.type)
      this.handleMissingReturnType(tsMethodDecl);
    if (tsMethodDecl.asteriskToken)
      this.incrementCounters(node, FaultID.GeneratorFunction);
  }

  private handleClassStaticBlockDeclaration(node: ts.Node) {
    if (ts.isClassDeclaration(node.parent)) {
      const tsClassDecl = node.parent as ts.ClassDeclaration;
      let className = '';
      if (tsClassDecl.name)
        // May be undefined in `export default class { ... }`.
        className = tsClassDecl.name.text;

      if (this.staticBlocks.has(className))
        this.incrementCounters(node, FaultID.MultipleStaticBlocks);
      else
        this.staticBlocks.add(className);
    }
  }

  private handleSwitchStatement(node: ts.Node) {
    let tsSwitchStmt = node as ts.SwitchStatement;
    let tsSwitchExprType = this.tsTypeChecker.getTypeAtLocation(tsSwitchStmt.expression);

    if (
      !(tsSwitchExprType.getFlags() & (ts.TypeFlags.NumberLike | ts.TypeFlags.StringLike)) &&
      !this.tsUtils.isEnumType(tsSwitchExprType)
    )
      this.incrementCounters(tsSwitchStmt.expression, FaultID.SwitchSelectorInvalidType);

    for (const tsCaseClause of tsSwitchStmt.caseBlock.clauses) {
      if (ts.isCaseClause(tsCaseClause)) {
        const tsCaseExpr = tsCaseClause.expression;
        const tsCaseExprType = this.tsTypeChecker.getTypeAtLocation(tsCaseExpr);
        if (
          !(
            ts.isNumericLiteral(tsCaseExpr) ||
            ts.isStringLiteralLike(tsCaseExpr) ||
            tsCaseExprType.flags & ts.TypeFlags.EnumLike
          )
        ) 
          this.incrementCounters(tsCaseExpr, FaultID.CaseExpressionNonConst);
      }
    }
  }

  private handleIdentifier(node: ts.Node) {
    let tsIdentifier = node as ts.Identifier;
    let tsIdentSym = this.tsTypeChecker.getSymbolAtLocation(tsIdentifier);

    if (tsIdentSym) {
      this.handleNamespaceAsObject(tsIdentifier, tsIdentSym);

      if (
        (tsIdentSym.flags & ts.SymbolFlags.Module) !== 0 &&
        (tsIdentSym.flags & ts.SymbolFlags.Transient) !== 0 &&
        tsIdentifier.text === 'globalThis'
      )
        this.incrementCounters(node, FaultID.GlobalThis);

      if (this.tsUtils.isGlobalSymbol(tsIdentSym) && TsUtils.LIMITED_STD_GLOBAL_VAR.includes(tsIdentSym.getName()))
        this.incrementCounters(node, FaultID.LimitedStdLibApi);
    }
  }

  private handleNamespaceAsObject(tsIdentifier: ts.Identifier, tsIdentSym: ts.Symbol) {
    if (
      tsIdentSym &&
      (tsIdentSym.getFlags() & ts.SymbolFlags.Module) !== 0 &&
      (tsIdentSym.getFlags() & ts.SymbolFlags.Variable) === 0 &&
      !ts.isModuleDeclaration(tsIdentifier.parent)
    ) {
      // If module name is duplicated by another declaration, this increases the possibility
      // of finding a lot of false positives. Thus, do not check further in that case.
      if (!this.tsUtils.symbolHasDuplicateName(tsIdentSym, ts.SyntaxKind.ModuleDeclaration)) {
        // If module name is the right-most name of Property Access chain or Qualified name,
        // or it's a separate identifier expression, then module is being referenced as an object.
        let tsIdentParent: ts.Node = tsIdentifier;

        while (ts.isPropertyAccessExpression(tsIdentParent.parent) || ts.isQualifiedName(tsIdentParent.parent))
          tsIdentParent = tsIdentParent.parent;

        if (
          (!ts.isPropertyAccessExpression(tsIdentParent) && !ts.isQualifiedName(tsIdentParent)) ||
          (ts.isPropertyAccessExpression(tsIdentParent) && tsIdentifier === tsIdentParent.name) ||
          (ts.isQualifiedName(tsIdentParent) && tsIdentifier === tsIdentParent.right)
        ) 
          this.incrementCounters(tsIdentifier, FaultID.NamespaceAsObject);
      }
    }
  }

  private handleElementAccessExpression(node: ts.Node) {
    let tsElementAccessExpr = node as ts.ElementAccessExpression;
    let tsElemAccessBaseExprType = this.tsTypeChecker.getTypeAtLocation(tsElementAccessExpr.expression);

    if (
      (tsElemAccessBaseExprType.isClassOrInterface() && !this.tsUtils.isGenericArrayType(tsElemAccessBaseExprType)) ||
      this.tsUtils.isObjectLiteralType(tsElemAccessBaseExprType) || this.tsUtils.isEnumType(tsElemAccessBaseExprType) ||
      this.tsUtils.isThisOrSuperExpr(tsElementAccessExpr.expression) 
    ) {
      let autofix = Autofixer.fixPropertyAccessByIndex(node);
      let autofixable = autofix != undefined;
      if (!this.autofixesInfo.shouldAutofix(node, FaultID.PropertyAccessByIndex))
        autofix = undefined;

      this.incrementCounters(node, FaultID.PropertyAccessByIndex, autofixable, autofix);
    }
  }

  private handleEnumMember(node: ts.Node) {
    let tsEnumMember = node as ts.EnumMember;
    let tsEnumMemberType = this.tsTypeChecker.getTypeAtLocation(tsEnumMember);
    let constVal = this.tsTypeChecker.getConstantValue(tsEnumMember);
    
    if (tsEnumMember.initializer && !this.tsUtils.isValidEnumMemberInit(tsEnumMember.initializer)) 
      this.incrementCounters(node, FaultID.EnumMemberNonConstInit);
    
    // check for type - all members should be of same type
    let enumDecl = tsEnumMember.parent;
    let firstEnumMember = enumDecl.members[0];
    let firstEnumMemberType = this.tsTypeChecker.getTypeAtLocation(firstEnumMember);
    let firstElewmVal = this.tsTypeChecker.getConstantValue(firstEnumMember);
    // each string enum member has its own type
    // so check that value type is string
    if( constVal !==undefined && typeof constVal === 'string' && 
        firstElewmVal !==undefined && typeof firstElewmVal === 'string' )
      return;
    if( constVal !==undefined && typeof constVal === 'number' && 
      firstElewmVal !==undefined && typeof firstElewmVal === 'number' )
      return;
    if(firstEnumMemberType !== tsEnumMemberType) {
      this.incrementCounters(node, FaultID.EnumMemberNonConstInit);   
    }
  }

  private handleExportDeclaration(node: ts.Node) {
    let tsExportDecl = node as ts.ExportDeclaration;
    if (tsExportDecl.isTypeOnly) {
      let autofix: Autofix[] | undefined = undefined;
      if (this.autofixesInfo.shouldAutofix(node, FaultID.TypeOnlyExport))
        autofix = [ Autofixer.dropTypeOnlyFlag(tsExportDecl) ];
      this.incrementCounters(node, FaultID.TypeOnlyExport, true, autofix);
    }

    if (tsExportDecl.moduleSpecifier)
      this.incrementCounters(node, FaultID.ReExporting);

    let exportClause = tsExportDecl.exportClause;
    if (exportClause && ts.isNamedExports(exportClause)) {
      this.incrementCounters(exportClause, FaultID.ExportListDeclaration);

      for (const exportSpec of exportClause.elements) {
        if (exportSpec.propertyName)
          this.incrementCounters(exportSpec, FaultID.ExportRenaming);

        if (exportSpec.isTypeOnly) {
          let autofix: Autofix[] | undefined = undefined;
          if (this.autofixesInfo.shouldAutofix(exportSpec, FaultID.TypeOnlyExport))
            autofix = [ Autofixer.dropTypeOnlyFlag(exportSpec) ];
          this.incrementCounters(exportSpec, FaultID.TypeOnlyExport, true, autofix);
        }
      }
    }
  }

  private handleExportAssignment(node: ts.Node) {
    let tsExportAssignment = node as ts.ExportAssignment;
    if (tsExportAssignment.isExportEquals)
      this.incrementCounters(node, FaultID.ExportAssignment);
  }

  private handleCallExpression(node: ts.Node) {
    let tsCallExpr = node as ts.CallExpression;

    this.handleImportCall(tsCallExpr);
    this.handleRequireCall(tsCallExpr);
    // NOTE: Keep handleFunctionApplyBindPropCall above handleGenericCallWithNoTypeArgs here!!!
    this.handleFunctionApplyBindPropCall(tsCallExpr);
    this.handleGenericCallWithNoTypeArgs(tsCallExpr);
    this.handleStructIdentAndUndefinedInArgs(tsCallExpr);
    this.handleStdlibAPICall(tsCallExpr);
  }

  private handleImportCall(tsCallExpr: ts.CallExpression) {
    if (tsCallExpr.expression.kind === ts.SyntaxKind.ImportKeyword) {
      this.incrementCounters(tsCallExpr, FaultID.DynamicImport);
      const tsArgs = tsCallExpr.arguments;
      if (tsArgs.length > 1 && ts.isObjectLiteralExpression(tsArgs[1])) {
        for (const tsProp of tsArgs[1].properties) {
          if (ts.isPropertyAssignment(tsProp) || ts.isShorthandPropertyAssignment(tsProp)) {
            if (tsProp.name.getText() === 'assert') {
              this.incrementCounters(tsProp, FaultID.ImportAssertion);
              break;
            }
          }
        }
      }
    }
  }

  private handleRequireCall(tsCallExpr: ts.CallExpression) {
    if (
      ts.isIdentifier(tsCallExpr.expression) &&
      tsCallExpr.expression.text === 'require' &&
      ts.isVariableDeclaration(tsCallExpr.parent)
    ) {
      let tsType = this.tsTypeChecker.getTypeAtLocation(tsCallExpr.expression);
      if (this.tsUtils.isInterfaceType(tsType) && tsType.symbol.name === 'NodeRequire') 
        this.incrementCounters(tsCallExpr.parent, FaultID.ImportAssignment);
    }
  }

  private handleGenericCallWithNoTypeArgs(callLikeExpr: ts.CallExpression | ts.NewExpression) {
    let callSignature = this.tsTypeChecker.getResolvedSignature(callLikeExpr);
    if (!callSignature) return;

    let tsSyntaxKind = ts.isNewExpression(callLikeExpr) ? ts.SyntaxKind.Constructor : ts.SyntaxKind.FunctionDeclaration;
    let signDecl = this.tsTypeChecker.signatureToSignatureDeclaration(callSignature, tsSyntaxKind,
      undefined, ts.NodeBuilderFlags.WriteTypeArgumentsOfSignature | ts.NodeBuilderFlags.IgnoreErrors);
  
    if (signDecl?.typeArguments) {
      let resolvedTypeArgs = signDecl.typeArguments;

      let startTypeArg = callLikeExpr.typeArguments?.length ?? 0;
      for (let i = startTypeArg; i < resolvedTypeArgs.length; ++i) {
        if (!this.tsUtils.isSupportedType(resolvedTypeArgs[i])) {
          this.incrementCounters(callLikeExpr, FaultID.GenericCallNoTypeArgs);
          break;
        }
      }
    }
  }

  private handleFunctionApplyBindPropCall(tsCallExpr: ts.CallExpression) {
    let tsExpr = tsCallExpr.expression;
    if (
      ts.isPropertyAccessExpression(tsExpr) &&
      (tsExpr.name.text === 'apply' || tsExpr.name.text === 'bind' || tsExpr.name.text === 'call')
    ) {
      const tsSymbol = this.tsTypeChecker.getSymbolAtLocation(tsExpr.expression);
      if (this.tsUtils.isFunctionOrMethod(tsSymbol))
        this.incrementCounters(tsCallExpr, FaultID.FunctionApplyBindCall);
    }
  }

  private handleStructIdentAndUndefinedInArgs(tsCallOrNewExpr: ts.CallExpression | ts.NewExpression) {
    let tsSignature = this.tsTypeChecker.getResolvedSignature(tsCallOrNewExpr);
    if (!tsSignature || !tsCallOrNewExpr.arguments) return;

    for (let argIndex = 0; argIndex < tsCallOrNewExpr.arguments.length; ++argIndex) {
      let tsArg = tsCallOrNewExpr.arguments[argIndex];
      let tsArgType = this.tsTypeChecker.getTypeAtLocation(tsArg);
      if (!tsArgType) continue;

      let paramIndex = argIndex < tsSignature.parameters.length ? argIndex : tsSignature.parameters.length-1;
      let tsParamSym = tsSignature.parameters[paramIndex];
      if (!tsParamSym) continue;

      let tsParamDecl = tsParamSym.valueDeclaration;
      if (tsParamDecl && ts.isParameter(tsParamDecl)) {
        let tsParamType = this.tsTypeChecker.getTypeOfSymbolAtLocation(tsParamSym, tsParamDecl);
        if (tsParamDecl.dotDotDotToken && this.tsUtils.isGenericArrayType(tsParamType) && tsParamType.typeArguments)
          tsParamType = tsParamType.typeArguments[0];

        if (!tsParamType) continue;

        if (
          tsArgType.isClassOrInterface() && tsParamType.isClassOrInterface() &&
          !this.tsUtils.relatedByInheritanceOrIdentical(tsArgType, tsParamType)
        ) 
          this.incrementCounters(tsArg, FaultID.StructuralIdentity);
        if (
          ts.isIdentifier(tsArg) && tsArg.originalKeywordKind === ts.SyntaxKind.UndefinedKeyword &&
          (!tsParamType.isUnion() || this.tsUtils.isUnsupportedUnionType(tsParamType))
        )
          this.incrementCounters(tsArg, FaultID.UndefinedValue);
      }
    }
  }

  private handleStdlibAPICall(callExpr: ts.CallExpression) {
    let callSignature = this.tsTypeChecker.getResolvedSignature(callExpr);
    if (!callSignature) return;

    let sym = this.tsTypeChecker.getSymbolAtLocation(callExpr.expression);
    if (sym) {
      let name = sym.getName();
      if (
        (this.tsUtils.isGlobalSymbol(sym) && TsUtils.LIMITED_STD_GLOBAL_FUNC.includes(name)) ||
        (this.tsUtils.isStdObjectAPI(sym) && TsUtils.LIMITED_STD_OBJECT_API.includes(name)) ||
        (this.tsUtils.isStdReflectAPI(sym) && TsUtils.LIMITED_STD_REFLECT_API.includes(name)) ||
        (this.tsUtils.isStdProxyHandlerAPI(sym) && TsUtils.LIMITED_STD_PROXYHANDLER_API.includes(name)) ||
        (this.tsUtils.isStdArrayAPI(sym) && TsUtils.LIMITED_STD_ARRAY_API.includes(name)) ||
        (this.tsUtils.isStdArrayBufferAPI(sym) && TsUtils.LIMITED_STD_ARRAYBUFFER_API.includes(name))
      ) {
        this.incrementCounters(callExpr, FaultID.LimitedStdLibApi);
      }
    }
  }

  private handleNewExpression(node: ts.Node) {
    let tsNewExpr = node as ts.NewExpression;
    this.handleGenericCallWithNoTypeArgs(tsNewExpr);
    this.handleStructIdentAndUndefinedInArgs(tsNewExpr);
  }

  private handleAsExpression(node: ts.Node) {
    let tsAsExpr = node as ts.AsExpression;
    if (tsAsExpr.type.getText() === 'const') this.incrementCounters(node, FaultID.ConstAssertion);
    
    let targetType = this.tsTypeChecker.getTypeAtLocation(tsAsExpr.type);
    let exprType = this.tsTypeChecker.getTypeAtLocation(tsAsExpr.expression);
    if (
      targetType.isClassOrInterface() && exprType.isClassOrInterface() &&
      !this.tsUtils.relatedByInheritanceOrIdentical(exprType, targetType) &&
      !this.tsUtils.relatedByInheritanceOrIdentical(targetType, exprType)
    )
      this.incrementCounters(tsAsExpr, FaultID.StructuralIdentity);
  }

  private handleTypeReference(node: ts.Node) {
    let typeRef = node as ts.TypeReferenceNode;

    if (ts.isIdentifier(typeRef.typeName) && LinterConfig.standardUtilityTypes.has(typeRef.typeName.text))
      this.incrementCounters(node, FaultID.UtilityType);
  }

  private handleMetaProperty(node: ts.Node) {
    let tsMetaProperty = node as ts.MetaProperty;
    if (tsMetaProperty.name.text === 'target')
      this.incrementCounters(node, FaultID.NewTarget);
  }

  private handleStructDeclaration(node: ts.Node) {
    node.forEachChild(child => {
      // Skip synthetic constructor in Struct declaration.
      if (!ts.isConstructorDeclaration(child)) this.visitTSNode(child);
    });
  }

  private handleBigIntLiteral(node: ts.Node) {
    let autofixes: Autofix[] | undefined;

    if (this.autofixesInfo.shouldAutofix(node, FaultID.BigIntLiteral)) {
      // If decimal value of literal exceeds 15 digits, create
      // BigInt ctor call with string argument.
      let isStringArg = false;
      let type = this.tsTypeChecker.getTypeAtLocation(node);
      if ((type.flags & ts.TypeFlags.BigIntLiteral) !== 0) {
        let bigIntType = type as ts.BigIntLiteralType;
        isStringArg = bigIntType.value.base10Value.length > 15;
      }

      autofixes = Autofixer.fixBigIntLiteral(node as ts.BigIntLiteral, isStringArg);
    }

    this.incrementCounters(node, FaultID.BigIntLiteral, true, autofixes);
  }

  private handleSpreadOp(node: ts.Node) {
    // spread assignment is disabled
    // spread element is allowed only for arrays as rest parameer
    if( ts.isSpreadElement(node) ) {
      let spreadElemNode = node as ts.SpreadElement;
      let spreadExprType = this.tsTypeChecker.getTypeAtLocation(spreadElemNode.expression);
      if (spreadExprType) {
        const spreadExprTypeNode = this.tsTypeChecker.typeToTypeNode(spreadExprType, undefined, ts.NodeBuilderFlags.None);
        if (spreadExprTypeNode && this.tsUtils.isArrayNotTupleType(spreadExprTypeNode) && ts.isCallLikeExpression(node.parent)) {
          // rest parameter should be the last and the one
          return;
        }
      }
    }
    this.incrementCounters(node, FaultID.SpreadOperator);
  }

  private handleNonNullExpression(node: ts.Node) {
    let nonNullExprNode = node as ts.NonNullExpression;
    // increment only at innermost exclamation
    // this ensures that only one report is generated for sequenced exclamations
    if (nonNullExprNode.expression.kind !== ts.SyntaxKind.NonNullExpression) {
      this.incrementCounters(node, FaultID.DefiniteAssignment);
    }
  }

  public lint(sourceFile: ts.SourceFile) {
    this.sourceFile = sourceFile;
    this.visitTSNode(this.sourceFile);
  }
}
