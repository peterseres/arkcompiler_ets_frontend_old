#  ``enum`` declaration merging is not supported

Rule ``arkts-no-enum-merging``

**Severity: error**

ArkTS does not support merging declratations for ``enum``.
The declaration of each ``enum`` must be kept compact in the code base.


## TypeScript


```

    enum Color {
        RED,
        GREEN
    }
    enum Color {
        YELLOW = 2
    }
    enum Color {
        BLACK = 3,
        BLUE
    }

```

## ArkTS


```

    enum Color {
        RED,
        GREEN,
        YELLOW,
        BLACK,
        BLUE
    }

```


