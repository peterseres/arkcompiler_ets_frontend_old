#  Declaration merging is not supported

Rule ``arkts-no-decl-merging``

**Severity: error**

ArkTS does not support merging declratations. All definitions of classes,
interfaces and so on must be kept compact in the code base.


## TypeScript


```

    interface Document {
        createElement(tagName: any): Element
    }

    interface Document {
        createElement(tagName: string): HTMLElement
    }

    interface Document {
        createElement(tagName: number): HTMLDivElement
        createElement(tagName: boolean): HTMLSpanElement
        createElement(tagName: string, value: number): HTMLCanvasElement
    }

```

## ArkTS


```

    interface Document {
        createElement(tagName: number): HTMLDivElement
        createElement(tagName: boolean): HTMLSpanElement
        createElement(tagName: string, value: number): HTMLCanvasElement
        createElement(tagName: string): HTMLElement
        createElement(tagName: Object): Element
    }

```


