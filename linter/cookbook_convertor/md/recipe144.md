#  Usage of standard library is restricted

Rule ``arkts-limited-stdlib``

**Severity: error**

ArkTS does not allow usage of some APIs from the TypeScript/JavaScript standard library.
The most part of the restricted APIs relates to manipulating objects in
dynamic manner, which is not compatible with the static typing. Following APIs
are prohibited from usage:

Properties and functions of the global object: ``eval``,
``Infinity``, ``NaN``, ``isFinite``, ``isNaN``, ``parseFloat``, ``parseInt``,
``encodeURI``, ``encodeURIComponent``, ``Encode``,
``decodeURI``, ``decodeURIComponent``, ``Decode``,
``escape``, ``unescape``, ``ParseHexOctet``

``Object``: ``__proto__``, ``__defineGetter__``, ``__defineSetter__``,
``__lookupGetter__``, ``__lookupSetter__``, ``assign``, ``create``,
``defineProperties``, ``defineProperty``, ``entries``, ``freeze``,
``fromEntries``, ``getOwnPropertyDescriptor``, ``getOwnPropertyDescriptors``,
``getOwnPropertyNames``, ``getOwnPropertySymbols``, ``getPrototypeOf``,
``hasOwn``, ``hasOwnProperty``, ``is``, ``isExtensible``, ``isFrozen``,
``isPrototypeOf``, ``isSealed``, ``keys``, ``preventExtensions``,
``propertyIsEnumerable``, ``seal``, ``setPrototypeOf``, ``values``

``Reflect``: ``apply``, ``construct``, ``defineProperty``, ``deleteProperty``,
``get``, ``getOwnPropertyDescriptor``, ``getPrototypeOf``, ``has``,
``isExtensible``, ``ownKeys``, ``preventExtensions``, ``set``,
``setPrototypeOf``

``Proxy``: ``handler.apply()``, ``handler.construct()``,
``handler.defineProperty()``, ``handler.deleteProperty()``, ``handler.get()``,
``handler.getOwnPropertyDescriptor()``, ``handler.getPrototypeOf()``,
``handler.has()``, ``handler.isExtensible()``, ``handler.ownKeys()``,
``handler.preventExtensions()``, ``handler.set()``, ``handler.setPrototypeOf()``

``Array``: ``isArray``

``ArrayBuffer``: ``isView``


## See also

- Recipe 001:  Objects with property names that are not identifiers are not supported (``arkts-identifiers-as-prop-names``)
- Recipe 002:  ``Symbol()`` API is not supported (``arkts-no-symbol``)
- Recipe 052:  Attempt to access an undefined property is a compile-time error (``arkts-no-undefined-prop-access``)
- Recipe 060:  ``typeof`` operator is allowed only in expression contexts (``arkts-no-type-query``)
- Recipe 066:  ``in`` operator is not supported (``arkts-no-in``)
- Recipe 105:  Property-based runtime type checks are not supported (``arkts-no-prop-existence-check``)
- Recipe 109:  Dynamic property declaration is not supported (``arkts-no-dyn-prop-decl``)
- Recipe 137:  ``globalThis`` is not supported (``arkts-no-globalthis``)


