#  Binary operators ``*``, ``/``, ``%``, ``-``, ``<<``, ``>>``, ``>>>``, ``&``, ``^`` and ``|`` work only on numeric types

Rule ``arkts-no-polymorphic-binops``

**Severity: error**

ArkTS allows applying binary operators ``*``, ``/``, ``%``, ``-``, ``<<``,
``>>``, ``>>>``, ``&``, ``^`` and ``|`` only to values of numeric types.
Implicit casts from other types to numeric types are prohibited and cause
compile-time errors.


## TypeScript


```

    let a = (5 & 5)     // 5
    let b = (5.5 & 5.5) // 5, not 5.5
    let c = (5 | 5)     // 5
    let d = (5.5 | 5.5) // 5, not 5.5

    enum Direction {
        Up = -1,
        Down
    }
    let e = Direction.Up >> 1 // -1
    let f = Direction.Up >>> 1 // 2147483647

    let g = ("10" as any) << 1  // 20
    let h = ("str" as any) << 1 // 0

    let i = 10 * 5
    let j = 10 / 5
    let k = 10 % 5
    let l = 10 - 5

```

## ArkTS


```

    let a = (5 & 5)     // 5
    let b = (5.5 & 5.5) // Compile-time error
    let c = (5 | 5)     // 5
    let d = (5.5 | 5.5) // Compile-time error

    enum Direction {
        Up,
        Down
    }

    let e = Direction.Up >> 1  // 0
    let f = Direction.Up >>> 1 // 0

    let i = 10 * 5
    let j = 10 / 5
    let k = 10 % 5
    let l = 10 - 5

```

## See also

- Recipe 055:  Unary operators ``+``, ``-`` and ``~`` work only on numbers (``arkts-no-polymorphic-unops``)
- Recipe 056:  Unary ``+`` cannot be used for casting to ``number`` (``arkts-no-unary-plus-cast``)
- Recipe 063:  Binary ``+`` operator supports implicit casts only for numbers and strings (``arkts-no-polymorphic-plus``)


