#  Structural typing is not supported for subtyping / supertyping

Rule ``arkts-no-structural-subtyping``

**Severity: error**

Currently, ArkTS does not check structural equivalence for type inference,
i.e., the compiler cannot compare two types' public APIs and decide whether
such types are identical. Use other mechanisms (inheritance or interfaces)
instead.


## TypeScript


```

    class X {
        public foo: number

        constructor() {
            this.foo = 0
        }
    }

    class Y {
        public foo: number

        constructor() {
            this.foo = 0
        }
    }

    let x = new X()
    let y = new Y()

    console.log("Assign X to Y")
    y = x

    console.log("Assign Y to X")
    x = y


```

## ArkTS


```

    class X {
        public foo: number

        constructor() {
            this.foo = 0
        }
    }

    // Y is derived from X, which explicitly set subtype / supertype relations:
    class Y extends X {
        constructor() {
            super()
        }
    }

    let x = new X()
    let y = new Y()

    console.log("Assign Y to X")
    x = y // ok, X is the super class of Y

    // Cannot assign X to Y
    //y = x - compile-time error


```

## See also

- Recipe 030:  Structural identity is not supported (``arkts-no-structural-identity``)
- Recipe 032:  Structural typing is not supported for assignability checks (``arkts-no-structural-assignability``)
- Recipe 035:  Structural typing is not supported for type inference (``arkts-no-structural-inference``)


