#  Attempt to access an undefined property is a compile-time error

Rule ``arkts-no-undefined-prop-access``

**Severity: error**

ArkTS supports accessing only those class properties that are either declared
in the class, or accessible via inheritance. Accessing any other properties is
prohibited and causes compile-time errors. Use proper types to check property
existence during compilation.


## TypeScript


```

    let person = {name: "Bob", isEmployee: true}

    let n = person["name"]
    let e = person["isEmployee"]
    let s = person["office"] // undefined

```

## ArkTS


```

    class Person {
        constructor(name: string, isEmployee: boolean) {
            this.name = name
            this.isEmployee = isEmployee
        }

        name: string
        isEmployee: boolean
    }

    let person = new Person("Bob", true)
    let n = person.name
    let e = person.isEmployee
    let s = person.office // Compile-time error

```

## See also

- Recipe 001:  Objects with property names that are not identifiers are not supported (``arkts-identifiers-as-prop-names``)
- Recipe 002:  ``Symbol()`` API is not supported (``arkts-no-symbol``)
- Recipe 059:  ``delete`` operator is not supported (``arkts-no-delete``)
- Recipe 060:  ``typeof`` operator is allowed only in expression contexts (``arkts-no-type-query``)
- Recipe 066:  ``in`` operator is not supported (``arkts-no-in``)
- Recipe 105:  Property-based runtime type checks are not supported (``arkts-no-prop-existence-check``)
- Recipe 109:  Dynamic property declaration is not supported (``arkts-no-dyn-prop-decl``)
- Recipe 144:  Usage of standard library is restricted (``arkts-limited-stdlib``)


