#  Unary operators ``+``, ``-`` and ``~`` work only on numbers

Rule ``arkts-no-polymorphic-unops``

**Severity: error**

ArkTS allows unary operators to work on numeric types only. A compile-time
error occurs if these operators are applied to a non-numeric type. Unlike in
TypeScript, implicit casting of strings in this context is not supported and must
be done explicitly.


## TypeScript


```

    let a = +5   // 5 as number
    let b = +"5" // 5 as number
    let c = -5   // -5 as number
    let d = -"5" // -5 as number
    let e = ~5   // -6 as number
    let f = ~"5" // -6 as number
    let g = +"string" // NaN as number

```

## ArkTS


```

    let a = +5   // 5 as int
    let b = +"5" // Compile-time error
    let c = -5   // -5 as int
    let d = -"5" // Compile-time error
    let e = ~5   // -6 as int
    let f = ~"5" // Compile-time error
    let g = +"string" // Compile-time error

```

## See also

- Recipe 061:  Binary operators ``*``, ``/``, ``%``, ``-``, ``<<``, ``>>``, ``>>>``, ``&``, ``^`` and ``|`` work only on numeric types (``arkts-no-polymorphic-binops``)
- Recipe 063:  Binary ``+`` operator supports implicit casts only for numbers and strings (``arkts-no-polymorphic-plus``)


