#  Structural typing is not supported for type inference

Rule ``arkts-no-structural-inference``

**Severity: error**

Currently, ArkTS does not support structural typing, i.e., the compiler cannot
compare two types' public APIs and decide whether such types are identical.
Use inheritance and interfaces to specify the relation between the types
explicitly.


## TypeScript


```

    class X  {
        public foo: number
        private s: string

        constructor (f: number) {
            this.foo = f
            this.s = ""
        }

        public say(): void {
           console.log("X = ", this.foo)
        }
    }

    class Y {
        public foo: number

        constructor (f: number) {
            this.foo = f
        }
        public say(): void {
            console.log("Y = ", this.foo)
        }
    }

    function bar(z: X): void {
        z.say()
    }

    // X and Y are equivalent because their public API is equivalent.
    // Thus the second call is allowed:
    bar(new X(1))
    bar(new Y(2) as X)

```

## ArkTS


```

    interface Z {
       say(): void
    }

    class X implements Z {
        public foo: number
        private s: string

        constructor (f: number) {
            this.foo = f
            this.s = ""
        }
        public say(): void {
            console.log("X = ", this.foo)
        }
    }

    class Y implements Z {
        public foo: number

        constructor (f: number) {
            this.foo = f
        }
        public say(): void {
            console.log("Y = ", this.foo)
        }
    }

    function bar(z: Z): void {
        z.say()
    }

    // X and Y implement the same interface Z, thus both calls are allowed:
    bar(new X(1))
    bar(new Y(2))

```

## See also

- Recipe 030:  Structural identity is not supported (``arkts-no-structural-identity``)
- Recipe 031:  Structural typing is not supported for subtyping / supertyping (``arkts-no-structural-subtyping``)
- Recipe 032:  Structural typing is not supported for assignability checks (``arkts-no-structural-assignability``)


