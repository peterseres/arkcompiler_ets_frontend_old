/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ETSfunction.h"

#include "plugins/ecmascript/es2panda/binder/binder.h"
#include "plugins/ecmascript/es2panda/binder/ETSBinder.h"
#include "plugins/ecmascript/es2panda/util/helpers.h"
#include "plugins/ecmascript/es2panda/binder/scope.h"
#include "plugins/ecmascript/es2panda/binder/variable.h"
#include "plugins/ecmascript/es2panda/compiler/base/lreference.h"
#include "plugins/ecmascript/es2panda/compiler/core/ETSGen.h"
#include "plugins/ecmascript/es2panda/compiler/core/envScope.h"
#include "plugins/ecmascript/es2panda/ir/base/scriptFunction.h"
#include "plugins/ecmascript/es2panda/ir/base/classDefinition.h"
#include "plugins/ecmascript/es2panda/ir/base/classProperty.h"
#include "plugins/ecmascript/es2panda/ir/expressions/identifier.h"
#include "plugins/ecmascript/es2panda/ir/statements/blockStatement.h"
#include "plugins/ecmascript/es2panda/ir/ts/tsEnumDeclaration.h"
#include "plugins/ecmascript/es2panda/ir/ts/tsEnumMember.h"
#include "plugins/ecmascript/es2panda/checker/types/ets/types.h"

namespace panda::es2panda::compiler {
void ETSFunction::CallImplicitCtor(ETSGen *etsg)
{
    RegScope rs(etsg);
    auto *super_type = etsg->ContainingObjectType()->SuperType();

    if (super_type == nullptr) {
        etsg->CallThisStatic0(etsg->RootNode(), etsg->GetThisReg(), Signatures::BUILTIN_OBJECT_CTOR);

        return;
    }

    auto res = std::find_if(super_type->ConstructSignatures().cbegin(), super_type->ConstructSignatures().cend(),
                            [](const checker::Signature *sig) { return sig->Params().empty(); });

    if (res == super_type->ConstructSignatures().cend()) {
        return;
    }

    etsg->CallThisStatic0(etsg->RootNode(), etsg->GetThisReg(), (*res)->InternalName());
}

void ETSFunction::CompileSourceBlock(ETSGen *etsg, const ir::BlockStatement *block)
{
    auto *script_func = etsg->RootNode()->AsScriptFunction();
    if (script_func->IsEnum()) {
        // TODO(user): add enum methods
    } else if (script_func->IsStaticBlock()) {
        const auto *class_def = etsg->ContainingObjectType()->GetDeclNode()->AsClassDefinition();

        for (const auto *prop : class_def->Body()) {
            if (!prop->IsClassProperty() || !prop->IsStatic()) {
                continue;
            }

            prop->AsClassProperty()->Compile(etsg);
        }
    } else if (script_func->IsConstructor()) {
        if (script_func->IsImplicitSuperCallNeeded()) {
            CallImplicitCtor(etsg);
        }

        const auto *class_def = etsg->ContainingObjectType()->GetDeclNode()->AsClassDefinition();

        for (const auto *prop : class_def->Body()) {
            if (!prop->IsClassProperty() || prop->IsStatic()) {
                continue;
            }

            prop->AsClassProperty()->Compile(etsg);
        }
    }

    const auto &statements = block->Statements();

    if (statements.empty()) {
        etsg->SetFirstStmt(block);
        etsg->EmitReturnVoid(block);
        return;
    }

    etsg->SetFirstStmt(statements.front());

    etsg->CompileStatements(statements);

    if (!statements.back()->IsReturnStatement()) {
        if (etsg->ReturnType()->IsETSVoidType()) {
            etsg->EmitReturnVoid(statements.back());
        } else {
            etsg->LoadDefaultValue(statements.back(), script_func->Signature()->ReturnType());
            etsg->ReturnAcc(statements.back());
        }
    }
}

void ETSFunction::CompileFunctionParameterDeclaration(ETSGen *etsg, const ir::ScriptFunction *func)
{
    ScopeContext scope_ctx(etsg, func->Scope()->ParamScope());

    uint32_t index = 0;

    for (const auto *param : func->Params()) {
        if (!param->IsRestElement()) {
            index++;
            continue;
        }

        auto ref = JSLReference::Create(etsg, param, true);
        [[maybe_unused]] binder::Variable *param_var = ref.Variable();
        ASSERT(param_var && param_var->IsLocalVariable());

        [[maybe_unused]] VReg param_reg = VReg(binder::Binder::MANDATORY_PARAMS_NUMBER + VReg::PARAM_START + index++);
        ASSERT(param_var->AsLocalVariable()->Vreg() == param_reg);

        ref.SetValue();
        index++;
    }
}

void ETSFunction::CompileFunction(ETSGen *etsg)
{
    const auto *decl = etsg->RootNode()->AsScriptFunction();
    CompileFunctionParameterDeclaration(etsg, decl);

    const ir::AstNode *body = decl->Body();

    if (body->IsExpression()) {
        // TODO(user):
    } else {
        CompileSourceBlock(etsg, body->AsBlockStatement());
    }
}

void ETSFunction::Compile(ETSGen *etsg)
{
    FunctionRegScope lrs(etsg);
    auto *top_scope = etsg->TopScope();

    if (top_scope->IsFunctionScope()) {
        CompileFunction(etsg);
    } else {
        ASSERT(top_scope->IsGlobalScope());
        CompileSourceBlock(etsg, etsg->RootNode()->AsBlockStatement());
    }

    etsg->SortCatchTables();
}

}  // namespace panda::es2panda::compiler
